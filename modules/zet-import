#!/usr/bin/env python3

import os
import sys
import argparse
import subprocess
import shutil
import re

from html.parser import HTMLParser

sys.path.append(os.path.join(os.environ["ZET_LIB_DIR"], "python"))
from pyzet import *


def prepare_args():
    parser = argparse.ArgumentParser(
        description="import files and URLs into the notebook"
    )

    parser.add_argument(
        "paths", nargs="+", help="files or URLs which should be imported"
    )

    parser.add_argument(
        "-d",
        "--destination",
        default=f"{api_default_notebook()}:",
        help="destination SELECTOR to which files will be imported in form "
        "'notebook:[<directory/>][<file>]'",
    )

    return parser.parse_args()


class TitleParser(HTMLParser):
    def __init__(self):
        self.collect = False
        self.title = None
        super().__init__()

    def handle_starttag(self, tag, attrs):
        if tag == "title":
            self.collect = True

    def handle_endtag(self, tag):
        if tag == "title":
            self.collect = False

    def handle_data(self, data):
        if self.title is None and self.collect is True:
            self.title = data


def slugify(s: str | None):
    if not s:
        return ""
    parts = re.split(r"\W+", s.lower().strip())
    return "-".join(parts)


def is_url(path):
    protocols = ("http", "https")
    return any(path.startswith(f"{proto}://") for proto in protocols)


def get_title(content, url) -> str:
    parser = TitleParser()
    parser.feed(content)
    if title := slugify(parser.title):
        return title

    import urllib.parse

    path = urllib.parse.urlparse(url).path
    for elem in reversed(path.split("/")):
        if title := slugify(parser.title):
            return title

    return "url"


def import_url(url, selector):
    if not command_exists("pandoc"):
        eprint("pandoc is required to convert web pages to text files")
        return False

    import urllib.request
    import urllib.error

    try:
        resp = urllib.request.urlopen(url)
    except urllib.error.HTTPError as e:
        eprint(e)
        return False

    content = resp.read()

    pandoc = [
        "pandoc",
        "--from",
        "html-native_divs-native_spans",
        "--to",
        "markdown-grid_tables",
        "--markdown-headings",
        "atx",
    ]
    cp = subprocess.run(pandoc, input=content, capture_output=True)
    if cp.returncode != 0:
        eprint(cp.stderr.decode())
        return False

    p = urllib.parse.urlparse(url)

    default_title = get_title(content.decode(), url)

    # TODO: remove extension parameter. Problem: pandoc to default_filetype
    # mapping (mg, 2023-03-05)
    dest = api_unique_files(selector, stem=default_title, extension="md")[0]

    os.makedirs(os.path.dirname(dest), exist_ok=True)
    with open(dest, "wb") as f:
        f.write(cp.stdout)

    print(f"{url} imported to {dest}")
    return True


def import_file(src, selector):
    basename = os.path.basename(src)
    stem, ext = os.path.splitext(basename)
    dest = api_unique_files(selector, stem=stem, extension=ext)[0]

    os.makedirs(os.path.dirname(dest), exist_ok=True)
    try:
        shutil.copy2(src, dest)
    except Exception as e:
        eprint(f"Failed importing {src}: {e}")
        return False
    else:
        print(f"{src} imported to {dest}")
        return True


def main():
    args = prepare_args()

    status = 0
    for path in args.paths:
        fn = import_url if is_url(path) else import_file
        if not fn(path, args.destination):
            status = 1

    return status


sys.exit(main())
