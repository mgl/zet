#!/usr/bin/env python3

import os
import sys
import argparse
import subprocess

sys.path.append(os.path.join(os.environ["ZET_LIB_DIR"], "python"))
from pyzet import api_default_notebook, api_notebooks, eprint


def prepare_args():
    parser = argparse.ArgumentParser(
        description="Run a command inside a notebook's directory. Default notebook's directory is used by default."
    )

    parser.add_argument(
        "-n", "--notebook", nargs="?", default=None, help="notebook to use"
    )

    parser.add_argument(
        "-a", "--all", action="store_true", help="run a command in all notebooks"
    )

    parser.add_argument(
        "-c", "--cwd", nargs="?", default=None,
        help="execute a command in a given directory")

    parser.add_argument("cmd", nargs=argparse.REMAINDER, help="command to run")

    return parser.parse_args()


def run(cmd, cwd) -> int:
    try:
        cp = subprocess.run(cmd, cwd=cwd, check=True)
        return cp.returncode
    except FileNotFoundError as fe:
        eprint(str(fe))
        return 1
    except subprocess.CalledProcessError as ce:
        return ce.returncode


def main():
    args = prepare_args()

    if len(args.cmd) < 1:
        sys.exit(1)

    nbdirs = []

    if args.all:
        nbdirs = api_notebooks(absolute=True)

    if args.notebook and not nbdirs:
        nbdirs.extend(api_notebooks(args.notebook, absolute=True))

    if args.cwd:
        nbdirs.append(args.cwd)

    if not nbdirs:
        nbdirs = [api_default_notebook(absolute=True)]

    ret = 0
    for nbdir in nbdirs:
        ret |= run(args.cmd, nbdir)
    sys.exit(ret)


main()
