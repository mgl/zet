default-notebook: Display or set the default notebook
gen: Generate a file name from format string
import: Import files and URLs into the notebook
journal: Create and maintain daily journal entries
ls: List contents of notebooks
menu: Interactive file list
mv: Rename a file
note: Add new notes
notebooks: List notebooks
open: Open notes and files in notebooks
replace: Search and replace phrases in notes
run: Run a command inside a notebook's directory
search: full-text search of the notebooks
show: show note contents inside a terminal
sync: Synchronize notes with git-sync
tasks: Show tasks in notebooks
