#!/usr/bin/env python3

from typing import Callable, List, Dict

import os
import sys
import argparse
import subprocess
from dataclasses import dataclass
from functools import cache

sys.path.append(os.path.join(os.environ["ZET_LIB_DIR"], "python"))
from pyzet import die, eprint
from pyzet import api_notebooks, api_list_files, api_print, command_exists, getenv

SearchResult = Dict[str, List[str]]
GrepFn = Callable[[List[str], List[str], argparse.Namespace], SearchResult]


@dataclass
class Match:
    no: int
    text: str

    # these operations only use line number, which is fine as long as we
    # compare matches from the same file
    def __hash__(self):
        return hash(self.no)

    def __eq__(self, other):
        return self.no == other.no

    def __lt__(self, other):
        return self.no < other.no


@dataclass
class Notebook:
    name: str
    abspath: str
    selector: str


def run(*cmd, **kw):
    kw.setdefault("capture_output", True)
    kw.setdefault("text", True)
    kw.setdefault("check", True)

    try:
        cp = subprocess.run(cmd, **kw)
    except FileNotFoundError:
        die(f"{cmd[0]} executable not found")
    return cp


def default_selectors():
    return [nb.selector for nb in get_notebooks()]


def choose_default_grep(grep_programs):
    for prog in grep_programs:
        if command_exists(prog):
            return prog

    gpls = list(grep_programs)  # for formatting
    die(f"No functional program found among: {gpls}")


def prepare_args(grep_programs):
    parser = argparse.ArgumentParser(description="Search the notes.")

    parser.add_argument(
        "query",
        nargs="+",
        help=(
            "phrase or regular expression which should be searched. "
            "Queries are joined with logical AND, returning items that match "
            "all queries."
        ),
    )
    parser.add_argument(
        "-s",
        "--selectors",
        action="append",
        default=[],
        help=(
            "narrow down search to the files given by selectors in form "
            "[notebook:][directory/][file]. By default all files are searched"
        ),
    )

    parser.add_argument(
        "--or",
        action="store_true",
        dest="or_queries",
        help="return notes with at least one of the queries found",
    )

    parser.add_argument(
        "-i",
        "--ignore-case",
        action="store_true",
        help="perform case-insensitive search",
    )

    parser.add_argument(
        "-l",
        "--files-with-matches",
        action="store_true",
        help="Only print the paths of files with matches",
    )

    parser.add_argument(
        "--absolute", action="store_true", help="Print absolute paths to matched files"
    )

    parser.add_argument(
        "-a",
        "--show-all",
        action="store_true",
        help="Don't truncate results, but show all matched lines",
    )

    parser.add_argument(
        "--grep-program",
        choices=grep_programs.keys(),
        default=choose_default_grep(grep_programs.keys()),
        help="Force using a particular grep flavor",
    )

    return parser.parse_args()


def analyze_grep_output(out: SearchResult, inp: str, delim=":") -> SearchResult:
    for line in inp.splitlines():
        try:
            path, lineno, text = line.split(delim, 2)
        except ValueError:  # binary files, TODO: remove them from file list
            continue

        out.setdefault(path, set()).add(Match(lineno, text))
    return out


def make_e_switches(queries: List[str]) -> List[str]:
    query_args = []
    for q in queries:
        query_args.extend(["-e", q])
    return query_args


def grep(
    queries: List[str], files: List[str], args: argparse.Namespace
) -> SearchResult:
    result = {}

    grep_args = ["--color=never", "--with-filename", "--line-number"]
    if args.ignore_case:
        grep_args.append("--ignore-case")

    query_args = make_e_switches(queries)
    for f in files:
        try:
            cp = run("grep", *grep_args, *query_args, f)
        except subprocess.CalledProcessError:
            continue
        analyze_grep_output(result, cp.stdout)
    return result


def rg(queries: List[str], files: List[str], args: argparse.Namespace) -> SearchResult:
    result = {}

    rg_args = [
        "--color",
        "never",
        "--with-filename",
        "--line-number",
        "--no-heading",
        "--no-binary",
    ]
    if args.ignore_case:
        rg_args.append("--ignore-case")

    query_args = make_e_switches(queries)
    chunksize = 64
    i = 0
    while i < len(files):
        filechunk = files[i : i + chunksize]
        i += chunksize

        try:
            cp = run("rg", *rg_args, *query_args, *filechunk)
        except subprocess.CalledProcessError:
            continue
        analyze_grep_output(result, cp.stdout)

    return result


def search_and(
    grep_fn: GrepFn, files: List[str], args: argparse.Namespace
) -> SearchResult:
    gathered_result = None

    def _intersection(lhs: dict, rhs: dict):
        # this is key (file) intersection; for values we gather all of
        # collcected matches
        intersection = lhs.keys() & rhs.keys()
        return {key: lhs[key] | rhs[key] for key in intersection}

    for query in args.query:
        result = grep_fn([query], files, args)

        if gathered_result is None:
            gathered_result = result
        else:
            gathered_result = _intersection(gathered_result, result)

        files = list(gathered_result.keys())

    return gathered_result


def search_or(
    grep_fn: GrepFn, files: List[str], args: argparse.Namespace
) -> SearchResult:
    return grep_fn(args.query, files, args)


def search(args, grep_programs, files: List[str]):
    grep_fn = grep_programs[args.grep_program]

    if args.or_queries:
        return search_or(grep_fn, files, args)
    return search_and(grep_fn, files, args)


@cache
def get_notebooks() -> List[Notebook]:
    fs = chr(0x1F)  # ASCII Unit Separator
    nbs = api_notebooks(absolute=True, selector=True, name=True, fs=fs)

    def make_notebook(nb_data: str):
        try:
            name, abspath, selector = nb_data.split(fs)
        except ValueError:
            possible_name = nb_data.split(fs)
            die(
                f"Notebook {possible_name} has non-printable ASCII character s in its name or path: 0x{ord(fs):x}! Please remove them."
            )

        return Notebook(name=name, abspath=abspath, selector=selector)

    return [make_notebook(nb_data) for nb_data in nbs]


def print_matches(args, result: SearchResult):
    root = getenv("ZET_ROOT")
    paths = sorted(result.keys())
    truncate_at = 5

    def find_notebook_on_path(path, nbs):
        for nb in nbs:
            if path.startswith(nb.abspath + "/"):
                return nb
        return None

    def conditional_relative_path(path):
        if not args.absolute:
            nb = find_notebook_on_path(path, get_notebooks())
            if nb:
                return f"{nb.selector}{path[len(nb.abspath) + 1 :]}"
        return path

    if args.files_with_matches:
        for path in paths:
            print(conditional_relative_path(path))
        return

    headers = []
    for path in paths:
        headers.append(conditional_relative_path(path) + ":")

    if sys.stdout.isatty():
        headers = api_print(*headers, style="header")

    for i, path in enumerate(paths):
        print(headers[i])
        matches = sorted(result[path])
        for j, match in enumerate(matches):
            print(f"    {match.text}")

            if j + 1 >= truncate_at and not args.show_all:
                print(
                    "(... result truncated, run 'zet search --show-all' to show all matched entries ...)"
                )
                break

        if i < len(paths) - 1:
            print()


def main():
    # order is important
    grep_programs = {
        "rg": rg,
        "grep": grep,
    }

    args = prepare_args(grep_programs)
    if not args.selectors:
        args.selectors = default_selectors()

    files = api_list_files(*args.selectors)

    lf = len(files)
    if lf > 100:
        eprint(
            f"Searching over {lf} files. This might take some time for certain backends..."
        )

    result = search(args, grep_programs, files)
    print_matches(args, result)


sys.exit(main())
