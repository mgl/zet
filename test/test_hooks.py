import pytest
import sh


def test_command_hooks(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    ret = zet("fake-command", "arg1", "--switch", _return_cmd=True)

    assert pre_command.called
    assert fake_command.called
    assert post_command.called

    assert len(pre_command.calls) == 1
    assert pre_command.calls[0]["args"] == ["fake-command", "arg1", "--switch"]

    assert len(post_command.calls) == 1
    assert post_command.calls[0]["args"] == ["0", "fake-command", "arg1", "--switch"]

    assert not ret.stdout
    assert not ret.stderr


def test_command_hooks_dont_suppress_hooks_output(
    zet, notebooks, monkeyprogram, monkeyhook
):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    pre_command.on_request(".*", responses="pre-command response")
    post_command.on_request(".*", responses="post-command response")

    ret = zet("fake-command", "arg1", "--switch", _return_cmd=True)

    assert pre_command.called
    assert fake_command.called
    assert post_command.called

    assert len(pre_command.calls) == 1
    assert pre_command.calls[0]["args"] == ["fake-command", "arg1", "--switch"]

    assert len(post_command.calls) == 1
    assert post_command.calls[0]["args"] == ["0", "fake-command", "arg1", "--switch"]

    assert not ret.splitlines() == [
        "pre-command response",
        "post-command response",
    ]

    assert not ret.stderr


def test_command_hooks_no_hooks_env_var(zet, notebooks, monkeyprogram, monkeyhook, monkeypatch):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    monkeypatch.setenv("ZET_NO_HOOKS", "1")
    ret = zet("fake-command", _return_cmd=True)

    assert fake_command.called
    assert not pre_command.called
    assert not post_command.called

    assert not ret.stdout
    assert not ret.stderr


def test_command_hooks_no_hooks_flag(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    ret = zet("--no-hooks", "fake-command", _return_cmd=True)

    assert fake_command.called
    assert not pre_command.called
    assert not post_command.called

    assert not ret.stdout
    assert not ret.stderr


def test_command_hooks_pre_command_fail(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    pre_command.on_request(".*", returncode=1).monkeypatch()

    ret = zet("fake-command", _ok_code=[2], _return_cmd=True)

    assert pre_command.called
    assert not fake_command.called
    assert not post_command.called

    err_lines = ret.stderr.decode().splitlines()
    assert err_lines == [
        "pre-command: hook failed",
        "pre-command: you may disable hooks with 'zet --no-hooks ...",
    ]


def test_command_hooks_post_command_fail(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    post_command.on_request(".*", returncode=1).monkeypatch()

    ret = zet("fake-command", _ok_code=[2], _return_cmd=True)

    assert pre_command.called
    assert fake_command.called
    assert post_command.called

    err_lines = ret.stderr.decode().splitlines()
    assert err_lines == [
        "post-command: hook failed",
        "post-command: you may disable hooks with 'zet --no-hooks ...",
    ]


def test_command_hooks_subcommand_failed(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    fake_command.on_request(".*", returncode=1).monkeypatch()

    zet("fake-command", "arg1", _ok_code=[2])

    assert pre_command.called
    assert fake_command.called
    assert post_command.called

    assert len(post_command.calls) == 1
    assert post_command.calls[0]["args"] == ["1", "fake-command", "arg1"]


def test_command_hooks_subcommand_not_found(zet, notebooks, monkeyprogram, monkeyhook):
    pre_command = monkeyhook("pre-command")
    post_command = monkeyhook("post-command")

    zet("fake-command", "arg1", _ok_code=[2])

    assert pre_command.called
    assert not post_command.called

    assert len(pre_command.calls) == 1
    assert pre_command.calls[0]["args"] == ["fake-command", "arg1"]


def test_hooks_not_executable(zet, notebooks, monkeyprogram, monkeyhook):
    # we copy instead of symlinking, because on most platforms chmoding follows
    # symlinks and would change permissions of our original monkeyprogram
    pre_command = monkeyhook("pre-command", symlink=False)
    post_command = monkeyhook("post-command", symlink=False)
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)

    pre_command.path.chmod(0o644)
    post_command.path.chmod(0o644)

    ret = zet("fake-command", "arg1", _return_cmd=True)

    assert not pre_command.called
    assert not post_command.called
    assert fake_command.called

    assert not ret.stdout
    assert not ret.stderr
