import os
import socket
import getpass
from datetime import date, timedelta
from pathlib import Path

import pytest
import sh

TEMPLATE = """
template start
{date}
{host}
{user}
template end
""".strip()


@pytest.fixture
def template(tmp_path):
    f = tmp_path / "template.txt"
    f.write_text(TEMPLATE)
    return f


def test_journal(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}")

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    contents = entry.read_text().splitlines()
    assert contents == ["foo"]


def test_journal_append(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}")
    zet("journal", "--editor", "sleep 0.01 && echo bar >> {}")

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    contents = entry.read_text().splitlines()
    assert contents == ["foo", "bar"]


def test_journal_different_dates(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}", "today")
    zet("journal", "--editor", "sleep 0.01 && echo bar > {}", "yesterday")

    today = date.today().isoformat()
    yesterday = date.today() - timedelta(days=1)
    t_entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    y_entry = notebooks / "content" / "my notebook" / "journal" / f"{yesterday}.md"

    today_contents = t_entry.read_text().splitlines()
    assert today_contents == ["foo"]

    yesterday_contents = y_entry.read_text().splitlines()
    assert yesterday_contents == ["bar"]


def test_journal_template(zet, notebooks, template):
    zet("journal", "--editor", "sleep 0.01 && touch {}", "--template", template)

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    contents = entry.read_text().splitlines()

    assert contents[0] == "template start"
    assert contents[1] == today
    assert contents[2] == socket.gethostname()
    assert contents[3] == getpass.getuser()
    assert contents[4] == "template end"


def test_journal_empty_template_update(zet, notebooks, template):
    template.write_text("")
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}", "--template", template)

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    contents = entry.read_text().splitlines()
    assert contents == ["foo"]


def test_journal_empty_template_no_update(zet, notebooks, template):
    template.write_text("")
    zet("journal", "--editor", "sleep 0.01 && echo > {}", "--template", template)

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    assert not entry.exists()
    assert not entry.parent.exists()


def test_use_changed_template(zet, notebooks, template):
    zet("journal", "--editor", "sleep 0.01 && touch {}", "--template", template)

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    assert entry.exists()


def test_dont_use_unchanged_template(zet, notebooks, template):
    zet("journal", "--editor", "sleep 0.01 && true", "--template", template)

    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"
    assert not entry.exists()
    assert not entry.parent.exists()


def test_dont_create_empty_journal_entry(zet, notebooks, template):
    today = date.today().isoformat()
    entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"

    zet("journal", "--editor", "sleep 0.01 && echo > {}", "--template", template)
    assert not entry.exists()
    assert not entry.parent.exists()

    zet("journal", "--editor", "sleep 0.01 && echo > {}")
    assert not entry.exists()
    assert not entry.parent.exists()

    zet("journal", "--editor", "true")
    assert not entry.exists()
    assert not entry.parent.exists()


def test_dont_create_empty_journal_entry_but_leave_old_entries(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}", "yesterday")
    zet("journal", "--editor", "sleep 0.01 && touch {}", "today")

    today = date.today().isoformat()
    yesterday = date.today() - timedelta(days=1)
    y_entry = notebooks / "content" / "my notebook" / "journal" / f"{yesterday}.md"
    t_entry = notebooks / "content" / "my notebook" / "journal" / f"{today}.md"

    assert y_entry.exists()
    assert not t_entry.exists()


def test_journal_directory(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}", "yesterday")
    zet(
        "journal",
        "--editor",
        "sleep 0.01 && echo foo > {}",
        "--journal-directory",
        "other:journaldir",
    )

    today = date.today().isoformat()
    entry = notebooks / "content" / "other" / "journaldir" / f"{today}.md"
    contents = entry.read_text().splitlines()
    assert contents == ["foo"]


def test_journal_directory_pointing_to_file(zet, notebooks):
    zet("journal", "--editor", "sleep 0.01 && echo foo > {}", "yesterday")
    with pytest.raises(sh.ErrorReturnCode):
        zet(
            "journal",
            "--editor",
            "sleep 0.01 && echo foo > {}",
            "--journal-directory",
            "other:note.md",
        )
