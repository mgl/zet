import os
from pathlib import Path

import pytest
import sh


def test_zet_run_default_notebook(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "pwd")
    lines = ret.splitlines()

    expected = [os.path.join(root, "my notebook")]
    assert lines == expected


def test_zet_run_notebook(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "--notebook", "other", "pwd")
    lines = ret.splitlines()

    expected = [os.path.join(root, "other")]
    assert lines == expected


def test_zet_run_notebook(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "--all", "pwd")
    lines = ret.splitlines()

    expected = [
        os.path.join(root, "my notebook"),
        os.path.join(root, "other")
    ]
    assert lines == expected


def test_zet_run_wrong_default_notebook_configured(zet, notebooks):
    root = os.path.join(notebooks, "content")

    dnf = Path(notebooks) / ".zet" / "default-notebook"
    dnf.write_text("wrong-notebook")

    ret = zet("run", "pwd")
    lines = ret.splitlines()

    expected = [os.path.join(root, "my notebook")]
    assert lines == expected


def test_zet_run_wrong_notebook(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        ret = zet("run", "-n", "wrong-notebook", "pwd")


def test_zet_run_one_missing_notebook(zet, zet_toml, notebooks):
    zet_toml["notebooks"].insert(0, {"name": "missing", "path": "content/missing"})
    zet_toml.write()

    ret = zet("run", "--all", "pwd", _ok_code=[2], _return_cmd=True)

    root = notebooks / "content"
    missing_path = root / "missing"
    exp_err = f"[Errno 2] No such file or directory: '{missing_path}'"
    exp_ok = [
        str(root / "my notebook"),
        str(root / "other")
    ]

    assert exp_err in ret.stderr.decode()
    assert exp_ok == ret.stdout.decode().splitlines()


def test_zet_run_cwd(zet, notebooks):
    ret = zet("run", "--cwd", ".", "pwd")
    lines = ret.splitlines()

    expected = [os.path.abspath(".")]
    assert lines == expected


def test_zet_run_notebook_and_all_arent_additive(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "--notebook", "other", "--all", "pwd")
    lines = ret.splitlines()

    expected = [
        os.path.join(root, "my notebook"),
        os.path.join(root, "other")
    ]
    assert lines == expected


def test_zet_run_cwd_is_additive_with_notebook(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "--notebook", "other", "--cwd", ".", "pwd")
    lines = ret.splitlines()

    expected = [
        os.path.join(root, "other"),
        os.path.abspath("."),
    ]
    assert lines == expected

def test_zet_run_cwd_is_additive_with_all(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("run", "--cwd", ".", "--all", "pwd")
    lines = ret.splitlines()

    expected = [
        os.path.join(root, "my notebook"),
        os.path.join(root, "other"),
        os.path.abspath("."),
    ]
    assert lines == expected
