import os

import pytest
import sh


@pytest.fixture
def newfile(tmp_path):
    f = tmp_path / "external_file.txt"
    f.write_text("hello")
    return f


def test_import_file_default_dest(zet, notebooks, newfile):
    ret = zet("import", str(newfile), _return_cmd=True)
    assert ret.exit_code == 0
    assert os.path.exists(
        os.path.join(notebooks, "content", "my notebook", newfile.name)
    )


def test_import_twice(zet, notebooks, newfile):
    newfile_2 = f"{newfile.stem}-1{newfile.suffix}"

    ret = zet("import", str(newfile), str(newfile), _return_cmd=True)
    assert ret.exit_code == 0
    assert os.path.exists(
        os.path.join(notebooks, "content", "my notebook", newfile.name)
    )
    assert os.path.exists(os.path.join(notebooks, "content", "my notebook", newfile_2))


def test_import_file_destination_notebook(zet, notebooks, newfile):
    ret = zet("import", "--destination", "other:", str(newfile), _return_cmd=True)
    assert ret.exit_code == 0
    assert os.path.exists(os.path.join(notebooks, "content", "other", newfile.name))


def test_import_file_destination_notebook_many_imports(zet, notebooks, newfile):
    newfile_2 = f"{newfile.stem}-1{newfile.suffix}"

    ret = zet(
        "import",
        "--destination",
        "other:",
        str(newfile),
        str(newfile),
        _return_cmd=True,
    )
    assert ret.exit_code == 0
    assert os.path.exists(os.path.join(notebooks, "content", "other", newfile.name))
    assert os.path.exists(os.path.join(notebooks, "content", "other", newfile_2))


def test_import_file_destination_file(zet, notebooks, newfile):
    ret = zet(
        "import", "--destination", "other:somefile.txt", str(newfile), _return_cmd=True
    )
    assert ret.exit_code == 0
    assert os.path.exists(os.path.join(notebooks, "content", "other", "somefile.txt"))


def test_import_file_destination_file_many_imports(zet, notebooks, newfile):
    ret = zet(
        "import",
        "--destination",
        "other:somefile.txt",
        str(newfile),
        str(newfile),
        str(newfile),
        _return_cmd=True,
    )
    assert ret.exit_code == 0
    assert os.path.exists(os.path.join(notebooks, "content", "other", "somefile.txt"))
    assert os.path.exists(os.path.join(notebooks, "content", "other", "somefile-1.txt"))
    assert os.path.exists(os.path.join(notebooks, "content", "other", "somefile-2.txt"))


def test_import_partial_fail(zet, notebooks, newfile):
    missing = newfile / "missing"

    with pytest.raises(sh.ErrorReturnCode):
        zet("import", str(missing), str(newfile))

    assert os.path.exists(
        os.path.join(notebooks, "content", "my notebook", newfile.name)
    )
