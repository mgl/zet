import os
from time import sleep


def test_ls_all_files(zet, notebooks, sorted_notes_selectors):
    ret = zet("ls")
    lines = ret.splitlines()
    assert sorted(lines) == sorted_notes_selectors


def test_ls_notebook(zet, notebooks, sorted_notes_selectors):
    ret = zet("ls", "other:")
    lines = ret.splitlines()
    assert sorted(lines) == [f for f in sorted_notes_selectors if f.startswith("other:")]


def test_ls_files(zet, notebooks):
    ret = zet("ls", "note.md")
    lines = ret.splitlines()
    assert lines == ["My Notebook:note.md"]


def test_ls_files_abs(zet, notebooks):
    ret = zet("ls", "note.md", "--absolute")
    lines = ret.splitlines()
    assert lines == [str(notebooks / "content/my notebook/note.md")]


def test_ls_no_file(zet, notebooks):
    ret = zet("ls", "no such note.md")
    lines = ret.splitlines()
    assert lines == []

def test_ls_sort_modify_time(zet, notebooks):
    lorem = notebooks / "content" / "other" / "lorem.md"
    lorem.touch()

    sleep(1)  # zet api list has a 1 second resolution
    new = notebooks / "content" / "other" / "00-totally_new_note.md"
    new.write_text("")

    ret = zet("ls", "other:", "--sort-modify-time")
    lines = ret.splitlines()
    assert lines[-1] == "other:00-totally_new_note.md"
    assert lines[-2] == "other:lorem.md"
