import os
import pytest
import sh


def test_api_notebooks(zet, notebooks):
    ret = zet("api", "notebooks")
    lines = ret.splitlines()

    expected = ["My Notebook", "other"]
    assert lines == expected


def test_api_notebooks_abspaths(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("api", "notebooks", "--absolute")
    lines = ret.splitlines()

    expected = [os.path.join(root, "my notebook"), os.path.join(root, "other")]
    assert lines == expected


def test_api_notebooks_relpaths(zet, notebooks):
    ret = zet("api", "notebooks", "--relative")
    lines = ret.splitlines()

    expected = ["content/my notebook", "content/other"]
    assert lines == expected


def test_api_notebooks_selectors(zet, notebooks):
    ret = zet("api", "notebooks", "--selector")
    lines = ret.splitlines()

    expected = ["My Notebook:", "other:"]
    assert lines == expected


def test_api_notebooks_all_fields(zet, notebooks, zet_toml):
    root = os.path.join(notebooks, "content")

    ret = zet("api", "notebooks", "--absolute", "--selector", "--relative", "--name")
    lines = ret.splitlines()

    expected = [
        f"My Notebook::{os.path.join(root, 'my notebook')}::content/my notebook::My Notebook:",
        f"other::{os.path.join(root, 'other')}::content/other::other:",
    ]
    assert lines == expected


def test_api_notebooks_all_fields_field_separator(zet, notebooks, zet_toml):
    root = os.path.join(notebooks, "content")

    ret = zet(
        "api",
        "notebooks",
        "--absolute",
        "--selector",
        "--relative",
        "--name",
        "--field-separator",
        "|",
    )
    lines = ret.splitlines()

    expected = [
        f"My Notebook|{os.path.join(root, 'my notebook')}|content/my notebook|My Notebook:",
        f"other|{os.path.join(root, 'other')}|content/other|other:",
    ]
    assert lines == expected


def test_api_notebooks_filter(zet, notebooks):
    ret = zet("api", "notebooks", "other")
    lines = ret.splitlines()

    expected = ["other"]
    assert lines == expected


def test_api_notebooks_filter_order(zet, notebooks):
    ret = zet("api", "notebooks", "other", "My Notebook")
    lines = ret.splitlines()

    expected = ["other", "My Notebook"]
    assert lines == expected


def test_api_notebooks_invaild_filter(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "notebooks", "invalid:")


def test_api_notebooks_config_order(zet, notebooks, zet_toml):
    ret = zet("api", "notebooks")
    lines = ret.splitlines()

    expected = ["My Notebook", "other"]
    assert lines == expected

    zet_toml["notebooks"] = list(reversed(zet_toml["notebooks"]))
    zet_toml.write()

    ret = zet("api", "notebooks")
    lines = ret.splitlines()

    expected = ["other", "My Notebook"]
    assert lines == expected
