import os
import pytest
import sh


TESTED_SELECTORS = [
    ("My Notebook:foo.md", "{prefix}/my notebook/foo.md"),
    ("other:note.md", "{prefix}/other/note.md"),
    ("My Notebook:", "{prefix}/my notebook"),
    ("My Notebook:dir", "{prefix}/my notebook/dir"),
    ("My Notebook:dir/", "{prefix}/my notebook/dir"),
    ("My Notebook:subdir", "{prefix}/my notebook/subdir"),
    ("My Notebook:subdir/", "{prefix}/my notebook/subdir"),
    ("My Notebook:subdir/subnote.md", "{prefix}/my notebook/subdir/subnote.md"),
    ("My Notebook:/foo", "{prefix}/my notebook/foo"),
    ("My Notebook:/dir/foo", "{prefix}/my notebook/dir/foo"),
    ("/foo", "/foo"),
    ("foo.md", "{prefix}/my notebook/foo.md"),
    ("./foo.md", "{prefix}/my notebook/foo.md"),
]


@pytest.mark.parametrize("selector,expected", TESTED_SELECTORS)
def test_convert_selectors(zet, notebooks, selector, expected):
    root = os.path.join(notebooks, "content")

    ret = zet("api", "paths", selector)
    lines = ret.splitlines()

    assert lines == [expected.format(prefix=root)]


@pytest.mark.parametrize("selector,expected", TESTED_SELECTORS)
def test_convert_selectors_relative(zet, notebooks, selector, expected):
    ret = zet("api", "paths", "--relative", selector)
    lines = ret.splitlines()

    assert lines == [expected.format(prefix="content")]


@pytest.mark.parametrize(
    "selector",
    [
        "foobar:",
        "foobar:foo.md",
        "not-a-notebook:",
        "not-a-notebook:not-a-note.md",
    ],
)
def test_convert_selectors_fail(zet, notebooks, selector):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "paths", selector)
