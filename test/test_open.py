import pytest
import sh


def test_open(zet, notebooks):
    ret = zet("open", "--program", "cat", "other:note.md")
    assert ["note"] == ret.splitlines()

    ret = zet("open", "--program", "cat", "other:note-1.md")
    assert ["note-1"] == ret.splitlines()


def test_open_case_insensitive(zet, notebooks):
    ret = zet("open", "--program", "cat", "--ignore-case", "other:NOTE.md")
    assert ["note"] == ret.splitlines()

    ret = zet("open", "--program", "cat", "--ignore-case", "other:NOTE-1.md")
    assert ["note-1"] == ret.splitlines()


def test_open_partial_match(zet, notebooks):
    ret = zet("open", "--program", "cat", "other:note", _in="1")
    query = ret.splitlines()
    assert query == [
        "More than one file matches. Which one to open?",
        "[1] content/other/note.md",
        "[2] content/other/note-1.md",
        "> note",  # note is output of cat - contents of a note
    ]

    ret = zet("open", "--program", "cat", "other:note", _in="2")
    query = ret.splitlines()
    assert query == [
        "More than one file matches. Which one to open?",
        "[1] content/other/note.md",
        "[2] content/other/note-1.md",
        "> note-1",  # note is output of cat - contents of a note
    ]


def test_open_no_match(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("open", "--program", "cat", "other:missing-note.md")
