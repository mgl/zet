import pytest
import itertools


def match_notes(call, notes):
    args = call["args"]
    matched = []
    for note in notes:
        if note in args:
            matched.append(note)
    return matched


def flatten(lst):
    return itertools.chain.from_iterable(lst)


def test_search_grep_arguments(zet, notebooks, monkeyprogram, sorted_notes):
    grep = monkeyprogram("grep", add_to_path=True)
    zet("search", "query", "--grep-program", "grep")

    note_paths = [str(notebooks / n) for n in sorted_notes]
    ms = flatten(grep.for_each_call(lambda c: match_notes(c, note_paths)))
    assert sorted(set(ms)) == note_paths


def test_search_rg_arguments(zet, notebooks, monkeyprogram, sorted_notes):
    rg = monkeyprogram("rg", add_to_path=True)
    zet("search", "query", "--grep-program", "rg")

    note_paths = [str(notebooks / n) for n in sorted_notes]
    ms = flatten(rg.for_each_call(lambda c: match_notes(c, note_paths)))
    assert sorted(set(ms)) == note_paths


def test_search_group_results(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 .*{note1}", [f"{note1}:11:a q1 b", f"{note1}:12:c q1 d"])
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:c q1 d")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1 b",
        "    c q1 d",
        "",
        "other:note.md:",
        "    c q1 d",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_group_results_absolute(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 .*{note1}", [f"{note1}:11:a q1 b", f"{note1}:12:c q1 d"])
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:c q1 d")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep", "--absolute")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        f"{note1}:",
        "    a q1 b",
        "    c q1 d",
        "",
        f"{note2}:",
        "    c q1 d",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_keep_absolute_paths_outside_of_notebooks(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "note.md"

    note2.write_text("c q1 d")

    grep.on_request(f".* -e q1 .*{note1}", [f"{note1}:11:a q1 b", f"{note1}:12:c q1 d"])
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:1:c q1 d")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep", "-s", "other:", "-s", note2)
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        f"other:lorem.md:",
        "    a q1 b",
        "    c q1 d",
        "",
        f"{note2}:",
        "    c q1 d",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_file_list(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 .*{note1}", f"{note1}:11:a q1")
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:b q1")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep", "--files-with-matches")
    lines = ret.splitlines()

    expected_lines = ["other:lorem.md", "other:note.md"]

    assert grep.called
    assert lines == expected_lines


def test_search_file_list_absolute(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 .*{note1}", f"{note1}:11:a q1")
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:b q1")
    grep.monkeypatch()

    ret = zet(
        "search", "q1", "--grep-program", "grep", "--files-with-matches", "--absolute"
    )
    lines = ret.splitlines()

    expected_lines = [str(note1), str(note2)]

    assert grep.called
    assert lines == expected_lines


def test_search_truncate(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(
        f".* -e q1 .*{note1}",
        [
            f"{note1}:11:a q1",
            f"{note1}:12:b q1",
            f"{note1}:13:c q1",
            f"{note1}:14:d q1",
            f"{note1}:15:e q1",
            f"{note1}:16:f q1",
            f"{note1}:17:g q1",
        ],
    )
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:a q1")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "    b q1",
        "    c q1",
        "    d q1",
        "    e q1",
        "(... result truncated, run 'zet search --show-all' to show all matched entries ...)",
        "",
        "other:note.md:",
        "    a q1",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_no_truncate(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(
        f".* -e q1 .*{note1}",
        [
            f"{note1}:11:a q1",
            f"{note1}:12:b q1",
            f"{note1}:13:c q1",
            f"{note1}:14:d q1",
            f"{note1}:15:e q1",
            f"{note1}:16:f q1",
            f"{note1}:17:g q1",
        ],
    )
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:a q1")
    grep.monkeypatch()

    ret = zet("search", "q1", "--grep-program", "grep", "--show-all")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "    b q1",
        "    c q1",
        "    d q1",
        "    e q1",
        "    f q1",
        "    g q1",
        "",
        "other:note.md:",
        "    a q1",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_queries_logical_and_grep(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 .*{note1}", f"{note1}:11:a q1")
    grep.on_request(f".* -e q2 .*{note1}", [f"{note1}:12:b q2", f"{note1}:13:c q2"])
    grep.on_request(f".* -e q3 .*{note1}", f"{note1}:14:d q3")
    grep.on_request(f".* -e q1 .*{note2}", f"{note2}:11:c q1")
    grep.on_request(f".* -e q3 .*{note2}", f"{note2}:14:d q3")
    grep.monkeypatch()

    ret = zet("search", "q1", "q2", "q3", "--grep-program", "grep")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "    b q2",
        "    c q2",
        "    d q3",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_queries_logical_or_grep(zet, notebooks, monkeyprogram):
    grep = monkeyprogram("grep", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    grep.on_request(f".* -e q1 -e q2 -e q3 .*{note1}", f"{note1}:11:a q1")
    grep.on_request(
        f".* -e q1 -e q2 -e q3 .*{note2}", [f"{note2}:11:a q2", f"{note2}:12:b q3"]
    )
    grep.monkeypatch()

    ret = zet("search", "q1", "q2", "q3", "--grep-program", "grep", "--or")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "",
        "other:note.md:",
        "    a q2",
        "    b q3",
    ]
    # fmt: on

    assert grep.called
    assert lines == expected_lines


def test_search_queries_logical_and_rg(zet, notebooks, monkeyprogram):
    rg = monkeyprogram("rg", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    rg.on_request(".* -e q1 ", [f"{note1}:11:a q1", f"{note2}:11:a q1"])
    rg.on_request(".* -e q2 ", [f"{note1}:12:b q2", f"{note1}:13:c q2"])
    rg.on_request(".* -e q3 ", [f"{note1}:14:d q3", f"{note2}:12:b q3"])
    rg.monkeypatch()

    ret = zet("search", "q1", "q2", "q3", "--grep-program", "rg")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "    b q2",
        "    c q2",
        "    d q3",
    ]
    # fmt: on

    assert rg.called
    assert lines == expected_lines


def test_search_queries_logical_or_rg(zet, notebooks, monkeyprogram):
    rg = monkeyprogram("rg", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"
    note2 = notebooks / "content/other/note.md"

    rg.on_request(
        ".* -e q1 -e q2 -e q3 ",
        [f"{note1}:11:a q1", f"{note2}:11:a q2", f"{note2}:12:b q3"],
    )
    rg.monkeypatch()

    ret = zet("search", "q1", "q2", "q3", "--grep-program", "rg", "--or")
    lines = ret.splitlines()

    # fmt: off
    expected_lines = [
        "other:lorem.md:",
        "    a q1",
        "",
        "other:note.md:",
        "    a q2",
        "    b q3",
    ]
    # fmt: on

    assert rg.called
    assert lines == expected_lines


def test_search_display_result_only_once_logical_and(zet, notebooks, monkeyprogram):
    rg = monkeyprogram("rg", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"

    rg.on_request(".*", f"{note1}:11:a q1 q2")
    rg.monkeypatch()

    ret = zet("search", "q1", "q2", "--grep-program", "rg")
    lines = ret.splitlines()

    # fmt: off
    # there was a bug that for logical and such query caused double printing "a
    # q1 q2" line
    expected_lines = [
        "other:lorem.md:",
        "    a q1 q2",
    ]
    # fmt: on

    assert rg.called
    assert lines == expected_lines


def test_search_display_result_only_once_logical_or(zet, notebooks, monkeyprogram):
    rg = monkeyprogram("rg", add_to_path=True)

    note1 = notebooks / "content/other/lorem.md"

    rg.on_request(".*", f"{note1}:11:a q1 q2")
    rg.monkeypatch()

    ret = zet("search", "q1", "q2", "--grep-program", "rg", "--or")
    lines = ret.splitlines()

    # fmt: off
    # there was a bug that for logical and such query caused double printing "a
    # q1 q2" line
    expected_lines = [
        "other:lorem.md:",
        "    a q1 q2",
    ]
    # fmt: on

    assert rg.called
    assert lines == expected_lines
