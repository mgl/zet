def test_version(zet):
    ret = zet("--version")
    name, _, ver = ret.splitlines()[0].partition(" ")
    assert name == "zet"
    assert ver
