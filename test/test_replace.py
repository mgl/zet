import pytest
import sh


@pytest.mark.parametrize(
    "from_,to,matches,options",
    [
        ("Lorem", "lorem", ["Lorem"], []),
        ("lorem", "foo", [], []),
        ("lorem", "foo", ["Lorem"], ["--ignore-case"]),
        ("/foo/bar/baz/", "/kimi/no/na/wa/", ["/foo/bar/baz/"], []),
        ("dolor", "color", ["dolor"], []),
        ("abc", "dolor", [], []),
        ("cil[^ ]*", "ok", ["cillum"], []),
        ("su[^ ]*", "ok", ["sum", "sunt"], []),
        ("SU[^ ]*", "ok", ["sum", "sunt"], ["--ignore-case"]),
    ],
)
def test_replace_patterns(zet, notebooks, from_, to, matches, options):
    note = notebooks / "content" / "other" / "lorem.md"

    exp = note.read_text()
    for m in matches:
        exp = exp.replace(m, to)

    zet("replace", from_, to, "other:lorem.md", *options)
    assert note.read_text() == exp


def test_replace_all_files(zet, notebooks, sorted_notes):
    from_ = "Lorem"
    to = "Wild Thing"

    expected = {}
    for note in sorted_notes:
        path = notebooks / note
        expected[path] = path.read_text().replace(from_, to)

    zet("replace", from_, to)

    for path, exp in expected.items():
        assert path.read_text() == exp


def test_replace_single_notebook(zet, notebooks, sorted_notes):
    from_ = "Lorem"
    to = "Wild Thing"

    expected = {}
    for note in sorted_notes:
        if note.startswith("content/other"):
            path = notebooks / note
            expected[path] = path.read_text().replace(from_, to)

    # sanity check
    assert expected, "TEST ERROR: MISSING NOTEBOOK: other"

    zet("replace", from_, to, "other:")

    for path in expected:
        assert path.read_text() == expected[path]


def test_replace_interactive(zet, notebooks):
    note = notebooks / "content" / "other" / "lorem.md"

    from_ = "Lorem"
    to = "Foobar"

    exp = note.read_text().replace(from_, to)

    ret = zet(
        "replace", from_, to, "other:lorem.md", "--interactive", "--no-color", _in="y"
    )
    out = ret.splitlines()

    # fmt: off
    assert "--- Current version of other:lorem.md" in out
    assert "+++ After replacement of other:lorem.md" in out
    assert "-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod" in out
    assert "+Foobar ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod" in out
    assert ">> Accept changes in other:lorem.md? ([Y]es / [N]o / [Q]uit)" in out
    assert "Applied other:lorem.md" in out
    # fmt: on

    assert note.read_text() == exp


def test_replace_interactive_no(zet, notebooks):
    note = notebooks / "content" / "other" / "lorem.md"

    from_ = "Lorem"
    to = "Foobar"

    exp = note.read_text()

    ret = zet(
        "replace", from_, to, "other:lorem.md", "--interactive", "--no-color", _in="n"
    )
    out = ret.splitlines()

    # fmt: off
    assert "--- Current version of other:lorem.md" in out
    assert "+++ After replacement of other:lorem.md" in out
    assert "-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod" in out
    assert "+Foobar ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod" in out
    assert ">> Accept changes in other:lorem.md? ([Y]es / [N]o / [Q]uit)" in out
    assert "Rejected other:lorem.md" in out
    # fmt: on

    assert note.read_text() == exp


def test_replace_interactive_quit(zet, notebooks):
    note = notebooks / "content" / "other" / "lorem.md"

    from_ = "Lorem"
    to = "Foobar"

    exp = note.read_text()

    with pytest.raises(sh.ErrorReturnCode):
        ret = zet("replace", from_, to, "other:lorem.md", "--interactive", _in="q")

    assert note.read_text() == exp
