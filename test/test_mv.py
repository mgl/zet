import pytest
import sh


def test_mv(zet, notebooks):
    zet("mv", "other:note.md", "other:note2.md")
    old = notebooks / "content" / "other" / "note.md"
    new = notebooks / "content" / "other" / "note2.md"
    assert not old.exists()
    assert new.is_file()


def test_mv_default_notebook(zet, notebooks):
    zet("mv", "note.md", "note2.md")
    old = notebooks / "content" / "my notebook" / "note.md"
    new = notebooks / "content" / "my notebook" / "note2.md"
    assert not old.exists()
    assert new.is_file()


def test_mv_between_notebooks(zet, notebooks):
    zet("mv", "other:note.md", "My Notebook:note2.md")
    old = notebooks / "content" / "other" / "note.md"
    new = notebooks / "content" / "my notebook" / "note2.md"
    assert not old.exists()
    assert new.is_file()


def test_mv_to_existing_note(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "other:note.md", "other:note.md")

    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "other:note.md", "other:note-1.md")

    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "other:note.md", "My Notebook:note.md")

    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "other:note.md", "note.md")


def test_mv_missing_note(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "missing_note.md", "other:note2.md")


def test_mv_to_title(zet, notebooks):
    note = notebooks / "content" / "other" / "note.md"
    note.write_text("# My Note Title\n")

    zet("mv", "other:note.md", "--to-title")
    old = notebooks / "content" / "other" / "note.md"
    new = notebooks / "content" / "other" / "my-note-title.md"
    assert not old.exists()
    assert new.is_file()


def test_mv_to_title_fallback_when_note_exists(zet, notebooks):
    note = notebooks / "content" / "other" / "note.md"
    note.write_text("# My Note Title\n")

    old = notebooks / "content" / "other" / "note.md"
    existing = notebooks / "content" / "other" / "my-note-title.md"
    new = notebooks / "content" / "other" / "my-note-title-1.md"
    existing.touch()

    zet("mv", "other:note.md", "--to-title")

    assert not old.exists()
    assert new.is_file()


def test_mv_to_title_fail_no_title(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("mv", "other:note.md", "--to-title")
