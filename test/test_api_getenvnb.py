import os
import pytest
import sh


@pytest.mark.parametrize(
    "var,nb,expected",
    [
        ("path", "my notebook", "content/my notebook"),
        ("PATH", "My Notebook", "content/my notebook"),
        ("PATH", "MY NOTEBOOK", "content/my notebook"),
        ("path", "other", "content/other"),
        ("name", "my notebook", "My Notebook"),
        ("name", "other", "other"),
    ],
)
def test_getenvnb(zet, notebooks, var, nb, expected):
    ret = zet("api", "getenvnb", var, nb)
    lines = ret.splitlines()
    assert lines == [expected]


@pytest.mark.parametrize(
    "var,nb",
    [
        ("nonexisting", "my notebook"),
        ("path", "nonexisting"),
    ],
)
def test_getenvnb_errors(zet, notebooks, var, nb):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "getenvnb", var, nb)
