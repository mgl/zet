import os
import socket
import getpass
from datetime import datetime as dt
from pathlib import Path

import pytest
import sh

TEMPLATE = """
template start
{title}
{dt}
{host}
{user}
{input}
template end
""".strip()


@pytest.fixture
def template(tmp_path):
    f = tmp_path / "template.txt"
    f.write_text(TEMPLATE)
    return f


def test_note(zet, notebooks, tmp_path):
    out = tmp_path / "out.txt"
    zet("note", "--editor", "sleep 0.01 && echo foo > {}", out)
    contents = out.read_text().splitlines()
    assert contents == ["foo"]


def test_note_stdin(zet, notebooks, tmp_path):
    out = tmp_path / "out.txt"
    zet("note", "--stdin", out, _in="bar")
    contents = out.read_text().splitlines()
    assert contents == ["bar"]


def test_note_template(zet, notebooks, template, tmp_path):
    out = tmp_path / "out.txt"
    zet(
        "note",
        "--title",
        "Note's Title",
        "--editor",
        "sleep 0.01 && touch {}",
        "--template",
        template,
        out,
    )
    contents = out.read_text().splitlines()
    assert contents[0] == "template start"
    assert contents[1] == "Note's Title"
    assert dt.fromisoformat(contents[2])
    assert contents[3] == socket.gethostname()
    assert contents[4] == getpass.getuser()
    assert contents[5] == ""
    assert contents[6] == "template end"


def test_note_template_stdin(zet, notebooks, template, tmp_path):
    out = tmp_path / "out.txt"
    zet(
        "note",
        "--title",
        "Note's Title",
        "--editor",
        "sleep 0.01 && touch {}",
        "--template",
        template,
        "--stdin",
        out,
        _in="my input",
    )
    contents = out.read_text().splitlines()
    assert contents[0] == "template start"
    assert contents[1] == "Note's Title"
    assert dt.fromisoformat(contents[2])
    assert contents[3] == socket.gethostname()
    assert contents[4] == getpass.getuser()
    assert contents[5] == "my input"
    assert contents[6] == "template end"


def test_note_selector(zet, notebooks):
    sel = "other:newnote"
    selpath = zet("api", "paths", sel).splitlines()[0]
    assert not os.path.exists(selpath)

    zet("note", "--editor", "sleep 0.01 && echo foo > {}", sel)
    assert os.path.exists(selpath)


def test_note_selector_is_dir(zet, notebooks):
    sel = "other:newnote/"
    selpath = zet("api", "paths", sel + "note.md").splitlines()[0]
    assert not os.path.exists(selpath)

    zet("note", "--editor", "sleep 0.01 && echo foo > {}", sel)
    assert os.path.exists(selpath)


def test_new_note_each_time_when_path_is_dir(zet, notebooks):
    sel = "other:somedir/"
    selpath = zet("api", "paths", sel).splitlines()[0]
    zet("note", "--editor", "sleep 0.01 && echo foo0 > {}", sel)
    zet("note", "--editor", "sleep 0.01 && echo foo1 > {}", sel)
    zet("note", "--editor", "sleep 0.01 && echo foo2 > {}", sel)

    note1 = Path(selpath) / "note.md"
    note2 = Path(selpath) / "note-1.md"
    note3 = Path(selpath) / "note-2.md"

    assert note1.is_file()
    assert note2.is_file()
    assert note3.is_file()
    assert note1.read_text().splitlines() == ["foo0"]
    assert note2.read_text().splitlines() == ["foo1"]
    assert note3.read_text().splitlines() == ["foo2"]


def test_note_inherit_extension_from_template(zet, notebooks, template):
    sel = "other:somedir/"
    selpath = zet("api", "paths", sel).splitlines()[0]
    zet("note", "--editor", "sleep 0.01 && echo foo0 > {}", "--template", template, sel)

    note = Path(selpath) / "note.txt"
    assert note.is_file()


def test_append_to_note(zet, notebooks):
    sel = "other:somedir/somenote"
    selpath = zet("api", "paths", sel).splitlines()[0]
    zet("note", "--editor", "sleep 0.01 && echo foo0 > {}", sel)
    zet("note", "--editor", "sleep 0.01 && echo foo1 > {}", sel)
    zet("note", "--editor", "sleep 0.01 && echo foo2 > {}", sel)

    note = Path(selpath)
    assert note.is_file()
    assert note.read_text().splitlines() == ["foo0", "foo1", "foo2"]


def test_note_bad_notebook(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("zet", "--editor", "sleep 0.01 && echo foo0 > {}", "bad notebook:foo")


def test_note_no_edit(zet, notebooks, template):
    sel = "other:somedir/"
    selpath = zet("api", "paths", sel).splitlines()[0]

    with pytest.raises(sh.ErrorReturnCode):
        zet("zet", "--editor", "true", "--template", template, sel)

    # this checks that parent directory of potential note isn't even created
    assert not Path(selpath).exists()


def test_note_use_title(zet, notebooks):
    zet(
        "note",
        "--editor",
        "sleep 0.01 && echo '# My New Note'> {}",
    )
    new = notebooks / "content" / "my notebook" / "my-new-note.md"
    contents = new.read_text().splitlines()
    assert contents == ["# My New Note"]


def test_note_and_then(zet, notebooks):
    out = (
        zet(
            "note",
            "--editor",
            "sleep 0.01 && echo '# My New Note'> {}",
            "--and-then",
            "echo {}",
        )
        .splitlines()[0]
    )
    new = notebooks / "content" / "my notebook" / "my-new-note.md"
    assert out == str(new)


def test_append_to_note_with_and_then(zet, notebooks):
    sel = "other:somedir/somenote"
    selpath = zet("api", "paths", sel).splitlines()[0]
    out1 = (
        zet(
            "note",
            "--editor",
            "sleep 0.01 && echo foo0 > {}",
            "--and-then",
            "echo {}",
            sel,
        )
        .splitlines()[0]
    )
    out2 = (
        zet(
            "note",
            "--editor",
            "sleep 0.01 && echo foo1 > {}",
            "--and-then",
            "echo {}",
            sel,
        )
        .splitlines()[0]
    )

    note = Path(selpath)
    assert note.is_file()
    assert out1 == str(note)
    assert out2 == str(note)
    assert note.read_text().splitlines() == ["foo0", "foo1"]


def test_note_and_then_command_fail(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet(
            "note",
            "--editor",
            "sleep 0.01 && echo '# My New Note'> {}",
            "--and-then",
            "false",
        )

    new = notebooks / "content" / "my notebook" / "my-new-note.md"
    assert new.is_file()
