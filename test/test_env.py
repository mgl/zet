from typing import List, Mapping

import shutil


def parse_env(lines: List[str]) -> Mapping[str, str]:
    env = {}
    for ln in lines:
        key, _, val = ln.strip().partition(":")
        key = key.strip().strip('"')
        val = val.strip().rstrip(",").strip('"')
        if not ln or not val:
            continue

        assert key not in env
        env[key] = val
    return env


def test_env(zet, notebooks):
    env = parse_env(zet("env").splitlines())

    assert env["ZET_ROOT"] == str(notebooks)
    assert env["ZET_DIR"] == str(notebooks / ".zet")
    assert env["ZET_CONFIG"] == str(notebooks / ".zet" / "zet.toml")
    assert env["ZET_NOTEBOOKS"] == "MY_NOTEBOOK:OTHER"
    assert env["ZET_NOTEBOOK_MY_NOTEBOOK_PATH"] == "content/my notebook"
    assert env["ZET_NOTEBOOK_MY_NOTEBOOK_NAME"] == "My Notebook"
    assert env["ZET_NOTEBOOK_OTHER_PATH"] == "content/other"
    assert env["ZET_NOTEBOOK_OTHER_NAME"] == "other"

    assert not "ZET_NO_HOOKS" in env


def test_env_include_user_environment(zet, notebooks, monkeypatch, tmp_path):
    zet_root = tmp_path / "envtest"
    shutil.copytree(str(notebooks), str(zet_root))

    monkeypatch.setenv("ZET_ROOT", str(zet_root))
    monkeypatch.setenv("ZET_NO_HOOKS", "1")

    env = parse_env(zet("env").splitlines())

    assert env["ZET_ROOT"] == str(zet_root)
    assert env["ZET_NO_HOOKS"] == "1"
