import os
import pytest
import sh
import glob
from pathlib import Path


def test_list_selectors(zet, notebooks):
    ret = zet("api", "list", "My Notebook:", "other:")
    lines = ret.splitlines()

    mn_list = glob.glob(
        os.path.join(notebooks, "content", "my notebook", "**", "*"), recursive=True
    )
    ot_list = glob.glob(
        os.path.join(notebooks, "content", "other", "**", "*"), recursive=True
    )

    globs = [mn_list + ot_list]
    expected = {p for p in mn_list + ot_list if os.path.isfile(p)}
    assert set(lines) == expected


def test_list_files(zet, notebooks):
    ret = zet("api", "list", "My Notebook:note.md")
    lines = ret.splitlines()

    expected = os.path.join(notebooks, "content", "my notebook", "note.md")
    assert lines == [expected]


def test_list_files_twice(zet, notebooks):
    ret = zet("api", "list", "My Notebook:note.md", "My Notebook:note.md")
    lines = ret.splitlines()

    expected = os.path.join(notebooks, "content", "my notebook", "note.md")
    assert lines == [expected, expected]


def test_list_symlinks(zet, notebooks):
    ret = zet("api", "list", "My Notebook:note-symlink.md")
    lines = ret.splitlines()

    expected = os.path.join(notebooks, "content", "my notebook", "note-symlink.md")
    assert lines == [expected]


def test_list_subdirs(zet, notebooks):
    ret = zet("api", "list", "My Notebook:subdir")
    lines = ret.splitlines()

    paths = glob.glob(
        os.path.join(notebooks, "content", "my notebook", "subdir", "**", "*"),
        recursive=True,
    )

    expected = {p for p in paths if os.path.isfile(p)}
    assert set(lines) == expected


def test_list_subdirs_symlinks(zet, notebooks):
    ret = zet("api", "list", "My Notebook:subdir-symlink")
    lines = ret.splitlines()

    paths = glob.glob(
        os.path.join(notebooks, "content", "my notebook", "subdir-symlink", "**", "*"),
        recursive=True,
    )

    expected = {p for p in paths if os.path.isfile(p)}
    assert set(lines) == expected


def test_list_unexisting_notebooks(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "list", "invalid:", "other:")


def test_list_relative(zet, notebooks):
    ret = zet("api", "list", "--relative", "My Notebook:note.md")
    lines = ret.splitlines()

    expected = os.path.join("content", "my notebook", "note.md")
    assert lines == [expected]


def test_list_selector_notation(zet, notebooks):
    ret = zet("api", "list", "--selector", "other:")
    lines = ret.splitlines()

    expected = [
        "other:lorem.md",
        "other:note-1.md",
        "other:note.md",
    ]
    assert lines == expected


def test_list_selector_notation_outside_notebook(zet, notebooks):
    fpabs = notebooks / "foo.md"
    fpabs.write_text("")

    ret = zet("api", "list", "--selector", str(fpabs))
    lines = ret.splitlines()

    expected = [str(fpabs)]
    assert lines == expected


@pytest.mark.parametrize(
    "file_,title,lines",
    [
        ("title.md", "My Title", ["# My Title"]),
        ("title.md", "", []),
        ("title.adoc", "My Title", ["= My Title"]),
        ("title.adoc", "", []),
        ("title.org", "My Title", ["#+TITLE: My Title"]),
        ("title.org", "", []),
        ("some-file.txt", "", ["# My Title"]),
    ],
)
def test_list_titles(zet, notebooks, file_, title, lines):
    fp = notebooks / "content" / file_
    fp.write_text("\n".join(lines))

    ret = zet("api", "list", "--title", str(fp)).splitlines()
    assert ret == [title]


@pytest.mark.parametrize(
    "title,exp",
    [
        ("My Title", "my-title"),
        ("   My     Title   ", "my-title"),
        ("My Title(or is it? really) hmm", "my-title-or-is-it-really-hmm"),
        ("", ""),
    ],
)
def test_list_canonical_titles(zet, notebooks, title, exp):
    fp = notebooks / "content" / "file.md"
    fp.write_text("# " + title)

    ret = zet("api", "list", "--canonical-title", str(fp)).splitlines()
    assert ret == [exp]


def test_list_titles_missing_file(zet, notebooks):
    ret = (
        zet("api", "list", "--title", "some-missing-file").splitlines()
    )
    assert ret == []


@pytest.mark.parametrize(
    "switches,output",
    [
        (("--absolute"), "{fpabs}"),
        (("--relative",), "{fprel}"),
        (("--selector",), "{fpsel}"),
        (("--absolute", "--title"), "{title}::{fpabs}"),
        (("--relative", "--title"), "{title}::{fprel}"),
        (("--absolute", "--title", "--canonical-title"), "{canonical}::{title}::{fpabs}"),
        (("--absolute", "--relative", "--title", "--canonical-title"), "{canonical}::{title}::{fpabs}::{fprel}"),
        (("--absolute", "--relative", "--selector", "--title", "--canonical-title"), "{canonical}::{title}::{fpabs}::{fprel}::{fpsel}"),
        (("--absolute", "--title", "--field-separator", ";"), "{title};{fpabs}"),
        (("--absolute", "--relative", "--title", "--field-separator", ";"), "{title};{fpabs};{fprel}"),
        (("--created", "--modified", "--accessed", "--canonical-title"), "{access}::{modify}::{create}::{canonical}"),
    ],
)
def test_list_formatting_switches(zet, notebooks, switches, output):
    title = "My File Title"
    canonical = "my-file-title"
    text = "This is text"
    fprel = Path("content") / "other" / "newnote.md"
    fpabs = notebooks / fprel
    fpsel = "other:newnote.md"

    fpabs.write_text("\n".join(["# " + title, text]))

    create = int(fpabs.stat().st_ctime)
    modify = int(fpabs.stat().st_mtime)
    access = int(fpabs.stat().st_atime)

    ret = zet("api", "list", *switches, str(fpabs)).splitlines()
    exp = output.format(
        fpabs=fpabs,
        fprel=fprel,
        fpsel=fpsel,
        title=title,
        canonical=canonical,
        create=create,
        modify=modify,
        access=access,
    )
    assert ret == [exp]


def test_list_path_and_no_file_title(zet, notebooks):
    fp = notebooks / "newnote.md"
    fp.touch()

    ret = zet("api", "list", "--title", "--absolute", str(fp)).splitlines()
    assert ret == [f"::{fp}"]
