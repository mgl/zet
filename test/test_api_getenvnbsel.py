import os
import pytest
import sh


@pytest.mark.parametrize(
    "var,sel,expected",
    [
        ("path", "My Notebook:", "content/my notebook"),
        ("path", "other:", "content/other"),
        ("name", "My Notebook:", "My Notebook"),
        ("name", "other:", "other"),
        ("path", "My Notebook:foo.md", "content/my notebook"),
        ("path", "My Notebook:foo", "content/my notebook"),
        ("path", "My Notebook:foo/", "content/my notebook"),
        ("path", "My Notebook:foo/bar.md", "content/my notebook"),
        ("path", "foo", "content/my notebook"),
        ("path", "foo.md", "content/my notebook"),
        ("path", "foo/bar.md", "content/my notebook"),
    ],
)
def test_getenvnbsel(zet, notebooks, var, sel, expected):
    ret = zet("api", "getenvnbsel", var, sel)
    lines = ret.splitlines()
    assert lines == [expected]


@pytest.mark.parametrize(
    "var,sel",
    [
        ("nonexisting", "My Notebook:"),
        ("path", "my notebook:"),
        ("path", "nonexisting:"),
    ],
)
def test_getenvnbsel_errors(zet, notebooks, var, sel):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "getenvnb", var, sel)
