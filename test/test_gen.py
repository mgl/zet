import re

import pytest


@pytest.mark.parametrize(
    "fmt,pattern",
    [
        ("%u", r"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"),
        ("%su", r"^[0-9a-f]{8}$"),
        ("%d", r"^\d{14}"),
        ("%r", r"^\d+"),
        ("foo-%r-bar-%d", r"^foo-\d+-bar-\d{14}"),
        ("%b", "%b"),
    ],
)
def test_gen(zet, notebooks, fmt, pattern):
    ret = zet("gen", fmt)
    lines = ret.splitlines()
    assert len(lines) == 1
    assert (
        re.match(pattern, lines[0]) is not None
    ), f"'{lines[0]}' doesn't match a regular expression: {pattern}"
