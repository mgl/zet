import pytest
import sh


def test_default_notebook_file_missing(zet, notebooks):
    ret = zet("default-notebook")
    lines = ret.splitlines()
    assert lines == ["My Notebook"]


def test_default_notebook_set(zet, notebooks):
    zet("default-notebook", "My Notebook")
    assert ["My Notebook"] == zet("default-notebook").splitlines()

    zet("default-notebook", "other")
    assert ["other"] == zet("default-notebook").splitlines()


def test_default_notebook_set_invalid(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("default-notebook", "bad notebook")
