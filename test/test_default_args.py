import os

import sh
import pytest


@pytest.mark.parametrize(
    "defaults,expected_cli",
    [
        ({}, []),
        ({"boolean": True}, ["--boolean"]),
        ({"boolean": False}, []),
        ({"integer": 1}, ["--integer", "1"]),
        ({"float": 1.1}, ["--float", "1.1"]),
        ({"string": "foo"}, ["--string", "foo"]),
        (
            {"array": ["foo", "bar", 11]},
            ["--array", "foo", "--array", "bar", "--array", "11"],
        ),
        ({"arguments": []}, []),
        ({"arguments": "foo"}, ["foo"]),
        ({"arguments": ["foo", "bar"]}, ["foo", "bar"]),
        # check that order of elements is preserved
        (
            {"c": "bar", "b": True, "d": "baz", "a": "foo"},
            ["--c", "bar", "--b", "--d", "baz", "--a", "foo"],
        ),
        ({"home": "~/foo"}, ["--home", os.path.join(os.path.expanduser("~"), "foo")]),
    ],
)
def test_default_args(zet, zet_toml, monkeyprogram, defaults, expected_cli):
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)
    zet_toml["command"] = {"fake-command": {"defaults": defaults}}
    zet_toml.write()

    ret = zet("fake-command", _return_cmd=True)

    assert not ret.stdout
    assert not ret.stderr

    assert fake_command.called
    assert len(fake_command.calls) == 1
    assert fake_command.calls[0]["args"] == expected_cli


def test_default_args_float(zet, zet_toml, monkeyprogram):
    f = 1.1
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)
    zet_toml["command"] = {"fake-command": {"defaults": {"boolean": f}}}
    zet_toml.write()

    ret = zet("fake-command", _return_cmd=True)

    assert not ret.stdout
    assert not ret.stderr

    assert fake_command.called
    assert len(fake_command.calls) == 1
    assert fake_command.calls[0]["args"][0] == "--boolean"
    assert fake_command.calls[0]["args"][1].startswith(str(f))


def test_default_args_mix(zet, zet_toml, monkeyprogram):
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)
    defaults = {
        "integer": 11,
        "arguments": ["foo", "bar"],
        "boolean": True,
    }

    zet_toml["command"] = {"fake-command": {"defaults": defaults}}
    zet_toml.write()

    ret = zet("fake-command", "--integer", "22", "posarg2", _return_cmd=True)

    assert not ret.stdout
    assert not ret.stderr

    assert fake_command.called
    assert len(fake_command.calls) == 1
    # fmt: off
    assert fake_command.calls[0]["args"] == [
        "--integer", "11", "--boolean", "foo", "bar", "--integer", "22", "posarg2"
    ]
    # fmt: on


@pytest.mark.parametrize(
    "defaults",
    [
        [],
        {"arguments": {}},
        {"arguments": [{}]},
        {"other_arg": {}},
        {"other_arg": [{}]},
    ],
)
def test_default_args_invalid(zet, zet_toml, monkeyprogram, defaults):
    monkeyprogram("zet-fake-command", add_to_path=True)
    zet_toml["command"] = {"fake-command": {"defaults": defaults}}
    zet_toml.write()

    with pytest.raises(sh.ErrorReturnCode):
        zet("fake-command")


def test_default_args_disable_cli(zet, zet_toml, monkeyprogram):
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)
    zet_toml["command"] = {"fake-command": {"defaults": {"foo": "bar"}}}
    zet_toml.write()

    ret = zet("--no-defaults", "fake-command", "--some-arg", _return_cmd=True)

    assert not ret.stdout
    assert not ret.stderr

    assert fake_command.called
    assert len(fake_command.calls) == 1
    assert fake_command.calls[0]["args"] == ["--some-arg"]


def test_default_args_disable_env(zet, zet_toml, monkeyprogram, monkeypatch):
    fake_command = monkeyprogram("zet-fake-command", add_to_path=True)
    zet_toml["command"] = {"fake-command": {"defaults": {"foo": "bar"}}}
    zet_toml.write()

    monkeypatch.setenv("ZET_NO_DEFAULTS", "1")
    ret = zet("--no-defaults", "fake-command", "--some-arg", _return_cmd=True)

    assert not ret.stdout
    assert not ret.stderr

    assert fake_command.called
    assert len(fake_command.calls) == 1
    assert fake_command.calls[0]["args"] == ["--some-arg"]
