import os
import pytest
from pathlib import Path
import sh


ZET_TOML_CONFIURED_FILETYPE = """
[[notebooks]]
name = "My Notebook"
path = "content/my notebook"
default_filetype = "adoc"

[[notebooks]]
name = "other"
path = "content/other"
default_filetype = "txt"
""".strip()


@pytest.mark.parametrize(
    "args,output",
    [
        (
            ["My Notebook:note.md", "other:note.md"],
            ["{root}/my notebook/note-1.md", "{root}/other/note-2.md"],
        ),
        (
            ["other:note.md", "--stem", "blah", "--extension", "adoc"],
            ["{root}/other/note-2.md"],
        ),
        (
            ["other:", "--stem", "blah", "--extension", "adoc"],
            ["{root}/other/blah.adoc"],
        ),
        (["other:"], ["{root}/other/note-2.md"]),
        (["other:", "--stem", "blah"], ["{root}/other/blah.md"]),
        (["other:", "--extension", "adoc"], ["{root}/other/note.adoc"]),
        (["other:", "--stem", "note", "--extension", "md"], ["{root}/other/note-2.md"]),
        (["other:", "--extension", ""], ["{root}/other/note"]),
        (["other:newdir/"], ["{root}/other/newdir/note.md"]),
        (
            ["other:newdir/", "--stem", "blah", "--extension", "adoc"],
            ["{root}/other/newdir/blah.adoc"],
        ),
        (
            ["newdir/", "--stem", "blah", "--extension", "adoc"],
            ["{root}/my notebook/newdir/blah.adoc"],
        ),
        (["foo.md"], ["{root}/my notebook/foo.md"]),
        (["note.md"], ["{root}/my notebook/note-1.md"]),
    ],
)
def test_unique_files(zet, notebooks, args, output):
    ret = zet("api", "unique-files", *args)
    lines = ret.splitlines()

    root = os.path.join(notebooks, "content")
    expected = [o.format(root=root) for o in output]
    assert lines == expected


@pytest.mark.parametrize(
    "args,output",
    [
        (
            ["My Notebook:note.md", "other:note.md"],
            ["{root}/my notebook/note-1.md", "{root}/other/note-2.md"],
        ),
        (
            ["My Notebook:", "other:"],
            ["{root}/my notebook/note.adoc", "{root}/other/note.txt"],
        ),
        (
            ["other:note.md", "--stem", "blah", "--extension", "rst"],
            ["{root}/other/note-2.md"],
        ),
        (
            ["other:", "--stem", "blah", "--extension", "rst"],
            ["{root}/other/blah.rst"],
        ),
        (["other:"], ["{root}/other/note.txt"]),
        (["other:", "--stem", "blah"], ["{root}/other/blah.txt"]),
        (["other:", "--extension", "rst"], ["{root}/other/note.rst"]),
        (["other:newdir/"], ["{root}/other/newdir/note.txt"]),
        (
            ["other:newdir/", "--stem", "blah", "--extension", "rst"],
            ["{root}/other/newdir/blah.rst"],
        ),
        (["newdir/"], ["{root}/my notebook/newdir/note.adoc"]),
        (["foo.md"], ["{root}/my notebook/foo.md"]),
    ],
)
def test_unique_files_configured_extension(zet, notebooks, args, output):
    zet_toml = Path(notebooks) / ".zet" / "zet.toml"
    zet_toml.unlink()
    zet_toml.write_text(ZET_TOML_CONFIURED_FILETYPE)

    ret = zet("api", "unique-files", *args)
    lines = ret.splitlines()

    root = os.path.join(notebooks, "content")
    expected = [o.format(root=root) for o in output]
    assert lines == expected


def test_unique_files_update(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("api", "unique-files", "foo.md")
    lines = ret.splitlines()
    assert lines == [f"{root}/my notebook/foo.md"]

    path = Path(lines[0].strip())
    path.touch()

    ret = zet("api", "unique-files", "foo.md")
    lines = ret.splitlines()
    assert lines == [f"{root}/my notebook/foo-1.md"]


def test_unique_files_wrong_notebook(zet, notebooks):
    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "unique-files", "invalid:")
