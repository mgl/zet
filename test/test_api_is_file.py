import os
import pytest
import sh


@pytest.mark.parametrize(
    "selector",
    [
        "My Notebook:foo.md",
        "My Notebook:note.md",
        "My Notebook:note.md/",
        "My Notebook:subdir/subnote.md",
        "My Notebook:subdir/subnote.md/",
        "My Notebook:subdir-symlink/subnote.md",
        "My Notebook:subdir-symlink/subnote.md/",
        "My Notebook:note-symlink.md",
        "My Notebook:note-symlink.md/:",
        "file",
        "file.md",
        "./file.md",
    ],
)
def test_is_file_for_files(zet, notebooks, selector):
    ret = zet("api", "is-file", selector, _return_cmd=True)
    assert ret.process.exit_code == 0


@pytest.mark.parametrize(
    "selector",
    [
        "My Notebook:",
        "My Notebook:subdir",
        "My Notebook:subdir/",
        "My Notebook:subdir-symlink",
        "My Notebook:subdir-symlink/",
        "My Notebook:missing-dir/",
    ],
)
def test_is_file_for_directories(zet, notebooks, selector):
    with pytest.raises(sh.ErrorReturnCode_1):
        zet("api", "is-file", selector)


@pytest.mark.parametrize(
    "selector",
    [
        "foo:",
        "foo:bla.md",
    ],
)
def test_is_file_errors(zet, notebooks, selector):
    with pytest.raises(sh.ErrorReturnCode_2):
        zet("api", "is-file", selector)
