def test_default_list(zet, notebooks):
    ret = zet("notebooks")
    lines = ret.splitlines()
    assert lines == ["My Notebook", "other"]


def test_fully_formatted_list(zet, notebooks):
    ret = zet("notebooks", "-f", "{name}:{path}:{abspath}")
    lines = ret.splitlines()

    assert lines == [
        f"My Notebook:content/my notebook:{notebooks}/content/my notebook",
        f"other:content/other:{notebooks}/content/other",
    ]
