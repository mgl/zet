from typing import List, Union, Optional

import os
import shutil
import re
import json
import shlex
from pathlib import Path
from collections import UserDict

import sh
import pytest
import tomli
import tomli_w

CURR_DIR = os.path.dirname(os.path.abspath(__file__))
SRC_ROOT = os.path.dirname(CURR_DIR)


def envstring(s: str):
    return s.replace(" ", "_").replace("-", "_").replace(".", "_").upper()


def place_program(prog, directory, newname=None, symlink=True):
    progpath = shutil.which(prog)
    if not progpath:
        return

    src = Path(progpath)
    if not newname:
        newname = src.name
    dest = directory / newname
    directory.mkdir(parents=True, exist_ok=True)

    if symlink:
        dest.symlink_to(src)
    else:
        shutil.copy(src, dest)


@pytest.fixture
def sorted_notes_selectors():
    return sorted(
        [
            "My Notebook:note-symlink.md",
            "My Notebook:note.md",
            "My Notebook:subdir-symlink/subnote.md",
            "My Notebook:subdir/subnote.md",
            "other:lorem.md",
            "other:note-1.md",
            "other:note.md",
        ]
    )


@pytest.fixture
def sorted_notes(sorted_notes_selectors, zet_toml):
    repls = { nb["name"]: nb["path"] for nb in zet_toml["notebooks"] }

    def to_relpath(note):
        nb, _, file = note.partition(":")
        return os.path.join(repls[nb], file)

    return [to_relpath(note) for note in sorted_notes_selectors]


@pytest.fixture(scope="session")
def install(tmp_path_factory):
    destdir = tmp_path_factory.mktemp("installation")
    prefix = Path("/usr")

    env = os.environ.copy()
    env["DESTDIR"] = str(destdir)
    env["PREFIX"] = str(prefix)
    sh.make("-C", SRC_ROOT, "install", _env=env)

    return destdir, prefix


@pytest.fixture
def monkeyprogram(monkeypatch, tmp_path):
    class MonkeyProgram:
        def __init__(self, name, bindir, symlink):
            self.name = name
            self.bindir = bindir
            self.responses = []
            self.returncodes = []
            self._call_cache = (0, None)  # (mtime, cache)

            monkeyprogsh = Path(CURR_DIR) / "bin" / "monkeyprog"
            place_program(monkeyprogsh, self.bindir, newname=self.name, symlink=symlink)

        def on_request(
            self,
            req_pattern: str,
            responses: Union[None, str, List[str]] = None,
            returncode: Optional[int] = None,
        ) -> "MonkeyProgram":
            assert responses is not None or returncode is not None
            if responses is not None:
                if isinstance(responses, str):
                    responses = [responses]
                self.responses.append((req_pattern, responses))
            if returncode is not None:
                self.returncodes.append((req_pattern, returncode))
            return self

        def clear_responses(self):
            self.responses = []

        def clear_returncodes(self):
            self.returncodes = []

        def monkeypatch(self):
            resps = json.dumps(self.responses)
            monkeypatch.setenv(f"MONKEYPROG_{envstring(self.name)}_RESPONSES", resps)

            rets = json.dumps(self.returncodes)
            monkeypatch.setenv(f"MONKEYPROG_{envstring(self.name)}_RETURNCODES", rets)

        @property
        def path(self) -> Path:
            return self.bindir / self.name

        @property
        def called(self) -> bool:
            return self.calls != []

        @property
        def calls(self) -> List[str]:
            calls_file = self.bindir / f"{self.name}-calls.json"

            try:
                mtime = calls_file.stat().st_mtime
            except FileNotFoundError:
                return []

            cmtime, ccalls = self._call_cache
            if ccalls and cmtime >= mtime:
                return ccalls

            try:
                text = calls_file.read_text()
                calls = json.loads(text)
                self._call_cache = mtime, calls
                return calls
            except FileNotFoundError:
                return []

        def for_each_call(self, fn):
            rets = []
            for call in self.calls:
                rets.append(fn(call))
            return rets

    def create(
        name: str,
        bindir: Optional[Path] = None,
        add_to_path: bool = False,
        symlink=True,
    ):
        if not bindir:
            bindir = tmp_path / "monkeyprog"
        mp = MonkeyProgram(name, bindir, symlink)
        if add_to_path:
            monkeypatch.setenv("PATH", str(bindir), prepend=os.pathsep)
        return mp

    return create


@pytest.fixture
def monkeyhook(monkeyprogram, notebooks):
    def create(hook_name: str, symlink=True):
        hookdir = notebooks / ".zet" / "hooks"
        return monkeyprogram(hook_name, hookdir, symlink=symlink)

    return create


@pytest.fixture
def notebooks(monkeypatch, tmp_path):
    data = tmp_path / "data"
    data.mkdir()
    zet_root = os.path.join(str(data), "notebooks")

    shutil.copytree(os.path.join(CURR_DIR, "notebooks"), str(zet_root))
    monkeypatch.setenv("ZET_ROOT", zet_root)
    return Path(zet_root)


@pytest.fixture
def zet_toml(notebooks):
    class ZetToml(UserDict):
        def __init__(self):
            self.path = notebooks / ".zet" / "zet.toml"
            super().__init__(tomli.loads(self.path.read_text()))

        def write(self):
            self.path.write_text(tomli_w.dumps(self.data))

    return ZetToml()


@pytest.fixture
def set_install_env(install, monkeypatch):
    destdir, prefix = install
    modules_path = os.path.join(str(destdir) + str(prefix), "share", "zet", "modules")
    lib_dir = os.path.join(str(destdir) + str(prefix), "share", "zet", "lib")
    path = os.path.join(str(destdir) + str(prefix), "bin")
    monkeypatch.setenv("NO_COLOR", "1")  # suppress clap's formatting
    monkeypatch.setenv("ZET_MODULES_PATH", modules_path)
    monkeypatch.setenv("ZET_LIB_DIR", lib_dir)
    monkeypatch.setenv("PATH", path, prepend=os.pathsep)
    monkeypatch.delenv("ZET_NO_HOOKS", raising=False)


@pytest.fixture
def zet(install, set_install_env):
    destdir, prefix = install
    exe_path = os.path.join(destdir, str(prefix).strip("/"), "bin", "zet")
    assert exe_path == shutil.which("zet")
    return sh.Command("zet")
