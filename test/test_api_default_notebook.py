import pytest
from pathlib import Path
import sh
import os


def test_api_default_notebook_first_notebook(zet, notebooks):
    ret = zet("api", "default-notebook")
    lines = ret.splitlines()
    assert lines == ["My Notebook"]


def test_api_default_notebook_configured(zet, notebooks):
    dnf = Path(notebooks) / ".zet" / "default-notebook"
    dnf.write_text("other")

    ret = zet("api", "default-notebook")
    lines = ret.splitlines()
    assert lines == ["other"]


def test_api_default_notebook_invalid_notebook_configured_returns_first(zet, notebooks):
    dnf = Path(notebooks) / ".zet" / "default-notebook"
    dnf.write_text("unknown-notebook")

    ret = zet("api", "default-notebook")
    lines = ret.splitlines()
    assert lines == ["My Notebook"]


def test_api_default_notebook_no_zet_toml(zet, notebooks):
    zet_toml = Path(notebooks) / ".zet" / "zet.toml"
    zet_toml.unlink()

    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "default-notebook")


def test_default_notebook_empty_zet_toml(zet, notebooks):
    zet_toml = Path(notebooks) / ".zet" / "zet.toml"
    zet_toml.unlink()
    zet_toml.touch()

    with pytest.raises(sh.ErrorReturnCode):
        zet("api", "default-notebook")


def test_api_default_notebook_abspath(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet("api", "default-notebook", "--absolute")
    lines = ret.splitlines()

    expected = [os.path.join(root, "my notebook")]
    assert lines == expected


def test_api_default_notebook_relpath(zet, notebooks):
    ret = zet("api", "default-notebook", "--relative")
    lines = ret.splitlines()

    expected = ["content/my notebook"]
    assert lines == expected


def test_api_default_notebook_selector(zet, notebooks):
    ret = zet("api", "default-notebook", "--selector")
    lines = ret.splitlines()

    expected = ["My Notebook:"]
    assert lines == expected


def test_api_default_notebook_all_fields(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet(
        "api", "default-notebook", "--absolute", "--selector", "--relative", "--name"
    )
    lines = ret.splitlines()

    expected = [
        f"My Notebook::{os.path.join(root, 'my notebook')}::content/my notebook::My Notebook:",
    ]
    assert lines == expected


def test_api_default_notebook_all_fields_field_separator(zet, notebooks):
    root = os.path.join(notebooks, "content")

    ret = zet(
        "api",
        "default-notebook",
        "--absolute",
        "--selector",
        "--relative",
        "--name",
        "--field-separator",
        "|",
    )
    lines = ret.splitlines()

    expected = [
        f"My Notebook|{os.path.join(root, 'my notebook')}|content/my notebook|My Notebook:",
    ]
    assert lines == expected
