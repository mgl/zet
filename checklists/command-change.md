# Command Change Checklist

- [ ] `git pull --rebase`

- [ ] implement your modifications

- [ ] Run command locally through either `zet-dev` or with exported
  `ZET_MODULES_PATH`:

  ```
  export ZET_MODULES_PATH=$(pwd)/modules
  ```

- [ ] Add tests in test/test<command-name>.py

- [ ] If command-line options are changed/added modify completion scripts
  inside `completions` directory

- [ ] Test completions:
  ```
  $ export PATH=$(pwd)/target/release/:$PATH
  $ export ZET_MODULES_PATH=$(pwd)/modules
  $ source completions/_zet.<shell>
  ```

- [ ] Update documentation:
    - doc/src/non-core.md
    - doc/src/cheat-sheet.md

- [ ] Update CHANGELOG's _NEXT_ section

- [ ] Check if the change is backward compatible. If it isn't and Zet's
  version is >= 1.0.0, and MAJOR version in _Cargo.toml_ is the same as the
  latest release, increment the MAJOR version in _Cargo.toml_.
    - Latest released version:
      ```
      $ git pull --tags
      $ git tag -l --sort=version:refname | tail -n1
      ```
    - currently configured version:
      ```
      $ cargo metadata --format-version=1 --no-deps | jq '.packages[0].version'
      ```

      or

      ```
      $ cargo pkgid | cut -d "#" -f2
      ```

- [ ] If you tick-off checkboxes in this file, make sure you DO NOT COMMIT
  THEM.
  ```
  $ git restore checklists/command-change.md
  ```

- [ ] Run the tests: `make check` and make sure that they pass.
