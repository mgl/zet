# zet - modular, Zettelkasten aware command line notebooks manager

**Zet** is command line (CLI) notes manager. It can be used to maintain a
personal wiki, journal, Zettelkasten-like notebook, to synchronize notes with
Git server, to easily discover past notes and so on. It was created to
address some pain points with CLI notes management, like renaming notes
without breaking links between them, templating and notes discovery.

Zet is modular and easily extensible thanks to its pluggable architecture. It
also has a built-in CLI API for writing user commands."

- [Online documentation](https://pages.goral.net.pl/zet)
- [Main repository](https://git.goral.net.pl/stag.git)

## Usage

```
$ # initialize first empty notebook:
$ zet init
```

From now on you can run zet from a drectory in which _.zet/zet.toml_ is
found. Optionally, you can set `ZET_ROOT` environment variable which zet will
detect and use as a root directory for notebooks.

Zet comes with many useful commands. Some of them are:

- `zet note` - write new note
- `zet import` - import files and urls
- `zet mv` - rename notes (optionally to its title)
- `zet open`, `zet show` - display notes
- `zet search` - fast and powerul search through the notes
- `zet replace` - search and replace
- `zet sync` - synchronize notes via git

Zet is extremely extensible, with plugin-like architecture. You can easily
build your [own commands](https://pages.goral.net.pl/zet/master/custom-commands.html)
on top of above commands, simply by putting a program named `zet-<name>`
inside your PATH (or inside ZET_MODULES_PATH).

Zet can be configured to work with several notebooks. For ease of use one of
them can be set as a default one, which will be accessed through selectors
which do not specify a notebook name.

Zet implements a powerful [selector](https://pages.goral.net.pl/zet/master/selectors.html)
syntax which extends ordinary way of referencing files with their paths. In
Zet you can instead write _notebook:file.md_.

Zet implements a powerful `zet api` command which allows you to easily use
Zet-specific features, like selectors or default notebooks, in your own
commands.

## Exmple zet.toml

```toml
[[notebooks]]
name = "Personal"
path = "content/0"
remote = "ssh://git@example.com/personal.git"

[[notebooks]]
name = "Work"
path = "content/work"
remote = "https://example.com/work.git"
```

## Web Interface

Web interface for notebooks managed with Zet exists. See
<https://git.goral.net.pl/zetweb.git/about/> for details.

## Issues

[Issue Tracker](https://issues.goral.net.pl/public/board/d4b62d01bb55743f30a15eec7f6c62183c8a9e1241cf94a1aeedc2eb4290)

Please report new issues to dev@goral.net.pl.
