NEXT:

0.13.0: 2025-03-05

  [Added]
  - zet run: added --cwd switch
  - zet run: --all, --notebook and --cwd switches are additive
  - zet import url: automatically fetch titles from the imported pages or their URLs.

  [Fixed]
  - Fix shell completion after changes in zet api notebooks in 0.12.0
  - zet run --all: fixed invalid notebook path preventing command execution in
    existing notebooks
  - zet run: fixed misleading error when notebook doesn't exist
  - zet import: fix using removed pandoc --atx-headers option

0.12.0: 2024-01-16

  I dedicate this release in memory of my father in law, who recently passed
  away. I love you, dad.

  [Breaking]
  - zet replace, zet open: rename --case-insensitive flag to --ignore-case
  - zet ls, search, tasks, open menu and replace return file paths in a
    selector syntax instead of paths relative to $ZET_ROOT
  - output of zet api notebooks, default-notebook and list is controlled by
    different switches which enable or disable which fields should be printed.
    Following list shows to which new switches the old ones correspond:
      - zet api list --path -> zet api list --absolute
      - zet api list --path --relative -> zet api list --relative
      - zet api list --as-selectors -> zet api list --selector
      - zet api notebooks --paths -> zet api notebooks --absolute
      - zet api notebooks --paths --relative -> zet api notebooks --relative
      - zet api notebooks --as-selectors -> zet api notebooks --selector
      - zet api default-notebook --paths -> zet api default-notebook --absolute
      - zet api default-notebook --paths --relative -> zet api default-notebook --relative
      - zet api default-notebook --as-selectors -> zet api default-notebook --selector
  - output of zet api notebooks is no longer lexicographically sorted, but
    instead it is in order of requested or configured notebooks (depending on
    the command invocation)

  [Added]
  - Created manpages (via scdoc)
  - zet run: added --all flag to run a command in all notebooks
  - zet sync: added --configure=NB flag
  - zet ls: added --sort-modify-time to output files sorted by their
    modification time
  - zet menu --absolute switch to return absolute paths
  - zet replace: added --no-color flag
  - zet api list: added --selector flag to print notes in notebooks in selector
    notation
  - zet api list: added --created, --modified and --accessed flags to print
    times of file creation, modification and last access

  [Fixed]
  - Correct order of added and removed lines in diffs presented during `zet replace --interactive`

0.11.0: 2023-09-25

  [Breaking]
  - zet replace: rename --insensitive flag to --case-insensitive

  [Added]
  - new command: zet run to run arbitrary commands inside notebook's directory
  - completions for Bash and ZSH
  - zet note: new option --inherit-stdin (useful e.g. for vim)
  - zet commands --list: easy to parse commands list
  - zet api default-notebook: added --path, --relative and --as-selector switches
    to allow easier access to default notebook's data
  - pyzet api_notebooks: added `as_selectors` argument
  - pyzet api_notebooks: add optional list of notebooks to filter
  - pyzet api_default_notebook: added `path`, `relative` and `as_selector`
    arguments

  [Changed]
  - pyzet: running any zet api command which fails will now by default print
    an error message

0.10.0: 2023-05-10

  [Breaking]
  - Elements of TOML arrays from zet.toml represented as environment variables
    passed to commands are now separated by a semicolon ';' (earlier: by a
    colon ':' which conflicted selector syntax).
  - --no-hooks switch no longer has -n as short option

  [Added]
  - settings from command.<subcommand>.defaults table from zet.toml will be
    automatically added to invocations of subcommands. This feature can be
    disabled by running zet --no-defauls or by setting ZET_NO_DEFAULT_ARGS=1
    environment variable.
  - zet menu: C-x keybind to execute arbitrary command on selected file
  - zetlib.bash: quote() function to safely single-quote strings
  - zetlib.bash: replace_all() function to replace a pattern in a string with a
    replacement string

  [Changed]
  - compact header in zet menu

  [Fixed]
  - zet menu: report empty files as such instead of binary files

0.9.0: 2023-04-27

  [Added]
  - New command: zet journal for day-by-day journaling.
  - New command: zet menu for interactive, fzf-based note list
  - zet note: --title switch which sets {title} template placeholder
  - zet note: --and-then switch which executes a command after a successful
    note creation, with a placeholder '{}' for a note's path
  - zet ls: --absolute switch
  - zet api getenvnbsel: return a value of environment variable specific for a
    notebook which is extracted from a passed selector.
  - zetlib.bash: contains_str() function which checks if a given string
    contains a substring.

0.8.0: 2023-03-05

  [Breaking]
  - zet api unique-files: --default-basename is replaced with 2 new switches:
    --stem and --extension. Stem defaults to "note" (as previously), but
    extension is dynamically computed and depends on "default_filetype" for
    particular notebook

  [Added]
  - Support new field "default_filetype" in [[notebooks]] in zet.toml. This
    field describes what file type should Zet prefer when it creates new notes
    (e.g. with bare `zet note`)
  - zet sync now accepts a list of notebook names which should be selectively
    synchronized
  - zet mv: confirm performed move

  [Changed]
  - zet mv --to-title will not fail when destination file exists, but instead
    it'll choose another derivative file name
  - change style of imported urls

  [Fixed]
  - zet show: try using both bat and batcat.
  - zet search: don't double print lines which quote both queries

0.7.0: 2023-03-03

  [Changed]
  - relicense to GPLv3 only (previously: GPLv3 or later)

  [Fixed]
  - Fixed invalid search of data dirs

0.6.0: 2023-03-03

  [Breaking]
  - zet.toml moved to .zet directory inside $ZET_ROOT
  - .default-notebook moved to .zet/default_notebook
  - zet api paths now returns expanded paths without a slash suffix for
    directories. This behaviour was buggy and unfixable. Please use zet api
    is-file to determine whether selector points to the file.
  - zet note doesn't support {basename} and {path} template placeholders anymore

  [Added]
  - zet now stores all of its artifacts inside .zet directory. It is passed to
    the subcommands under $ZET_DIR environment variable.
  - zet now stores module-specific libraries (pyzet, zetlib.bash) inside
    separate directories which are exposed with $ZET_LIB_DIR environment
    variable.
  - Added support for pre-command and post-command hooks, which are called
    before and after any non-core zet subcommand runs
  - New command: zet search to perform a sophisticated full-text searches
  - New command: zet show (basically a fancy zet-open wrapper)
  - New API command: `zet api print`
  - zet api list: ability to print note titles, and canonical titles suitable
    for file names optionally together with file paths and separated by a
    custom separator string
  - zet ls: added switches to print note titles and choose a field separator
  - zet mv: --to-title switch which renames notes to their titles
  - zet note: automatically detect titles for new notes and name new files from
    their canonical form
  - zet open: allow specifying a placeholder '{}' for --program option, which
    will be replaced with a path to open

  [Fixed]
  - Added support for short -h for zet-gen, ls, mv and sync. Its lack caused
    showing incorrect help pages in some cases.
  - pyzet: use correct zet api command for api_default_notebook
  - zet mv: don't try to move directories
  - zet note: allow specifying editor without '{}' placeholder (which will be
    automatically appended in that case)

0.5.0: 2023-02-13

  [Breaking]
  - Renamed zet ls-commands to zet commands
  - Renamed zet print-env to zet env
  - Require rust 1.64
  - pyzet: rename default_notebook to api_default_notebook
  - pyzet: ensure_unique_basename changes name to api_unique_files and
    now it optionally accepts a list o paths and returns a list of unique files.
  - zet import: -n --notebook changes to -d --destination
  - zet ls: selectors notation is now enforced. Previously `zet ls foo` meant
    "list files in foo notebook", now it means "list file foo in a default
    notebook". Add a colon to the notebook name to preserve the old meaning 
    (`zet ls foo:`)
  - zet mv no longer changes references to the old files
  - zet tasks now accepts selectors instead of notebooks

  [Added]
  - Added core subcommand: zet api, which provides a set of porcelain
    (script-parsable) sub-subcommands for querying configured zet notebooks.
    They are meant to replace module libraries and provide a unified interface
    for all programming languages. See `zet api help [command]` for help for
    particular API calls.
  - Modules (non-core zet subcommands) can now provide a short, one line
    description which will be shown on `zet commands` screen. Each directory in
    ZET_MODULES_PATH can have a command-list.txt file with descriptions for
    subcommands.
  - Documentation, which can be viewed at https://pages.goral.net.pl/zet
  - zetlib.bash: added a parser stolen from makepkg (GPLv2+)
  - Users can set ZET_MODULES_PATH environment variable to override the one
    which zet automatically detects.
  - zet mv: files can be moved to not existing directories, which will be
    automatically created
  - zet init: accept initial notebook's name and path
  - zet replace: comand to perform search-and-replace of strings inside notes

  [Changed]
  - modernized help pages

  [Fixed]
  - zet env prints to standart output now (previously: standard error)
  - zet init: don't return an error when notebook's directory already exists

0.4.1: 2022-12-22

  [Breaking]
  - Removed gzet

  [Added]
  - zet-note: new module to quickly add new notes. Notes can be edited with an
    editor. Alternatively, note can be passed to stdin of zet note. Notes can
    be also created from a template.
  - zet-open: option to use case-insensitive matching
  - zet-default-notebook: get and set default notebook
  - pass path to the modules in the environment of subcommands as
    ZET_MODULES_PATH variable
  - Allow set install flags via CARGOFLAGS_INSTALL
  - zet is now aware of ZET_ROOT environment variable, in which case it will
    use it instead of the current directory for reading zet.toml

  [Changed]
  - default notebooks can be now stored in $ZET_ROOT/.default-notebook file,
    which should be used by all subcommands which need to operate on default
    notebooks (typically, when user doesn't provide an explicit notebook when
    using a "notebook notation").

  [Fixed]
  - tasks: Fixed displaying tasks when a notebook with colon is passed (e.g.
    "-n notebook:"
  - tasks: Fixed displaying file names on top of the tasks when only single file
    is searched for tasks

0.3.0: 2022-01-14

  [Breaking]
  - renamed zet-git-sync to zet-sync
  - zet-sync (previously zet-git-sync) now uses remote entries in zet.toml
    instead of git entries.

  [Added]
  - New submodule: import, for importing files and URLs into notebooks.
  - New submodule: open, for opening files stored in notebooks.
  - sync: added basic local git synchronization (via "git add -A")
  - create empty git directories for missing notebooks without configured
    remote, implying that such directories will be synchronised through git
    later

  [Changed]
  - sync: git-sync will be only used if git repository is configured with a
    remote branch


  [Fixed]
  - Fixed lack of output when running zet-tasks from outside of zet's root
    directory.

0.2.0: 2022-01-11

  [Added]
  - New submodule: tasks, which greps for lines which look like tasks.
  - gzet: global command wrapper for running zet pointed to a single location
    from anywhere.
  - zet-git-sync: support for GNU Parallel.

  [Changed]
  - [BREAKING] Replaced --print-env with print-env subcommand.
  - zet-git-sync: skip non-git directories.
  - zet-ls: ignore hidden files.

  [Fixed]
  - Correct parsing of positional arguments. Separating zet's arguments from
    subcommand's ones is no longer necessary.
  - Fixed incorrect help strings.

0.1.0: 2020-11-10

Initial Release
