PREFIX ?= ${HOME}/.local
MANPREFIX ?= $(PREFIX)/share/man
CARGOFLAGS ?= --release
CARGOFLAGS_INSTALL ?= --offline --path . --locked

SCDOC ?= scdoc
INSTALL ?= install
