#!/bin/bash

[[ -n "${ZET_LIB_ZETLIB_BASH:-}" ]] && return
ZET_LIB_ZETLIB_BASH=1

# print usage by grepping the special comment
usage() {
    grep '^#/' "$0" | cut -c4-
}

# parse opts and set them as command arguments in the easily parsable form:
#   - verifies that opts which require arguments have them
#   - transforms all kinds of argument styles (GNU, short, long etc) to the form
#     where everything is easily looped:
#       --foo --bar -f -b --baz arg1 -- pos1 pos2
#   - requires that opt_short variable and opt_long array are set and command
#     args are passed by calling `parse_opts "$@"`.
#   - when opt ends with a colon, it requires an argument:
#       opt_short="f:b"
#       opt_long=("foo:" "bar")
#   parsed args are set in OPTRET array. Use it like that:
#       set -- "${OPTRET[@]}"
parse_opts() {
    . "${ZET_LIB_DIR}/bash/parseopts.bash"

    if ! _parseopts "${opt_short}" "${opt_long[@]}" -- "${@}"; then
        exit 1
    fi
    unset opt_short opt_long
}

## print to stderror
eprintf() {
    msg="$1"; shift
    printf "${msg}\n" "$@" >&2
}

## print error message and exit
die() {
    eprintf "$@"
    exit 1
}

# get environment variable named after the passed string
getvar() {
    echo ${!1}
}

## filter converting string to lower/upper case
tolower() {
    awk '{print tolower($0)}'
}

toupper() {
    awk '{print toupper($0)}'
}

command_exists() {
    hash "$1" 2>/dev/null
}

contains_str() {
    local str="$1"
    local substr="$2"
    if [[ $str == *"$substr"* ]]; then
        return 0
    fi
    return 1
}

quote() {
    local str="$1"
    local q="'"
    local qq="\\'"
    echo "'${str//${q}/${qq}}'"
}

replace_all() {
    local str=$1
    local pat=$2
    local repl=$3

    echo "${str//${pat}/${repl}}"
}

yesnoquit() {
    local question="$1"
    while true; do
        echo ">> ${question} ([Y]es / [N]o / [Q]uit)"
        read -s -n1 answer
        case "${answer}" in
            y|Y) return 0 ;;
            n|N) return 1 ;;
            q|Q) exit 2 ;;
        esac
    done
}

