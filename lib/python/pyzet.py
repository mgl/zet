"""Helpers library for zet"""

from typing import List

import os
import sys
import shutil
import contextlib
from functools import cache
import subprocess


class APIError(Exception):
    def __init__(self, cmd, what=None):
        self.cmd = cmd
        self.what = what

    def __str__(self):
        if self.what:
            return f"zet api {self.cmd}: {self.what}"
        return f"zet api {self.cmd}"


def zet(*args, **kw):
    kw.setdefault("capture_output", True)
    kw.setdefault("text", True)
    kw.setdefault("check", True)
    cmd = ["zet"] + list(args)

    try:
        cp = subprocess.run(cmd, **kw)
    except subprocess.CalledProcessError as e:
        die(f"Underlying API command failed: {' '.join(cmd)}", status=e.returncode)
    except FileNotFoundError:
        die("zet executable not found")
    return cp


def die(msg=None, status=1):
    if msg:
        eprint(msg)
    sys.exit(status)


def eprint(*a, **kw):
    kw["file"] = sys.stderr
    print(*a, **kw)


def getenv(varname, default=""):
    """Get the value of environment variable"""
    return os.environ.get(varname.strip(), default).strip()


def getenvnb(notebook, varname, default=""):
    """Get the value of zet-specific notebook environment variable."""
    notebook = notebook_env_name(notebook)
    envvar = f"ZET_NOTEBOOK_{notebook}_{varname.upper()}"
    return getenv(envvar, default)


@contextlib.contextmanager
def tmpchdir(path):
    origcwd = os.getcwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(origcwd)


def notebook_env_name(nb):
    return nb.replace(" ", "_").upper()


def command_exists(cmd):
    """Return whether supplied command is accessible in a current environment"""
    return shutil.which(cmd) is not None


def api_default_notebook(
    name=False, selector=False, absolute=False, relative=False, fs=None
) -> str:
    """Return the default notebook"""

    cmd = ["api", "default-notebook"]

    if name:
        cmd.append("--name")
    if selector:
        cmd.append("--selector")
    if absolute:
        cmd.append("--absolute")
    if relative:
        cmd.append("--relative")
    if fs:
        cmd.extend("--field-separator", fs)

    return zet(*cmd).stdout.splitlines()[0]


def api_paths(*selectors):
    """Get absolute system paths of given selectors."""
    cp = zet("api", "paths", *selectors)
    return cp.stdout.splitlines()


def api_unique_files(paths, stem=None, extension=None):
    """Transform basename of a path in a way that makes sure that it doesn't
    exist yet."""

    if not isinstance(paths, (list, tuple)):
        paths = [paths]
    args = []
    if stem:
        args.extend(["--stem", stem])
    if extension:
        args.extend(["--extension", extension])
    cp = zet("api", "unique-files", *args, *paths)
    return cp.stdout.splitlines()


def api_is_file(selector: str) -> bool:
    """Return whether a given selector points to a file or directory. Path
    doesn't have to exist, in which case directories must end with a slash"""
    cp = zet("api", "is-file", selector, check=False)
    if cp.stderr:
        eprint(cp.stderr.strip())
    if cp.returncode not in (0, 1):
        raise APIError("is-file", f"unexpected return code: {cp.returncode}")
    return cp.returncode == 0


def api_notebooks(
    *nbfilter, name=False, selector=False, absolute=False, relative=False, fs=None
) -> List[str]:
    cmd = ["api", "notebooks"]
    cmd.extend(nbfilter)

    if name:
        cmd.append("--name")
    if selector:
        cmd.append("--selector")
    if absolute:
        cmd.append("--absolute")
    if relative:
        cmd.append("--relative")
    if fs:
        cmd.extend(("--field-separator", fs))

    cp = zet(*cmd)
    return cp.stdout.splitlines()


def api_list_files(
    *selectors, absolute=False, relative=False, title=False, canonical_title=False, fs=None
) -> List[str]:
    cmd = ["api", "list"]

    if absolute:
        cmd.append("--absolute")
    if relative:
        cmd.append("--relative")
    if title:
        cmd.append("--title")
    if canonical_title:
        cmd.append("--canonical-title")
    if fs:
        cmd.extend("--field-separator", fs)

    cmd.extend(selectors)
    cp = zet(*cmd)
    return cp.stdout.splitlines()


@cache
def api_print(*lines, style) -> List[str]:
    cmd = ["api", "print", "--style", style, *lines]
    cp = zet(*cmd)
    return cp.stdout.splitlines()
