/* SPDX-Ltcense-Identifier: GPL-3.0-only */

use std::path::PathBuf;

use clap::{Parser, Subcommand};

use crate::api;

#[derive(Parser, Debug)]
#[command(name = "zet")]
#[command(max_term_width = 90)]
#[command(version)]
#[command(after_help = "'zet --help' or 'zet help' will print full help.
'zet <cmd> --help' will display help page for subcommand.
'zet commands' will list all discovered subcommands.

Zet is a Free Software and a work of love by Michał Góral.
Source code: https://git.goral.net.pl/zet.git
Online documentation: https://pages.goral.net.pl/zet/")]
/// zet - command line notebooks manager
///
/// Zet is a note and notebooks manager. It is composed of many commands to create, read, search or
/// edit notes as well as to do many other tasks related to the maintainance of notebooks and
/// knowledge bases.
///
/// 'zet init' initializes new workspace (notebook collection). 'zet commands' produces a list of
/// available commands. Core built-in commands are listed below.
///
/// To access your notebooks from anywhere, you can set $ZET_ROOT environment variable.
pub struct Args {
    #[arg(short = 'C', long = "directory")]
    /// Change to directory before reading configuration
    pub directory: Option<PathBuf>,

    #[arg(short = 'c', long = "config")]
    /// Path to zet.toml
    pub config: Option<PathBuf>,

    #[arg(long)]
    /// Don't run any hooks
    pub no_hooks: bool,

    #[arg(long)]
    /// Disable automatic default arguments configured for zet commands in zet.toml
    pub no_defaults: bool,

    #[command(subcommand)]
    /// Command to execute
    pub command: Subcommands,
}

#[derive(Subcommand, Debug, PartialEq, Eq)]
pub enum Subcommands {
    /// Initialize new workspace
    Init {
        #[arg(default_value = "default")]
        /// Initial notebook's path. It will be created if it is missing
        path: String,

        #[arg(default_value = "My Notebook")]
        /// Initial notebook's name
        name: String,
    },

    #[command(name = "commands")]
    /// Prints all installed zet non-core commands available in PATH and zet's modules directory.
    LsCommands {
        #[arg(long)]
        /// Print easy to parse list of commands and their descriptions (each on a single line)
        list: bool,
    },

    #[command(name = "env")]
    /// Print environment which will be passed to non-core commands, for debug
    /// purposes.
    PrintEnv,

    #[command(name = "api")]
    /// Plumbing, easy to parse API commands which can be used by non-core commands.
    CallApi(api::ApiCommand),

    #[command(external_subcommand)]
    Other(Vec<String>),
}
