/* SPDX-License-Identifier: GPL-3.0-only */

use std::collections::HashMap;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};
use toml::Value;

#[derive(Serialize, Deserialize, Debug)]
pub struct Notebook {
    pub name: String,

    #[serde(rename = "path")]
    pub relpath: String,

    // The effect of flattening to the map is that other fields, not captured
    // above, will be serialised under "extra" field.
    #[serde(flatten)]
    pub extra: HashMap<String, Value>,
}

impl Notebook {
    pub fn abspath<P>(&self, root: P) -> PathBuf
    where
        P: AsRef<Path>,
    {
        root.as_ref().join(&self.relpath)
    }
}

pub struct NotebookFormatter {
    separator: String,
    root: PathBuf,
    abspath: bool,
    relpath: bool,
    name: bool,
    selector: bool,
}

impl NotebookFormatter {
    pub fn new(separator: &str, root: &Path) -> NotebookFormatter {
        NotebookFormatter {
            separator: separator.to_owned(),
            root: root.to_path_buf(),
            abspath: false,
            relpath: false,
            name: false,
            selector: false,
        }
    }

    pub fn set_abspath(&mut self, val: bool) -> &mut NotebookFormatter {
        self.abspath = val;
        self
    }

    pub fn set_relpath(&mut self, val: bool) -> &mut NotebookFormatter {
        self.relpath = val;
        self
    }

    pub fn set_name(&mut self, val: bool) -> &mut NotebookFormatter {
        self.name = val;
        self
    }

    pub fn set_selector(&mut self, val: bool) -> &mut NotebookFormatter {
        self.selector = val;
        self
    }

    pub fn format(&self, nb: &Notebook) -> String {
        let mut parts: Vec<String> = Vec::new();

        if self.name {
            parts.push(nb.name.clone());
        }

        if self.abspath {
            parts.push(nb.abspath(&self.root).to_string_lossy().into_owned())
        }

        if self.relpath {
            parts.push(nb.relpath.clone());
        }

        if self.selector {
            let mut name = nb.name.clone();
            name.push(':');
            parts.push(name);
        }

        parts.join(&self.separator)
    }
}
