/* SPDX-License-Identifier: GPL-3.0-only */

use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};
use toml::Value;

use crate::notebook::Notebook;

#[derive(Serialize, Deserialize, Debug)]
pub struct CommandOptions {
    #[serde(flatten)]
    pub opts: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub notebooks: Vec<Notebook>,

    #[serde(rename = "command")]
    pub commands: Option<HashMap<String, CommandOptions>>,
}

#[derive(Deserialize, Debug)]
pub struct ConfigInfo {
    pub path: PathBuf,
    pub config: Config,
}

impl PartialEq for ConfigInfo {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

fn parse_config(path: &PathBuf) -> Result<Config, io::Error> {
    let mut file = File::open(path)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;

    match toml::from_str(&s) {
        Ok(cfg) => Ok(cfg),
        Err(e) => {
            eprintln!("zet.toml: {:}", e);
            Err(io::Error::new(io::ErrorKind::InvalidData, "parsing error"))
        }
    }
}

impl ConfigInfo {
    pub fn new(path: &Option<PathBuf>) -> Result<ConfigInfo, io::Error> {
        let cfgpath = match path {
            Some(p) => p.clone(),
            None => Self::find_config()?,
        };

        Ok(ConfigInfo {
            path: cfgpath.clone(),
            config: parse_config(&cfgpath)?,
        })
    }

    fn find_config() -> Result<PathBuf, io::Error> {
        let root = env::var("ZET_ROOT").unwrap_or_else(|_| "".to_string());
        let mut path = if !root.is_empty() {
            PathBuf::from(root)
        } else {
            env::current_dir()?
        };

        loop {
            path.push(".zet");
            if path.exists() {
                path.push("zet.toml");
                return Ok(path);
            }
            path.pop(); // pops file name
            if !path.pop() {
                break;
            }
        }

        Err(io::Error::new(
            io::ErrorKind::NotFound,
            "zet.toml not found",
        ))
    }

    pub fn get_zet_dir(&self) -> &Path {
        return self.path.parent().unwrap();
    }

    pub fn get_zet_root(&self) -> &Path {
        return self.get_zet_dir().parent().unwrap();
    }
}
