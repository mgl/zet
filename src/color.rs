/* SPDX-Ltcense-Identifier: GPL-3.0-only */

use std::io;
use std::io::Write;

use clap::ValueEnum;

#[derive(Debug, PartialEq, Eq, Clone, Copy, ValueEnum)]
pub enum Style {
    Header,
    Literal,
}

#[derive(Debug)]
pub struct ColorPrinter {
    parts: Vec<(String, Option<Style>)>,
    wrap: Option<usize>,
    tty_check: bool,
}

/// Class which helps printing styled text. Its basic interface is `add` method, which adds a new
/// styled part, separated by a space from the previous part, wrapped by configurable line length.
impl ColorPrinter {
    pub fn new() -> ColorPrinter {
        ColorPrinter {
            parts: Vec::new(),
            wrap: None,
            tty_check: true,
        }
    }

    pub fn disable_tty_check(&mut self) -> &mut ColorPrinter {
        self.tty_check = false;
        self
    }

    pub fn print(&mut self) -> Result<(), io::Error> {
        let cc = if self.tty_check && !atty::is(atty::Stream::Stdout) {
            termcolor::ColorChoice::Never
        } else {
            termcolor::ColorChoice::Auto
        };

        let bw = termcolor::BufferWriter::stdout(cc);
        let mut buf = bw.buffer();

        if let Some(last) = self.parts.last() {
            if last.0 != "\n" {
                self.nl();
            }
        }

        for (text, style) in self.parts.iter() {
            write(&mut buf, text, style)?;
        }

        bw.print(&buf)?;
        self.parts.clear();
        Ok(())
    }

    pub fn add<S>(&mut self, text: S, style: Option<Style>) -> &mut ColorPrinter
    where
        S: AsRef<str>,
    {
        match self.wrap {
            None => self._add(text, style),
            Some(wr) => {
                let mut l = self.current_line_len();

                for word in text.as_ref().split_whitespace() {
                    let wc = word.chars().count();
                    if l > 0 && l + wc + 1 > wr {
                        self.nl();
                        l = 0;
                    }
                    if l > 0 {
                        l += 1;
                    }
                    self._add(word, style);
                    l += wc;
                }
            }
        };

        self
    }

    pub fn add_plain<S>(&mut self, text: S) -> &mut ColorPrinter
    where
        S: AsRef<str>,
    {
        self.add(text, None)
    }

    pub fn nl(&mut self) -> &mut ColorPrinter {
        self.parts.push(("\n".to_string(), None));
        self
    }

    pub fn indent(&mut self, n: usize) -> &mut ColorPrinter {
        self.parts.push((" ".repeat(n), None));
        self
    }

    /// Set a new line length at which text will be wrapped.
    ///
    /// Setting `n` to 0 disables line wrapping. Wrapping can be changed only at the beginning of
    /// new lines. If `set_line_length` is called at any other place, it automatically inserts a
    /// new line via `ColorPrinter::nl()`.
    pub fn set_line_length(&mut self, n: usize) -> &mut ColorPrinter {
        if !self.is_begin_of_new_line() {
            self.nl();
        }

        self.wrap = match n {
            0 => None,
            i => Some(i),
        };

        self
    }

    fn _add<S>(&mut self, text: S, style: Option<Style>)
    where
        S: AsRef<str>,
    {
        if let Some((ltext, lstyle)) = self.parts.last() {
            if ltext != "\n" {
                if *lstyle == style {
                    self.parts.push((" ".to_string(), style));
                } else {
                    self.parts.push((" ".to_string(), None));
                }
            }
        }
        self.parts.push((text.as_ref().to_string(), style));
    }

    fn current_line_len(&self) -> usize {
        let mut count: usize = 0;
        let mut iters = 0;

        for (text, _) in self.parts.iter().rev() {
            if text == "\n" {
                break;
            }
            count += text.chars().count() + 1;
            iters += 1;
        }

        // include spaces between parts
        if iters > 0 {
            count += iters - 1;
        }

        count
    }

    fn is_begin_of_new_line(&self) -> bool {
        if let Some((ltext, _)) = self.parts.last() {
            return ltext == "\n";
        }
        true
    }
}

fn write<S>(buffer: &mut termcolor::Buffer, text: S, style: &Option<Style>) -> Result<(), io::Error>
where
    S: AsRef<str>,
{
    use termcolor::WriteColor;

    let mut color = termcolor::ColorSpec::new();
    match style {
        Some(Style::Header) => {
            color.set_bold(true);
            color.set_underline(true);
        }
        Some(Style::Literal) => {
            color.set_bold(true);
        }
        None => {}
    }

    buffer.set_color(&color)?;
    buffer.write_all(text.as_ref().as_bytes())?;
    buffer.reset()?;
    Ok(())
}
