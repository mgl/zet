use std::path::Path;

use lazy_static::lazy_static;
use regex::{Regex, RegexBuilder};

use crate::utils::read_lines;

fn grep_for_match<P>(path: P, re: &Regex, linelimit: usize) -> Option<String>
where
    P: AsRef<Path>,
{
    let lines = read_lines(&path);
    if lines.is_err() {
        return None;
    }

    let mut i = 0;
    for lineres in lines.unwrap() {
        i += 1;
        if i > linelimit {
            break;
        }

        let line = lineres.unwrap_or_else(|_| "".to_string());
        if let Some(caps) = re.captures(&line) {
            return caps.get(1).map(|m| m.as_str().to_string());
        }
    }

    None
}

fn read_org<P>(path: P) -> Option<String>
where
    P: AsRef<Path>,
{
    lazy_static! {
        static ref ORG_RE: Regex = RegexBuilder::new(r"#\+TITLE: *(.+)")
            .case_insensitive(true)
            .build()
            .unwrap();
    }
    grep_for_match(&path, &ORG_RE, 2)
}

fn read_md<P>(path: P) -> Option<String>
where
    P: AsRef<Path>,
{
    lazy_static! {
        static ref MD_RE: Regex = RegexBuilder::new(r"^(?:title: *|# *)(.+)").build().unwrap();
    }
    grep_for_match(&path, &MD_RE, 4)
}

fn read_adoc<P>(path: P) -> Option<String>
where
    P: AsRef<Path>,
{
    lazy_static! {
        static ref ADOC_RE: Regex = RegexBuilder::new(r"^= +(.+)").build().unwrap();
    }
    grep_for_match(&path, &ADOC_RE, 20)
}

pub fn get_title<P>(path: P) -> String
where
    P: AsRef<Path>,
{
    let ext = path
        .as_ref()
        .extension()
        .map_or_else(|| Some("txt"), |os| os.to_str());

    let s = match ext {
        Some("org") => read_org(&path),
        Some("md") => read_md(&path),
        Some("adoc") => read_adoc(&path),
        _ => None,
    };

    s.unwrap_or_default()
}
