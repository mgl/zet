use std::cell::{Ref, RefCell};
use std::fs;
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};

use crate::notebook::Notebook;
use crate::titles::get_title;

pub struct Note<'a> {
    abspath: PathBuf,
    nb: Option<&'a Notebook>,
    title: RefCell<Option<String>>,
}

impl Note<'_> {
    pub fn new<P>(path: P, nb: Option<&Notebook>) -> Note
    where
        P: AsRef<Path>,
    {
        let pathref = path.as_ref();
        assert!(pathref.is_absolute());

        Note {
            abspath: pathref.to_path_buf(),
            nb,
            title: RefCell::new(None),
        }
    }

    pub fn path(&self) -> &Path {
        &self.abspath
    }

    pub fn as_relative_path<P>(&self, root: P) -> &Path
    where
        P: AsRef<Path>,
    {
        let pr = self.abspath.as_path();

        // in case note is not relative to root, print its absolute path
        match self.abspath.as_path().strip_prefix(root) {
            Ok(p) => p,
            Err(_) => pr,
        }
    }

    pub fn as_selector<P>(&self, root: P) -> String
    where
        P: AsRef<Path>,
    {
        let pr = self.abspath.as_path();

        // in case note is not relative to the notebook, or doesn't have a notebook, print its
        // absolute path
        if let Some(nb) = self.nb {
            match self.abspath.as_path().strip_prefix(nb.abspath(root)) {
                Ok(p) => format!("{}:{}", nb.name, p.display()),
                Err(_) => pr.to_string_lossy().into_owned(),
            }
        } else {
            pr.to_string_lossy().into_owned()
        }
    }

    /// Fetch a title from the note
    pub fn title(&self) -> Ref<'_, String> {
        if self.title.borrow().is_none() {
            *self.title.borrow_mut() = Some(get_title(&self.abspath));
        }

        Ref::map(self.title.borrow(), |t| t.as_ref().unwrap())
    }

    /// Fetch a title from the note and transform it to "canonical title", which is suitable as a
    /// file path.
    ///
    /// In canonical titles there are no quote characters and additionally many characters like
    /// colons, question and exclamation marks are removed. Whitespaces are joined with a dash.
    pub fn canonical_title(&self) -> String {
        let ct = self.title().to_lowercase();
        ct.trim()
            .replace(['"', '\''], "")
            .split(|ch: char| ch.is_whitespace() || "().?!,:;/-".find(ch).is_some())
            .filter(|s| !s.is_empty())
            .collect::<Vec<&str>>()
            .join("-")
    }
}

pub struct NoteFormatter {
    separator: String,
    root: PathBuf,
    abspath: bool,
    relpath: bool,
    selector: bool,
    title: bool,
    canonical_title: bool,
    created: bool,
    accessed: bool,
    modified: bool,
}

impl NoteFormatter {
    pub fn new(separator: &str, root: &Path) -> NoteFormatter {
        NoteFormatter {
            separator: separator.to_owned(),
            root: root.to_path_buf(),
            abspath: false,
            relpath: false,
            selector: false,
            title: false,
            canonical_title: false,
            created: false,
            accessed: false,
            modified: false,
        }
    }

    pub fn set_abspath(&mut self, val: bool) -> &mut NoteFormatter {
        self.abspath = val;
        self
    }

    pub fn set_relpath(&mut self, val: bool) -> &mut NoteFormatter {
        self.relpath = val;
        self
    }

    pub fn set_selector(&mut self, val: bool) -> &mut NoteFormatter {
        self.selector = val;
        self
    }

    pub fn set_title(&mut self, val: bool) -> &mut NoteFormatter {
        self.title = val;
        self
    }

    pub fn set_canonical_title(&mut self, val: bool) -> &mut NoteFormatter {
        self.canonical_title = val;
        self
    }

    pub fn set_created(&mut self, val: bool) -> &mut NoteFormatter {
        self.created = val;
        self
    }

    pub fn set_accessed(&mut self, val: bool) -> &mut NoteFormatter {
        self.accessed = val;
        self
    }

    pub fn set_modified(&mut self, val: bool) -> &mut NoteFormatter {
        self.modified = val;
        self
    }

    pub fn format(&self, note: &Note) -> String {
        let mut parts: Vec<String> = Vec::new();

        // stat order: atime,mtime,ctime. Reason: ease of sorting for MRU case with e.g.
        // zet api list (... switches ...) | sort
        //
        // - if file was only created, then atime=mtime=ctime
        // - if file was created and then modified, then atime=mtime > ctime
        // - if file was accessed without modification, than atime > mtime >= ctime
        if self.accessed {
            parts.push(
                fs::metadata(note.path())
                    .and_then(|x| x.accessed())
                    .map(since_epoch)
                    .unwrap_or_else(|_| "".to_string()),
            );
        }

        if self.modified {
            parts.push(
                fs::metadata(note.path())
                    .and_then(|x| x.modified())
                    .map(since_epoch)
                    .unwrap_or_else(|_| "".to_string()),
            );
        }

        if self.created {
            parts.push(
                fs::metadata(note.path())
                    .and_then(|x| x.created())
                    .map(since_epoch)
                    .unwrap_or_else(|_| "".to_string()),
            );
        }

        if self.canonical_title {
            parts.push(note.canonical_title());
        }

        if self.title {
            parts.push((*note.title()).clone());
        }

        if self.abspath {
            parts.push(note.path().to_string_lossy().into_owned())
        }

        if self.relpath {
            parts.push(
                note.as_relative_path(&self.root)
                    .to_string_lossy()
                    .into_owned(),
            )
        }

        if self.selector {
            parts.push(note.as_selector(&self.root))
        }

        parts.join(&self.separator)
    }
}

fn since_epoch(t: SystemTime) -> String {
    match t.duration_since(UNIX_EPOCH) {
        Ok(n) => n.as_secs().to_string(),
        Err(_) => "".to_string(),
    }
}
