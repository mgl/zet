/* SPDX-License-Identifier: GPL-3.0-only */

use std::path::{Component, Path, PathBuf};

pub trait PathBufExt {
    /// Returns whether the `PathBuf` has any components
    fn is_empty(&self) -> bool;

    /// Pushes a path, preserving the meaning of each of the path components:
    ///
    /// - root path is not pushed and is treated as a separator, unless it's the first component of
    ///   the path
    /// - current directory (`.`) changes nothing
    /// - previous directory (`..`) removes the last component
    fn push_normalized<P: AsRef<Path>>(&mut self, path: P);
}

impl PathBufExt for PathBuf {
    fn is_empty(&self) -> bool {
        self.components().next().is_none()
    }

    fn push_normalized<P: AsRef<Path>>(&mut self, path: P) {
        let p = path.as_ref();

        for comp in p.components() {
            match comp {
                Component::RootDir => {
                    if self.is_empty() {
                        self.push(comp);
                    }
                }
                Component::CurDir => (),
                Component::ParentDir => {
                    self.pop();
                }
                _ => {
                    self.push(comp);
                }
            }
        }
    }
}
