/* SPDX-Ltcense-Identifier: GPL-3.0-only */

use std::ffi::OsStr;
use std::io;
use std::path::{Path, PathBuf};

// Yes, I called it a "hooker". I like this name, ok?
pub struct Hooker {
    hook_dir: PathBuf,
    disabled: bool,
}

impl Hooker {
    pub fn new<P>(zet_dir: P) -> Hooker
    where
        P: AsRef<Path>,
    {
        assert_eq!(zet_dir.as_ref().file_name().unwrap(), ".zet");
        Hooker {
            hook_dir: zet_dir.as_ref().join("hooks"),
            disabled: false,
        }
    }

    pub fn set_disabled(&mut self, disabled: bool) -> &mut Hooker {
        self.disabled = disabled;
        self
    }

    pub fn get<S>(&self, name: S) -> HookCommand
    where
        S: AsRef<str>,
    {
        let mut hc = HookCommand {
            name: name.as_ref().to_string(),
            command: None,
        };

        if self.disabled {
            return hc;
        }

        let exe = self.hook_dir.join(name.as_ref());
        if exe.try_exists().unwrap_or(false) {
            hc.command = Some(std::process::Command::new(exe));
        }

        hc
    }
}

pub struct HookCommand {
    name: String,
    command: Option<std::process::Command>,
}

impl HookCommand {
    pub fn arg<S>(&mut self, lhs: S) -> &mut HookCommand
    where
        S: AsRef<OsStr>,
    {
        if let Some(c) = &mut self.command {
            c.arg(lhs);
        }

        self
    }

    pub fn args<S, I>(&mut self, it: I) -> &mut HookCommand
    where
        S: AsRef<OsStr>,
        I: IntoIterator<Item = S>,
    {
        if let Some(c) = &mut self.command {
            c.args(it);
        }

        self
    }

    pub fn envs<I, S>(&mut self, it: I) -> &mut HookCommand
    where
        S: AsRef<OsStr>,
        I: IntoIterator<Item = (S, S)>,
    {
        if let Some(c) = &mut self.command {
            c.envs(it);
        }

        self
    }

    /// Convert Command's Result<ExitStatus> to the result which denotes whether a hook failed or
    /// succeeded.
    ///
    /// Rules are:
    ///
    ///     - no command => no hook configured, success => Ok("")
    ///     - Err(_) => hook wasn't run at (no executable bit maybe?)
    ///       all and thus it succeeded => Ok("")
    ///     - Ok(ExitStatus(code = 0)) => hook succeeded => Ok("")
    ///     - Ok(ExitStatus(code > 0) => hook failed => Err("msg")
    ///     - Ok(ExitStatus(code < 0) => hook failed (signal) => Err("msg")
    ///     - Ok(ExitStatus(no code) => hook failed (signal) => Err("msg")
    pub fn status(&mut self) -> io::Result<&str> {
        if self.command.is_none() {
            return Ok("");
        }

        let status = self.command.as_mut().unwrap().status();
        if status.is_err() {
            return Ok("");
        }

        let code = status.unwrap().code().unwrap_or(-1);

        match code {
            0 => Ok(""),
            -1 => Err(io::Error::new(
                io::ErrorKind::Interrupted,
                "interrupted by a signal",
            )),
            _ => Err(io::Error::new(io::ErrorKind::Interrupted, "hook failed")),
        }
    }

    pub fn status_report(&mut self) -> bool {
        if let Err(e) = self.status() {
            let n = &self.name;
            eprintln!("{}: {}", n, e);
            eprintln!("{}: you may disable hooks with 'zet --no-hooks ...", n);
            return false;
        }
        true
    }
}
