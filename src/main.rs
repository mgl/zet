/* SPDX-Ltcense-Identifier: GPL-3.0-only */

use std::collections::HashMap;
use std::env;
use std::error;
use std::fs::{create_dir, create_dir_all, remove_dir_all, OpenOptions};
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process::{exit, Command};

use clap::Parser;
use dirs::{data_dir, data_local_dir, home_dir};
use toml::value::Table;
use toml::Value;
use walkdir::{DirEntry, WalkDir};

use config::{Config, ConfigInfo};
use hooks::Hooker;
use notebook::Notebook;
use utils::{kind_to_str, read_lines};

mod api;
mod args;
mod color;
mod config;
mod hooks;
mod note;
mod notebook;
mod pathext;
mod titles;
mod utils;

type Result<T> = ::std::result::Result<T, Box<dyn error::Error>>;

fn init(path: &str, name: &str) -> Result<()> {
    let config = Config {
        notebooks: vec![Notebook {
            name: name.to_string(),
            relpath: path.to_string(),
            extra: HashMap::new(),
        }],
        commands: None,
    };

    let ts = toml::to_string(&config)?;

    create_dir(".zet").map_err(|err| format!(".zet: {}", kind_to_str(&err.kind())))?;
    create_dir(".zet/hooks").map_err(|err| format!(".zet/hooks: {}", kind_to_str(&err.kind())))?;

    if let Err(err) = create_dir_all(path) {
        remove_dir_all(".zet").ok();
        return Err(format!("{}: {}", path, kind_to_str(&err.kind())).into());
    };

    let mut file = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(".zet/zet.toml")?;

    file.write_all(ts.as_bytes())?;
    Ok(())
}

fn read_command_list(entry: &DirEntry) -> HashMap<String, String> {
    let mut hm = HashMap::new();
    if let Ok(lines) = read_lines(entry.path()) {
        for lineres in lines {
            let line = lineres.unwrap_or_else(|_| "".to_string());
            if let Some((cmd, desc)) = line.trim().split_once(':') {
                let cmdt = cmd.trim();
                if !cmdt.is_empty() && !hm.contains_key(cmdt) {
                    hm.insert(cmdt.to_string(), desc.trim().to_string());
                }
            }
        }
    }

    hm
}

fn print_commands(commands: &Vec<String>, descriptions: &HashMap<String, String>) -> Result<()> {
    use color::{ColorPrinter, Style};

    let default_desc = "".to_string();
    let wrl = 90;

    let mut cp = ColorPrinter::new();
    cp.set_line_length(wrl);

    cp.add("usage:", Some(Style::Header))
        .add("zet", Some(Style::Literal))
        .add_plain("<command> [options] [args]");
    cp.nl().nl();

    cp.add_plain(
        r#"
        These are non-core commands which zet found in $PATH and $ZET_MODULES_PATH.
        More details about each of them are usually available by typing 'zet <command> --help'.
        "#,
    );

    cp.nl().nl();
    cp.add("Commands:", Some(Style::Header));
    cp.nl();
    for cmd in commands {
        let desc = descriptions.get(cmd.as_str()).unwrap_or(&default_desc);
        cp.indent(2).add(cmd, Some(Style::Literal)).nl();

        for line in textwrap::wrap(desc, wrl - 10) {
            cp.indent(10).add_plain(&line).nl();
        }
    }

    cp.print()?;
    Ok(())
}

fn print_commands_raw(
    commands: &Vec<String>,
    descriptions: &HashMap<String, String>,
) -> Result<()> {
    let default_desc = "".to_string();

    for cmd in commands {
        let desc = descriptions.get(cmd.as_str()).unwrap_or(&default_desc);
        println!("{}:{}", &cmd, &desc);
    }

    Ok(())
}

fn get_zet_search_paths() -> Vec<String> {
    let mut paths: Vec<String> = modules_path()
        .split(':')
        .filter(|s| !s.is_empty())
        .map(|s| s.to_string())
        .collect();
    paths.extend(
        env::var("PATH")
            .unwrap_or_else(|_| "".to_string())
            .split(':')
            .filter(|s| !s.is_empty())
            .map(|s| s.to_string()),
    );

    paths
}

fn get_zet_commands(paths: &[String]) -> (Vec<String>, HashMap<String, String>) {
    fn is_zet(entry: &DirEntry) -> bool {
        entry
            .file_name()
            .to_str()
            .map(|s| s.starts_with("zet-"))
            .unwrap_or(false)
    }

    fn is_command_list(entry: &DirEntry) -> bool {
        entry
            .file_name()
            .to_str()
            .map(|s| s == "command-list.txt")
            .unwrap_or(false)
    }

    #[cfg(target_family = "unix")]
    fn is_executable(entry: &DirEntry) -> bool {
        use std::os::unix::fs::PermissionsExt;
        entry
            .metadata()
            .map(|m| m.permissions().mode() & 0o111 != 0)
            .unwrap_or(false)
    }

    #[cfg(target_family = "windows")]
    fn is_executable(entry: &DirEntry) -> bool {
        return true;
    }

    let mut descriptions: HashMap<String, String> = HashMap::new();
    let mut cmds: Vec<String> = Vec::new();

    // reverse iteration has no effect for the list of commands (they're sorted and deduplicated
    // anyway). For descriptions it will overwrite (via HashMap.extend()) descriptions of commands
    // earlier in ZET_MODULES_PATH, so the description will match the command which is actually
    // run. This still isn't perfect, but better than normal order.
    for dir in paths.iter().rev() {
        let walker = WalkDir::new(dir).max_depth(1).into_iter();
        for entry in walker.flatten() {
            if is_zet(&entry) && is_executable(&entry) {
                let name = entry
                    .file_name()
                    .to_str()
                    .unwrap_or("")
                    .strip_prefix("zet-")
                    .unwrap_or("");
                cmds.push(name.to_string());
            } else if is_command_list(&entry) {
                let cl = read_command_list(&entry);
                descriptions.extend(cl);
            }
        }
    }

    cmds.sort_unstable();
    cmds.dedup();

    (cmds, descriptions)
}

fn list_modules(list: &bool) -> Result<()> {
    let paths = get_zet_search_paths();
    let (cmds, descriptions) = get_zet_commands(&paths);

    match list {
        true => print_commands_raw(&cmds, &descriptions),
        false => print_commands(&cmds, &descriptions),
    }
}

fn path_to_string<P>(p: P, default: &str) -> String
where
    P: AsRef<Path>,
{
    return p.as_ref().to_str().unwrap_or(default).to_string();
}

fn prepare_env(ci: &ConfigInfo) -> HashMap<String, String> {
    fn to_string(lhs: &Value) -> String {
        // represents a Value in appropriate way (e.g. by default, toml will
        // display strings surrounded by quotes, i.e. the way  they're typed
        // in a configuration file
        match lhs {
            Value::String(s) => s.to_string(),
            Value::Array(a) => {
                let mut out: Vec<String> = Vec::new();
                for val in a {
                    out.push(to_string(val));
                }
                out.join(";")
            }
            _ => lhs.to_string().trim().to_string(),
        }
    }

    fn esc(s: &str) -> String {
        s.to_uppercase().replace(' ', "_")
    }

    let mut env: HashMap<String, String> = HashMap::new();
    let zet_config = ci.path.as_path();
    let zet_dir = zet_config.parent().unwrap();
    let zet_root = zet_dir.parent().unwrap();
    let zet_no_hooks = env::var("ZET_NO_HOOKS");

    env.insert("ZET_CONFIG".to_string(), path_to_string(zet_config, ""));
    env.insert("ZET_DIR".to_string(), path_to_string(zet_dir, ""));
    env.insert("ZET_ROOT".to_string(), path_to_string(zet_root, ""));

    if let Ok(v) = zet_no_hooks {
        env.insert("ZET_NO_HOOKS".to_string(), v);
    }

    let mpath = modules_path();
    let path = env::var("PATH").unwrap_or_else(|_| "".to_string());
    if mpath.is_empty() {
        env.insert("PATH".to_string(), path);
    } else {
        env.insert("PATH".to_string(), format!("{}:{}", mpath, path));
    }
    env.insert("ZET_MODULES_PATH".to_string(), mpath);
    env.insert("ZET_LIB_DIR".to_string(), lib_dir());

    let mut notebooks: Vec<String> = Vec::new();
    for nb in &ci.config.notebooks {
        let ename = esc(&nb.name);
        let prefix = format!("ZET_NOTEBOOK_{}", ename);

        env.insert(format!("{}_NAME", prefix), nb.name.to_string());
        env.insert(format!("{}_PATH", prefix), nb.relpath.to_string());

        for (key, val) in &nb.extra {
            env.insert(format!("{}_{}", prefix, esc(key)), to_string(val));
        }
        notebooks.push(ename);
    }

    if let Some(commands) = &ci.config.commands {
        for (name, cmd) in commands {
            for (opt, val) in &cmd.opts {
                let var = format!("ZET_COMMAND_{}_{}", esc(name), esc(opt));
                env.insert(var, to_string(val));
            }
        }
    }

    env.insert("ZET_NOTEBOOKS".to_string(), notebooks.join(":"));
    env
}

fn search_data_dirs(dirname: &str) -> Option<PathBuf> {
    let search_dirs = &[
        data_local_dir(),
        data_dir(),
        Some(PathBuf::from(r"/usr/local/share")),
        Some(PathBuf::from(r"/usr/share")),
    ];

    for path in search_dirs.iter().flatten() {
        let mut searched = path.join("zet");
        searched.push(dirname);
        if searched.is_dir() {
            return Some(searched.clone());
        }
    }
    None
}

fn modules_path() -> String {
    if let Ok(mpath) = env::var("ZET_MODULES_PATH") {
        return mpath;
    }

    let mut v: Vec<String> = Vec::new();

    if let Some(path) = search_data_dirs("modules") {
        if let Some(s) = path.to_str() {
            v.push(s.to_string());
        }
    }

    #[cfg(target_family = "unix")]
    {
        let system_path = "/usr/share/zet/modules".to_string();
        if !v.contains(&system_path) {
            v.push(system_path);
        }
    }

    v.join(":")
}

fn lib_dir() -> String {
    if let Ok(libdir) = env::var("ZET_LIB_DIR") {
        return libdir;
    }

    if let Some(path) = search_data_dirs("lib") {
        if let Some(s) = path.to_str() {
            return s.to_string();
        }
    }

    "".to_string()
}

fn print_env(ci: &ConfigInfo) {
    let env = prepare_env(ci);
    println!("{:#?}", env);
}

fn get_command_option<'a>(subc: &str, opt: &str, config: &'a Config) -> Option<&'a Value> {
    config
        .commands
        .as_ref()
        .and_then(|c| c.get(subc))
        .and_then(|cmdopts| cmdopts.opts.get(opt))
}

fn expand_home(val: &str) -> String {
    if !val.starts_with("~/") {
        return val.to_string();
    }

    home_dir()
        .map(|mut hd| {
            if hd == Path::new("/") {
                val[1..val.len()].to_string()
            } else {
                hd.push(&val[2..val.len()]);
                path_to_string(hd, val)
            }
        })
        .unwrap_or_else(|| val.to_string())
}

fn parse_args(table: &Table) -> Result<(Vec<String>, Vec<String>)> {
    fn interpret_value(val: &str) -> String {
        expand_home(val.trim())
    }

    fn push_opt(key: &str, val: &str, opts: &mut Vec<String>, args: &mut Vec<String>) {
        let name = key.trim();

        if name == "arguments" {
            args.push(interpret_value(val))
        } else {
            opts.push(format!("--{}", &name));
            opts.push(interpret_value(val));
        }
    }

    fn push_bool(key: &str, opts: &mut Vec<String>) {
        opts.push(format!("--{}", key.trim()));
    }

    let mut opts: Vec<String> = Vec::new();
    let mut pos: Vec<String> = Vec::new();

    for (key, val) in table {
        match val {
            Value::String(x) => push_opt(key, x, &mut opts, &mut pos),
            Value::Integer(x) => push_opt(key, &x.to_string(), &mut opts, &mut pos),
            Value::Float(x) => push_opt(key, &x.to_string(), &mut opts, &mut pos),
            Value::Boolean(x) => {
                if *x {
                    push_bool(key, &mut opts)
                }
            }
            Value::Array(a) => {
                for elem in a {
                    match elem {
                        Value::String(x) => push_opt(key, x, &mut opts, &mut pos),
                        Value::Integer(x) => push_opt(key, &x.to_string(), &mut opts, &mut pos),
                        Value::Float(x) => push_opt(key, &x.to_string(), &mut opts, &mut pos),
                        _ => {
                            return Err(format!("invalid type in array '{}'", &key).into());
                        }
                    }
                }
            }
            _ => return Err(format!("invalid type for '{}'", &key).into()),
        }
    }

    Ok((opts, pos))
}

fn collect_defaults(subc: &str, args: &[String], config: &Config) -> Result<Vec<String>> {
    if let Some(defaults) = get_command_option(subc, "defaults", config) {
        let dt = defaults
            .as_table()
            .ok_or_else(|| format!("zet.toml command.{}.defaults: expected a table", &subc))?;
        return match parse_args(dt) {
            Ok((options, posargs)) => {
                let mut ret = Vec::new();
                ret.extend(options);
                ret.extend(posargs);
                ret.extend_from_slice(args);
                Ok(ret)
            }
            Err(e) => Err(format!("zet.toml command.{}: {}", &subc, &e).into()),
        };
    }

    Ok(args.into())
}

fn run_subcommand(subc: &String, args: &[String], ci: &ConfigInfo, hooker: &Hooker) -> bool {
    let env = prepare_env(ci);
    let subc_args = std::iter::once(subc).chain(args.iter());

    if !hooker
        .get("pre-command")
        .args(subc_args.clone())
        .envs(&env)
        .status_report()
    {
        return false;
    }

    let zetcmd = format!("zet-{}", subc);
    let stres = Command::new(zetcmd).args(args).envs(&env).status();
    if stres.is_err() {
        return false;
    }

    let status = stres.unwrap();

    if !hooker
        .get("post-command")
        .arg((!status.success() as i32).to_string())
        .args(subc_args.clone())
        .envs(&env)
        .status_report()
    {
        return false;
    }

    status.success()
}

fn run_api(cmd: &api::ApiCmd, ci: &ConfigInfo) -> Result<()> {
    let env = prepare_env(ci);
    api::Api::new(ci, &env)
        .run(cmd)
        .map_err(|err| format!("{}", err).into())
}

fn is_feat_disabled(varname: &str, cli: bool) -> bool {
    let var = env::var(varname).unwrap_or_else(|_| "0".to_string());
    cli || (var == "1")
}

fn try_main() -> Result<()> {
    let mut args = args::Args::parse();

    if let Some(dir) = &args.directory {
        env::set_current_dir(dir)?
    }

    match args.command {
        args::Subcommands::Init { path, name } => {
            init(&path, &name)?;
        }
        args::Subcommands::LsCommands { list } => {
            list_modules(&list)?;
        }
        args::Subcommands::PrintEnv => {
            let ci = ConfigInfo::new(&args.config)?;
            print_env(&ci);
        }
        args::Subcommands::CallApi(cmd) => {
            let ci = ConfigInfo::new(&args.config)?;
            run_api(&cmd.command, &ci)?
        }
        args::Subcommands::Other(ref mut subc) => {
            let ci = ConfigInfo::new(&args.config)?;
            let mut hooker = Hooker::new(ci.get_zet_dir());

            hooker.set_disabled(is_feat_disabled("ZET_NO_HOOKS", args.no_hooks));

            let subc_args = if is_feat_disabled("ZET_NO_DEFAULTS", args.no_defaults) {
                subc[1..subc.len()].to_vec()
            } else {
                collect_defaults(&subc[0], &subc[1..subc.len()], &ci.config)?
            };

            let success = run_subcommand(&subc[0], &subc_args, &ci, &hooker);
            if !success {
                exit(2);
            }
        }
    }
    Ok(())
}

fn main() {
    if let Err(err) = try_main() {
        eprintln!("{}", err);
        exit(1);
    }
}
