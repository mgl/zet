/* SPDX-License-Identifier: GPL-3.0-only */

use std::fs::File;
use std::io::{self, BufRead, ErrorKind};
use std::path::Path;

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn kind_to_str(err: &ErrorKind) -> &str {
    match err {
        ErrorKind::NotFound => "file or directory not found",
        ErrorKind::PermissionDenied => "permission denied",
        ErrorKind::BrokenPipe => "broken pipe",
        ErrorKind::AlreadyExists => "already exists",
        ErrorKind::InvalidInput => "invalid input",
        ErrorKind::InvalidData => "invalid data",
        ErrorKind::TimedOut => "time out",
        ErrorKind::Interrupted => "interrupted",
        ErrorKind::Unsupported => "unsupported",
        ErrorKind::UnexpectedEof => "unexpected end of file",
        ErrorKind::OutOfMemory => "out of memory",
        _ => "other error",
    }
}
