/* SPDX-License-Identifier: GPL-3.0-only */

use std::cell::RefCell;
use std::collections::HashMap;
use std::error;
use std::io::Write;
use std::iter;
use std::path::{Path, PathBuf};
use std::process;

use clap::{Args, Parser};
use walkdir::{DirEntry, WalkDir};

use crate::color::{ColorPrinter, Style};
use crate::config::ConfigInfo;
use crate::note::{Note, NoteFormatter};
use crate::notebook::{Notebook, NotebookFormatter};
use crate::pathext::PathBufExt;
use crate::utils::read_lines;

type Environment = HashMap<String, String>;
type ApiResult<T> = ::std::result::Result<T, Box<dyn error::Error>>;

#[derive(Debug, PartialEq, Eq, Args)]
pub struct ApiCommand {
    #[command(subcommand)]
    pub command: ApiCmd,
}

#[derive(Debug, PartialEq, Eq, Parser)]
pub enum ApiCmd {
    /// Get list of configured zet notebooks.
    ///
    /// Output is controlled by formatting switches which enable printing particular fields. Fields
    /// are separated with a field separator FS ('::' by default), set with --field-separator.
    ///
    /// When using --selector together with other fields, it is recommended to change field
    /// separator.
    ///
    /// Notebooks are printed in order in which they are requested, or in order in which they are
    /// configured in zet.toml.
    ///
    /// Order of fields is: {name}<FS>{absolute}<FS>{relative}<FS>{selector}
    Notebooks {
        /// Print only notebooks with the following names. If empty, all configured notebooks will
        /// be printed.
        nbs: Vec<String>,

        #[arg(long)]
        /// Print notebook names. This is on by default when no other options are chosen
        name: bool,

        #[arg(long)]
        /// Print notebook names as selectors, adding a colon after each of them
        selector: bool,

        #[arg(long)]
        /// Print notebook absolute paths
        absolute: bool,

        #[arg(long)]
        /// Print notebook paths relative to $ZET_ROOT
        relative: bool,

        #[arg(long, default_value = "::", value_name = "FS")]
        /// Separator string to put between printed fields
        field_separator: String,
    },

    /// Get the current default notebook.
    ///
    /// Default notebook is read either from $ZET_ROOT/.zet/default-notebook, or it is first
    /// configured notebook in zet.toml
    ///
    /// Printed data may be formatted similar to 'zet api notebooks' output.
    DefaultNotebook {
        #[arg(long)]
        /// Print notebook name. This is on by default when no other options are chosen
        name: bool,

        #[arg(long)]
        /// Print notebook name as selector, adding a colon after it
        selector: bool,

        #[arg(long)]
        /// Print notebook absolute path
        absolute: bool,

        #[arg(long)]
        /// Print notebook path relative to $ZET_ROOT
        relative: bool,

        #[arg(long, default_value = "::", value_name = "FS")]
        /// Separator string to put between printed fields
        field_separator: String,
    },

    #[command(name = "getenvnb")]
    /// Get environment variable specific for a given notebook.
    ///
    /// Both variable and notebook names are case insensitive.
    ///
    /// Examples:
    ///
    /// zet api getenvnb path mynotebook -> ${ZET_NOTEBOOK_MYNOTEBOOK_PATH}
    GetEnvNb { var: String, nb: String },

    #[command(name = "getenvnbsel")]
    /// Get environment variable specific for a notebook which is extracted from a selector.
    ///
    /// Variable is case insensitive, but selector is case sensitive. When selector doesn't
    /// explicitly point to any particular notebook, the default notebook is used (see: zet api
    /// default-notebook).
    ///
    /// Examples:
    ///
    /// zet api getenvnbsel path mynotebook:dir/file -> ${ZET_NOTEBOOK_MYNOTEBOOK_PATH}
    GetEnvNbSel { var: String, sel: String },

    /// Expand selectors into system paths.
    ///
    /// Paths can be either ordinary absolute paths or selectors in form of
    /// [notebook]:[subdir/][file.ext]. In the second form selector expansion rules apply.
    ///
    /// Examples:
    ///
    /// zet api paths 0:file.md -> /home/user/docs/0/file.md
    ///
    /// zet api paths 0:journals/today.md -> /home/user/docs/0/journals/today/file.md
    Paths {
        selectors: Vec<String>,

        #[arg(long)]
        /// Print paths as relative to $ZET_ROOT
        relative: bool,
    },

    /// Tell via exit status whether a given selector is a file or directory.
    ///
    /// When is-file exits with status 0, the path is a file. When it returns with status 1, the
    /// path is a directory. Other status values indicate other errors and shouldn't be used to
    /// reason about passed paths.
    ///
    /// Selectors are automatically expanded to absolute paths.
    ///
    /// When path exists, is-file reports its final status, after traversing symlinks. When path
    /// doesn't exists, is-file assumes that paths which end with a path separator (slash or
    /// backslash) are directories and paths which don't - are files.
    IsFile { selector: String },

    /// Get list of all existing, not hidden files inside a path.
    ///
    /// If path is a file, it will be yielded. If it is a directory, all files inside it will be
    /// yielded, recursively.
    ///
    /// Output is controlled by formatting switches which enable printing particular fields. Fields
    /// are separated with a field separator FS ('::' by default), set with --field-separator.
    ///
    /// Order of fields is:
    /// {accessed}<FS>{modified}<FS>{created}<FS>{canonical-title}<FS>{title}<FS>{absolute}<FS>{relative}<FS>{selector}
    List {
        selectors: Vec<String>,

        #[arg(long)]
        /// Print absolute paths. This is on by default when no other options are chosen
        absolute: bool,

        #[arg(long)]
        /// Print paths as relative to $ZET_ROOT.
        relative: bool,

        #[arg(long)]
        /// Print paths in selector notation. Absolute paths will be used for files outside of
        /// notebooks.
        selector: bool,

        #[arg(long)]
        /// Formatting switch to print note titles. Might return empty string.
        title: bool,

        #[arg(long)]
        /// Formatting switch to print canonical titles, suitable for file names. Might return
        /// empty string.
        canonical_title: bool,

        #[arg(long)]
        /// Print file creation time in seconds since Unix Epoch
        created: bool,

        #[arg(long)]
        /// Print file modification time in seconds since Unix Epoch
        modified: bool,

        #[arg(long)]
        /// Print file access time in seconds since Unix Epoch
        accessed: bool,

        #[arg(long, default_value = "::", value_name = "FS")]
        /// Separator string to put between printed fields
        field_separator: String,
    },

    /// Transforms file names in a way which ensures that resulting files don't exist.
    ///
    /// Transformation is performed by spliting the file name (basename) into file stem and
    /// extension. Stem is then transformed by concatenating it with consecutive numbers until they
    /// form a file name which is not present yet.
    ///
    /// For example, if note.md and note-1.md were present in the notes directory, then zet would
    /// generate note-2.md.
    ///
    /// If selector points to a directory, provided stem and extension will be used instead. In
    /// case there's no extension provided, zet will try to use the one configured for a notebook
    /// in 'default_filetype' configuration option.
    UniqueFiles {
        selectors: Vec<String>,

        #[arg(default_value = "note", long)]
        /// File stem (basename's part before extension) used when selector expands to a directory.
        stem: String,

        #[arg(long)]
        /// File extension (without a dot) used when selector expands to a directory. If not
        /// provided, notebook's configured default_filetype will be used, or "md".
        extension: Option<String>,
    },

    /// Get absolute path of notes root directory
    Root,

    /// Print with style
    ///
    /// Print stylish strings aligned with the stylesheet of zet. This command doesn't
    /// automatically check whether the output is TTY (use '--tty-check' for that), but respects
    /// the NO_COLOR environment variable.
    ///
    /// Each argument passed to 'zet print' will be printed on a new line. You are thus advised to
    /// collect all strings which you want to collect and print them all at once, through a single
    /// 'zet print' call.
    ///
    /// This API command requires '--style' switch, meaning that unstyled prints are not supported.
    /// Virtually all programming languages have built-in ways of outputting unstyled text to the
    /// terminal, which should be preferred due to the overhead of executing zet process for such
    /// simple task.
    Print {
        lines: Vec<String>,

        #[arg(short, long)]
        style: Style,

        #[arg(long)]
        tty_check: bool,
    },
}

pub struct Api<'a> {
    pub ci: &'a ConfigInfo,
    pub env: &'a Environment,

    defnb: RefCell<Option<String>>,
}

impl<'a> Api<'a> {
    pub fn new(ci: &'a ConfigInfo, env: &'a Environment) -> Api<'a> {
        Api {
            ci,
            env,
            defnb: RefCell::new(None),
        }
    }

    /// Main entry point for API command
    pub fn run(&self, cmd: &ApiCmd) -> ApiResult<()> {
        match cmd {
            ApiCmd::Notebooks {
                nbs,
                name,
                selector,
                absolute,
                relative,
                field_separator,
            } => {
                let mut nf = NotebookFormatter::new(field_separator, self.ci.get_zet_root());
                let needs_name = *name || (!absolute && !relative && !selector);

                nf.set_abspath(*absolute)
                    .set_relpath(*relative)
                    .set_name(needs_name)
                    .set_selector(*selector);

                if nbs.is_empty() {
                    print_lines(self.ci.config.notebooks.iter().map(|x| nf.format(x)));
                } else {
                    let filtered = self.filter_notebooks(nbs)?;
                    print_lines(filtered.iter().map(|x| nf.format(x)));
                    if filtered.len() != nbs.len() {
                        process::exit(1);
                    }
                }
            }
            ApiCmd::DefaultNotebook {
                name,
                selector,
                absolute,
                relative,
                field_separator,
            } => {
                let mut nf = NotebookFormatter::new(field_separator, self.ci.get_zet_root());
                let needs_name = *name || (!absolute && !relative && !selector);

                nf.set_abspath(*absolute)
                    .set_relpath(*relative)
                    .set_name(needs_name)
                    .set_selector(*selector);

                let defnb = &self.default_notebook()?;
                match self.find_notebook(defnb) {
                    Some(nb) => print_lines(iter::once(nf.format(nb))),
                    None => {
                        eprintln!("Notebook not found: {}", defnb);
                        process::exit(1);
                    }
                };
            }
            ApiCmd::GetEnvNb { var, nb } => {
                print_lines(iter::once(self.getenvnb(var, nb)?));
            }
            ApiCmd::GetEnvNbSel { var, sel } => {
                print_lines(iter::once(self.getenvnbsel(var, sel)?));
            }
            ApiCmd::Paths {
                selectors,
                relative,
            } => {
                print_lines(self.paths(selectors, *relative)?);
            }
            ApiCmd::IsFile { selector } => match self.is_file(selector) {
                Ok(true) => process::exit(0),
                Ok(false) => process::exit(1),
                Err(e) => {
                    eprintln!("{}", e);
                    process::exit(2);
                }
            },
            ApiCmd::List {
                selectors,
                absolute,
                relative,
                selector,
                title,
                canonical_title,
                created,
                modified,
                accessed,
                field_separator,
            } => {
                let mut nf = NoteFormatter::new(field_separator, self.ci.get_zet_root());
                let needs_abspath = *absolute
                    || (!relative
                        && !selector
                        && !title
                        && !canonical_title
                        && !created
                        && !modified
                        && !accessed);

                nf.set_abspath(needs_abspath)
                    .set_relpath(*relative)
                    .set_selector(*selector)
                    .set_title(*title)
                    .set_canonical_title(*canonical_title)
                    .set_created(*created)
                    .set_modified(*modified)
                    .set_accessed(*accessed);

                print_lines(self.list_files(selectors)?.iter().map(|x| nf.format(x)));
            }
            ApiCmd::UniqueFiles {
                selectors,
                stem,
                extension,
            } => {
                print_lines(self.unique_files(selectors, stem, extension.as_ref())?);
            }
            ApiCmd::Root => {
                print_lines(iter::once(
                    self.ci.get_zet_root().to_string_lossy().into_owned(),
                ));
            }
            ApiCmd::Print {
                lines,
                style,
                tty_check,
            } => {
                print_styled(lines, *style, *tty_check)?;
            }
        }

        Ok(())
    }

    fn getenv(&self, var: &str) -> Option<&str> {
        if let Some(val) = self.env.get(var.trim()) {
            return Some(val.trim());
        }
        None
    }

    fn getenverr(&self, var: &str) -> ApiResult<&str> {
        self.getenv(var)
            .ok_or_else(|| format!("{} environment variable not set", var).into())
    }

    fn getenvnb(&self, var: &str, notebook: &str) -> ApiResult<&str> {
        let real_nb = notebook.replace(' ', "_").to_uppercase();
        let real_var = format!("ZET_NOTEBOOK_{}_{}", real_nb, var.to_uppercase());
        self.getenverr(&real_var)
    }

    /// Return environment variable related to the notebook given by a selector. This can use
    /// default notebook if selector doesn't use explicit notebook.
    fn getenvnbsel(&self, var: &str, selector: &str) -> ApiResult<&str> {
        if let Some(nb) = self.get_notebook_from_selector(selector) {
            return self.getenvnb(var, &nb.name);
        }

        Err(format!(
            "selector '{}' doesn't point to any existing notebook",
            selector
        )
        .into())
    }

    fn get_notebook_from_selector(&self, selector: &'a str) -> Option<&Notebook> {
        let path = Path::new(&selector);
        if path.is_absolute() {
            return self.find_notebook_on_path(path);
        }

        match selector.split_once(':') {
            Some((nb, _)) => self.find_notebook(nb),
            None => self
                .default_notebook()
                .ok()
                .and_then(|name| self.find_notebook(&name)),
        }
    }

    fn find_notebook(&self, name: &str) -> Option<&Notebook> {
        self.ci.config.notebooks.iter().find(|&nb| nb.name == name)
    }

    fn find_notebook_on_path(&self, path: &Path) -> Option<&Notebook> {
        // TODO: nested notebooks?
        self.ci.config.notebooks.iter().find(|&nb| {
            if path.is_absolute() {
                return path.starts_with(nb.abspath(self.ci.get_zet_root()));
            }
            path.starts_with(&nb.relpath)
        })
    }

    fn filter_notebooks(&self, requested: &Vec<String>) -> ApiResult<Vec<&Notebook>> {
        if requested.is_empty() {
            return Err("empty requested vector of notebooks".into());
        }

        #[rustfmt::skip]
        let nbset: HashMap<&str, &Notebook> = HashMap::from_iter(
            self.ci.config.notebooks
                .iter()
                .map(|nb| (nb.name.as_str(), nb)),
        );

        let ret = requested
            .iter()
            .filter_map(|name| nbset.get(name.as_str()))
            .copied()
            .collect();

        Ok(ret)
    }

    fn notebook_abspath(&self, name: &str) -> Option<String> {
        self.find_notebook(name).and_then(|nb| {
            let path = PathBuf::from(&self.ci.get_zet_root()).join(nb.relpath.clone());
            path.to_str().map(|s| s.to_string())
        })
    }

    fn find_default_notebook(&self) -> Option<String> {
        let root = self.ci.get_zet_root();
        let path = PathBuf::from(root).join(".zet/default-notebook");

        let mut found: String = String::new();

        if let Ok(mut file_lines) = read_lines(path) {
            let first = file_lines
                .next()
                .map(|res| res.unwrap_or_else(|_| "".to_string()))
                .unwrap_or_else(|| "".to_string());
            let trimmed = first.trim();

            if self.find_notebook(trimmed).is_some() {
                found = trimmed.to_string();
            }
        }

        if found.is_empty() {
            found = self
                .ci
                .config
                .notebooks
                .first()
                .map(|nb| nb.name.trim())
                .unwrap_or("")
                .to_string()
        }

        if !found.is_empty() {
            return Some(found);
        }

        None
    }

    /// Read the default notebook. This involves some disk I/O, so the result is cached.
    fn default_notebook(&self) -> ApiResult<String> {
        let mut defnb = self.defnb.borrow_mut();
        if defnb.is_none() {
            *defnb = match self.find_default_notebook() {
                Some(nb) => Some(nb),
                None => {
                    return Err("Default notebook couldn't be found".into());
                }
            }
        }

        Ok(defnb.as_ref().unwrap().clone())
    }

    fn partition_selector(&self, selector: &str) -> ApiResult<(String, String)> {
        let (notebook, file) = if let Some((wanted_nb, path_inside)) = selector.split_once(':') {
            let nb = self
                .find_notebook(wanted_nb)
                .ok_or_else(|| format!("Notebook not found: {}", wanted_nb))?;
            let nbpath = nb.abspath(self.ci.get_zet_root());
            (
                nbpath.to_string_lossy().into_owned(),
                path_inside.to_string(),
            )
        } else {
            let defnb = self.default_notebook()?;
            let notebook = match self.notebook_abspath(&defnb) {
                Some(nb) => nb,
                None => {
                    return Err(format!("Unknown notebook: {}", defnb).into());
                }
            };
            (notebook, selector.to_string())
        };

        Ok((notebook, file))
    }

    fn convert_to_path(&self, selector: &str) -> ApiResult<String> {
        if Path::new(&selector).is_absolute() {
            return Ok(selector.to_string());
        }

        let (notebook, file) = self.partition_selector(selector)?;

        let mut path = PathBuf::from(notebook);
        if !file.is_empty() {
            path.push_normalized(file);
        }

        path.to_str()
            .map(|s| s.to_string())
            .ok_or_else(|| "Failed to decode non-unicode path".into())
    }

    fn paths(&self, selectors: &[String], relative: bool) -> ApiResult<Vec<String>> {
        let root = self.ci.get_zet_root();
        selectors
            .iter()
            .map(|s| {
                let p = self.convert_to_path(s.as_str())?;
                if relative {
                    return Ok(to_relative(&p, root));
                }
                Ok(p)
            })
            .collect()
    }

    fn list_files(&self, selectors: &Vec<String>) -> ApiResult<Vec<Note>> {
        fn _is_hidden(entry: &DirEntry) -> bool {
            entry
                .file_name()
                .to_str()
                .map(|s| s.starts_with('.'))
                .unwrap_or(false)
        }

        fn _is_file(entry: &DirEntry) -> bool {
            entry.metadata().map(|md| md.is_file()).unwrap_or(false)
        }

        let mut ret: Vec<Note> = Vec::new();

        for sel in selectors {
            let path = self.preprocess_selector(sel)?;
            let nb = self.find_notebook_on_path(Path::new(&path));
            let it = WalkDir::new(&path)
                .into_iter()
                .filter_entry(|e| !_is_hidden(e))
                .filter_map(|e| e.ok())
                .filter(_is_file)
                .map(|it| it.into_path())
                .map(|p| Note::new(p, nb));
            ret.extend(it);
        }

        Ok(ret)
    }

    fn is_file(&self, selector: &str) -> ApiResult<bool> {
        let path = self.preprocess_selector(selector)?;
        Ok(self._is_file(Path::new(&path), selector))
    }

    fn _is_file(&self, path: &Path, selector: &str) -> bool {
        if path.exists() {
            return path.is_file();
        }

        !selector.ends_with('/') && !selector.ends_with('\\')
    }

    fn preprocess_selector(&self, selector: &str) -> ApiResult<String> {
        let trimmed = selector.trim().to_string();
        self.convert_to_path(&trimmed)
    }

    fn unique_files(
        &self,
        selectors: &[String],
        default_stem: &str,
        default_extension: Option<&String>,
    ) -> ApiResult<Vec<String>> {
        selectors
            .iter()
            .map(|sel| self.ensure_unique_file(sel, default_stem, default_extension))
            .collect()
    }

    fn ensure_unique_file(
        &self,
        selector: &str,
        default_stem: &str,
        default_extension: Option<&String>,
    ) -> ApiResult<String> {
        let processed = self.preprocess_selector(selector)?;
        let mut path = PathBuf::from(&processed);

        if !self._is_file(Path::new(&processed), selector) {
            let ext = match default_extension {
                Some(e) => e,
                None => self
                    .getenvnbsel("default_filetype", selector)
                    .unwrap_or("md"),
            };

            let default_basename = if ext.is_empty() {
                default_stem.to_string()
            } else {
                format!(
                    "{}.{}",
                    default_stem.trim(),
                    ext.trim().strip_prefix('.').unwrap_or(ext)
                )
            };
            path.push(default_basename);
        }

        let stem = path.file_stem().unwrap().to_string_lossy().into_owned();
        let ext = path
            .extension()
            .and_then(|oss| oss.to_str())
            .map(|ext| {
                if ext.is_empty() {
                    "".to_string()
                } else {
                    format!(".{}", ext)
                }
            })
            .unwrap_or_else(|| "".to_string());

        let mut i = 0;
        loop {
            if path.exists() {
                i += 1;
                let newbase = format!("{}-{}{}", stem, i, ext);
                path.pop();
                path.push(newbase);
            } else {
                break;
            }
        }

        path.to_str()
            .map(|s| s.to_string())
            .ok_or_else(|| "Failed to decode non-unicode path".into())
    }
}

fn to_relative<P>(path: P, root: &Path) -> String
where
    P: AsRef<Path>,
{
    let pr = path.as_ref();
    let mut ret = match pr.strip_prefix(root) {
        Ok(p) => p,
        Err(_) => pr,
    }
    .to_string_lossy()
    .into_owned();

    if pr.to_string_lossy().ends_with('/') && !ret.ends_with('/') {
        ret.push('/')
    }
    ret
}

fn print_lines<S, I>(it: I)
where
    S: AsRef<str>,
    I: IntoIterator<Item = S>,
{
    let stdout = std::io::stdout();
    let mut lock = stdout.lock();

    it.into_iter().for_each(|line| {
        writeln!(lock, "{}", line.as_ref()).ok();
    });
}

fn print_styled(lines: &Vec<String>, style: Style, tty_check: bool) -> ApiResult<()> {
    let mut cp = ColorPrinter::new();
    if !tty_check {
        cp.disable_tty_check();
    }

    for line in lines {
        cp.add(line, Some(style)).nl();
    }
    cp.print()?;
    Ok(())
}
