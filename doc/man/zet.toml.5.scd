zet.toml(5)

# NAME

zet.toml - zet configuration file

# DESCRIPTION

*zet.toml* is a configuration file used by *zet*. It is a TOML file (see
https://toml.io/en). It is stored at zet workspace's root: _.zet/zet.toml_.
Empty zet.toml is created by running *zet init*.

Zet exports configuration to environment with added ZET_ prefix and name derived
from the name of configuration option. Items of TOML's arrays of strings are
separated with a semicolon ";". You can examine the output of *zet env* command
to see what environment is passed to non-core commands.

# CONTENTS

## Notebooks Array

Zet discovers all of managed notebooks by reading _[[notebooks]]_ array entries.
Order in which _[[notebooks]]_ are declared in zet.toml matters for evaluating
the default notebook.

It has the following keys:

*name* (mandatory)
	name of the notebook which will be used in selectors to access it.

*path* (mandatory)
	notebook's path, relative to the workspace root (see the "WORKSPACE
	DISCOVERY" section in *zet(1)*).

*default_filetype* (optional)
	file type for notes which zet should prefer when it creates new ones.
	Default: _md_.

*remote* (optional)
	Git URL used by *zet sync* to automatically initialize the notebook from
	remote repository.

Zet commands aren't restricted the fields listed above. It's possible to add any
other notebook properties for non-core commands to use.

## Command Table

Command table contains options specific for zet commands. There are no
restrictions for names or types of these options. Zet and bundled non-core
commands recognize the following fields:

*command.[command].defaults*
	Table of default command line options automatically added when non-core
	command is invoked, on top of options explicitly used in the invocation.
	Each entry is converted to command line options by the following rules:

	- entries are transformed into long options and are prefixed with 2 dashes
	  "--";
	- value assigned to entry is passed as its next argument, similar to how
	  --option value would be passed;
	- if value is a boolean true, then entry is considered a simple switch
	  without any following parameter;
	- if value is an array, then entry is duplicated for each array's value;
	- values which start with a tilde "~/" are expanded to the home directory
	  (or its equivalent) of current user;
	- special entry named *arguments* can be used to pass positional arguments
	  to the command;
	- default arguments are placed before ordinary command line arguments. The
	  order is: <configured options>, <configured positional arguments>, <command
	  line arguments>

# EXAMPLE

```
[command.menu.deaults]
arguments = "main:"
fresort = true

[command.journal]
template = "~/journal-template.md"
journal-directory = "journal:"

[[notebooks]]
name = "main"
path = "content/0"
default_filetype = "adoc"
remote = "ssh://git@example.com/notebook.git"

[[notebooks]]
name = "journal"
path = "content/my journal"
default_filetype = "org"
remote = "ssh://git@example.com/journal.git"
```

# SEE ALSO

*zet*(1)

Online documentation: https://pages.goral.net.pl/zet/


# AUTHORS

Michał Góral <dev@goral.net.pl>

Source code: https://git.goral.net.pl/zet.git
