# zet replace

Finds all occurences of a chosen regular expression and replaces them
in-place with a string. Search is performed in all files in all notebooks.
Alternatively, [selectors](selectors.md) can be passed as a third and further
arguments to narrow the list of files in which search-replace occurs.

Search can be case-insensitive (`-I`) and interactive (`-i`). In interactive
mode, `zet replace` will print a diff with proposed changes in each file,
which must be accepted before doing the actual replacement.

### Examples

```
$ zet replace 'some.*text' "other text"
Search-replacing through 14 files...

$ zet replace 'some.*text' "other text" -i
Search-replacing through 14 files...
--- Current version of 0/note.md
+++ After replacement of 0/note.md
@@ -1,2 +1,2 @@
-This is some of my text
+This is other text

>> Accept changes in 0/note.md? ([Y]es / [N]o / [Q]uit)
```
