# zet search

Search the notes for selected queries. Each query is a phrase or regular
expression which is expected to match. When more than one query is given, all
of them must be present in a file to return a match. `--or` switch can be
used in cases when _any of_ passed queries must be present to return a
match.

Search scope can be narrowed down by passing [selectors](selectors.md)
through `-s` switch. Searches can be performed case-insensitive with `-i`
switch.

`zet search` uses _grep_ programs found on the system to perform the actual
search and presents their output in the unified way. `zet search` will try to
select the fastest available program, but you can enforce one with `--grep`
switch.

`zet search` will truncate many long matches. You can control this behaviour
with `--show-all` switch.

### Examples

```
$ zet search a e
0:poetry.md:
    I am here
    You are there
    They are everywhere
    This is not a very great poem
    But hopefully someone stops me at some point
(... result truncated, run 'zet search --show-all' to show all matched entries ...)

0:butcher.md:
    Aaaah, fresh meat

$ zet search a y
0:poetry.md:
    They are everywhere
    This is not a very great poem
    But hopefully someone stops me at some point

$ zet search a -l
0:poetry.md
0:butcher.md

$ zet search one two --or
0:ones.md
    one
    one
    certainly not two
    and not the other one

0:twos.md
    two
    two

$ zet search 'regex.*engine' -l -s nb1: -s nb2:
nb1:note.md
nb2:other-note.md
```
