# Non-core Commands

Non-core commands are all the commands which aren't built into Zet. Zet
discovers them dynamically by inspecting $PATH and $ZET_MODULES_PATH. Some
essential non-core commands are distributed with Zet. You can also read their
respective help pages by running `zet <cmd> --help`.

You can list all non-core commands available on your system by running `zet
commands`.
