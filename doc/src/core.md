# Core Commands

Core commands are the ones built into Zet. You can't change them or remove
them. They are the ones which are listed when running `zet --help`.

## zet init

`zet init` initializes a new notebooks collection in a current directory. It
creates initial _.zet/zet.toml_ [configuration file](config.md).

Without arguments, `zet init` will create a new notebook and initialize
zet.toml with data about it. You can change the defaults by passing to `zet
init` a path and a name.

## zet commands

`zet commands` prints all installed non-core commands, together with a short
description of each one (if available).

## zet api

Run plumbing, easy to parse API commands which provide a common interface for
non-core commands to access and compute Zet implementation details. See the
[chapter about API](api.md).

## zet env

Print environment which will be passed to non-core commands. It is used
mostly for debugging purposes.
