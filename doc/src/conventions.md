# Conventions for Zet Commands

You are free to write non-core commands however you want, however it's a good
idea to follow some conventions, because it makes easier for users to use
zet.

## Use selector syntax

When zet commands accept paths to notebooks or files, they should accept
them in [selector syntax](selectors.md). You may use `zet api paths` to
easily convert selectors to actual system paths and correctly handle all of
its quirks. For example:

```
$ zet my-command notebook:file.md
```

The other way around, commands that print a path to the notebook or to the
files inside a notbook, should print them in [selector syntax](selectors.md).
Commands should also support `--absolute` option to print absolute paths
instead of selector syntax. It's not necessary to support paths relative to
`$ZET_ROOT`.

```
$ zet my-command
notebook:file.md

$ zet my-command --absolute
/home/user/docs/notebook/file.md
```

This approach makes it easier to compose together several zet commands or
copy-paste their output to each other.

## Configuration

The best commands are the ones which don't need any configuration. If a
command needs one, it should access it from _zet.toml_ from the [command
table](config.md#command-table).

## Provide help

Zet's non-core commands must provide `--help` parameter which describes its
usage.

## Long and short options

Zet's non-core commands should provide long options (e.g. `--long-option`)
for all possible arguments. Commands should also provide short options (e.g.
`-s`) for the most common ones.

## Don't depend on other non-core commands

Commands shouldn't depend on other non-core commands. They should should use
`zet api` instead.

For example, instead of using `zet notebooks`, commands should use `zet
api notebooks`, or `zet api list` instead of `zet ls`.
