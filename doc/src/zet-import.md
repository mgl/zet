# zet import

Import files and URLs into the notebook. Imported files are automatically
copied to the [default notebook](many-notebooks.md#default-notebook). URLs
are automatically downloaded and converted by [Pandoc](https://pandoc.org)
into Markdown files.

You can change the default destination of imported files with `-d` switch.

`zet import` never overwrites files. If file with a chosen destination
already exists, `zet import` will enumerate it.

### Examples

```
$ zet import https://example.com
https://example.com imported to /home/user/notebooks/0/url.md

$ zet import https://example.com
https://example.com imported to /home/user/notebooks/0/url-1.md

$ zet import ~/some/document.pdf
/home/user/file.pdf imported to /home/user/notebooks/0/document.pdf

$ zet import ~/some/file.pdf -d 0:imported.pdf
/home/user/file.pdf imported to /home/user/notebooks/0/imported.pdf
```
