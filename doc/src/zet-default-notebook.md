# zet default-notebook

This command prints current [default
notebook](many-notebooks.md#default-notebook). You can also change default
notebook by running `zet default-notebook <notebook-name>`.

`<notebook-name>` is the name of notebook configured in _zet.toml_, not its
path. You can list all currently configured notebooks by running `zet
notebooks`.
