# Cheat Sheet

## Write a note

```shell
$ zet note
$ zet note notebook:note.md
```

### Setup a note's template

Note's template can be kept inside the notebook and accessed through a
[selector](selectors.md).

```shell
$ cat <<EOF | zet note --stdin template.md
Captain's Log, stardate {dt}

{input}
EOF

$ zet note -t template.md
$ echo "quick note" | zet note -t template.md --stdin
```

### Append note

Repeatedly using the same note name will append to the file instead of
replacing it.

```shell
$ echo Hello | zet note quicknotes.md --stdin
$ echo "Is it me you're looking for?" | zet note quicknotes.md --stdin
$ cat "$(zet ls quicknotes.md)"
Hello
Is it me you're looking for?
```

## Open existing notes

```shell
$ zet open notebook:note.md
```

## Read the notes

```shell
$ zet show notebook:note.md
```

## Search

```shell
$ zet search "lost money"
$ zet search --or "budget.*taxes" "budget.*expenses"
```

## Search and replace

```shell
$ zet replace "foo bar" "baz"
$ zet replace --interactive "regular.*expression" "Regular Expression"
```

## List all available commands

```shell
$ zet commands
```
