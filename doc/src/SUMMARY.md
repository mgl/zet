# Summary

[Introduction](index.md)

# User Guide

- [Quick Start](quickstart.md)
  - [Installation](installation.md)
  - [Synchronization](sync.md)
  - [Cheat Sheet](cheat-sheet.md)
- [Working with Many Notebooks](many-notebooks.md)
- [Core Commands](core.md)
- [Non-core Commands](non-core.md)
  - [zet gen](zet-gen.md)
  - [zet import](zet-import.md)
  - [zet journal](zet-journal.md)
  - [zet ls](zet-ls.md)
  - [zet menu](zet-menu.md)
  - [zet mv](zet-mv.md)
  - [zet note](zet-note.md)
  - [zet notebooks](zet-notebooks.md)
  - [zet open](zet-open.md)
  - [zet replace](zet-replace.md)
  - [zet run](zet-run.md)
  - [zet search](zet-search.md)
  - [zet show](zet-show.md)
  - [zet sync](zet-sync.md)
  - [zet tasks](zet-tasks.md)
- [Selectors](selectors.md)
- [Hooks](hooks.md)
- [Configuration](config.md)

# Programmer Guide

- [Custom Commands](custom-commands.md)
  - [Zet API]()
  - [Testing Commands]()
  - [Conventions for Zet Commands](conventions.md)
- [Environment Variables]()
