# zet run

Run arbitrary command inside a notebook's directory. Notebook can be changed
with `-n` switch, otherwise a [default
notebook](many-notebooks.md#default-notebook) is used. You may also use `-a`
switch to run a command separately for each configured notebook.

To choose an arbitrary directory, use `-c` switch. It is useful if you want to
run an arbitrary command within zet's environment, e.g. when testing a new zet
module which isn't added to `ZET_MODULES_PATH`.

`-a`, `-n` and `-c` switches are additive, which means that all unique paths
chosen with these switches will be visited by zet.

### Examples

```
$ zet run git status
On branch master
(...)

$ zet run -n other-notebook bash
user@host$ (input prompt)

$ zet run -c . env | grep ZET_
ZET_DIR=....

$ zet run -c . zet-newcommand ...
...
```
