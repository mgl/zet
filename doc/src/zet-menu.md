## zet menu

Interactive command which ties together several other commands. Shows a list
of files inside notebooks, which user can browse, edit or open in external
program. List of files can be initially filtered by passing [selectors](selectors.md).

`zet menu` requires [fzf](https://github.com/junegunn/fzf) to show a
searchable list of files. Pressing `<enter>` prints a selection to standard
output, which can be captured in a script and processed further. However,
`zet menu` also allows several actions without exiting the menu. They are
bound to the following keys:

- `<ctrl-e>`: edit selected file in the current terminal. `${EDITOR}`
  environment variable is used to determine the editor.
- `<ctrl-o>`: open selected file in external program. `xdg-open` is used to
  determine the program which will be used. Typically this opens a new
  window which is detached from the `zet menu` process and should outlive it.
- `<ctrl-f>` search currently typed query via [`zet search`](#zet-search).
  Query may contain any switch accepted by `zet search` (for example _term1
  term2 --or_). Searches are performed in scope of filtered selectors, but
  you can extend them by passing additional `-s` switches. You can also start
  a query with `--` to indicate that from this point only literal arguments
  will be passed.
- `<ctrl-r>` - reload the initial list of files. It is useful if you made
  some changes to the files or if you want to show all the files after using
  the search.

### Sort by Frecency

If you have [fresort](https://git.goral.net.pl/fresort.git) program
installed, file list can be sorted by their usage "frecency". To activae this
feature, run `zet menu --fresort`. Files are recognized by their paths so if
they are renamed (e.g. with [zet mv](#zet-mv.md), their frecency data will
be lost.

