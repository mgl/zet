# zet mv

Rename a file. This command accepts either the _source_ and _destination_
[selectors](selectors.md), or _source_ selector and `--to-title` switch,
which renames a file to the _canonical title_.

Canonical title is a note's title converted to the form which is suitable for
file names. Typically it is lower-cased, has stripped whitespace and some
other special characters.

Renaming can be performed interactively by passing `-i` switch.

### Examples

```
$ zet mv 0:note.md 0:newname.md
$ zet mv 0:newname.md --to-title -i
```

