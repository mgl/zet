# Configuration

Zet reads the configuration inside [TOML's](https://toml.io/en/)
_.zet/zet.toml_ file. Empty _zet.toml_ is created by running [`zet
init`](core.html#zet-init).

Zet exports configuration to environment with added `ZET_` prefix and name
derived from the name of configuration option. Items of TOML's arrays of
strings are separated with a semicolon ";". You can examine the output of
[`zet env`](core.html#zet-env) to see what environment is passed to
non-core commands.

## Configuration Example

```toml
[command.menu.deaults]
arguments = "main:"
fresort = true

[command.journal]
template = "~/journal-template.md"
journal-directory = "journal:"

[[notebooks]]
name = "main"
path = "content/0"
default_filetype = "adoc"
remote = "ssh://git@example.com/notebook.git"

[[notebooks]]
name = "journal"
path = "content/my journal"
default_filetype = "org"
remote = "ssh://git@example.com/journal.git"
```

## Notebooks Array

Zet discovers all of managed notebooks by reading `[[notebooks]]` array
entries. Order in which `[[notebooks]]` are declared in _zet.toml_ matters
for evaluating the [default notebook](many-notebooks.md#default-notebook).

It has the following keys:

#### name

_(mandatory)_

Name of the notebook which will be used in [selectors](selectors.md) to
access it. It can be any string, but for ease of access it's preferabbly a
short single word, all lower-case.

#### path

_(mandatory)_

Relative path to the root directory of notebook. `path` is relative to the
`$ZET_ROOT`, i.e. to the _.zet_ directory.

#### default_filetype

_(optional)_

Change the filetype for notes which Zet should prefer when it creates new
ones (for example with a bare [`zet note`](non-core.md#zet-note)). If this
option isn't set, Zet will default to _md_.

`default_filetype` should be a file extension without the initial dot.

#### remote

_(optional)_

Git URL used by [`zet sync`](non-core.md#zet-sync) to automatically
initialize the notebook from remote repository. See the chapter about
[synchronization](sync.md).

#### other fields

Zet commands aren't restricted the fields listed above. It's possible to add
any other notebook properties for non-core commands to use.

## Command table

Command table contains options specific for zet commands. There are no
restrictions for names or types of these options. Zet and bundled non-core
commands recognize the following fields:

#### command.[command].defaults

Table of default command line options automatically added when non-core
command is invoked, on top of options explicitly used in the invocation.

Zet converts each entry in _defaults_ table to a command line parameter and
adds it to the related command _as if it were typed in command line_. There
are several rules on how these transformations occur:

- entries are transformed into _long options_ and are prefixed with 2 dashes
  "--";
- value assigned to entry is passed as its next argument, similar to how
  `--option value` would be passed;
- if value is a _boolean true_, then entry is considered a simple switch
  without any following parameter;
- if value is an array, then entry is duplicated for each array's value;
- values which start with a tilde "~/" are expanded to the home directory (or
  its equivalent) of current user;
- special entry named _arguments_ can be used to pass positional arguments to
  the command;
- default arguments are placed before ordinary command line arguments. The
  order is: `<configured options>, <configured positional arguments>,
  <command line arguments>`

This feature may be disabled by running zet --no-defaults or by setting
ZET_NO_DEFAULTS=1 environment variable.

##### Examples

```
foo = bar  →  --foo bar
foo = ["bar", "baz"]  →  --foo bar --foo baz
foo = true  →  --foo
arguments = ["foo" bar"]  →  foo bar
```
