# zet show

Show contents of a note inside the terminal, automatically choosing the
fanciest formatting program, like _less_ or _batcat_. You can force using any
particular program with `-p`.

In similar fashion to [`zet open`](#zet-open), `zet show` accepts a partial
selector which interactively queries which file should be opened if partial
selector matches more than one.

> Implementation detail: `zet show` is actually only a wrapper for `zet open`.

### Examples

```
$ zet show glados.md
───────┬────────────────────────────────────────────────────────────────────
       │ File: /home/user/notebooks/0/glados.md
───────┼────────────────────────────────────────────────────────────────────
   1   │ # Glados Anthem
   2   │
   3   │ This was a triumph!
   4   │ I'm making a note here: "HUGE SUCCESS".
   5   │ It's hard to overstate my satisfaction.
```
