# zet journal

Create and edit daily journal entries. `zet journal` opens a file fo the
current day. All journal entries are kept in a separate directory, controlled
by `--journal-directory` switch. It accepts a [selector](selectors.md)
pointing to existing or not existing directory.

You can edit journal for different dates by passing an argument to `zet
journal`. It can be either a date in ISO format, or a human-readable date
string, like "yesterday" or "last Friday".

Similar to `zet note`, journal accepts `--editor` and `--template` arguments.

Journal templates accept following placeholders:

- `{date}` - current date
- `{host}` - hostame of the current system, as reported my `hostname` program
- `{user}` - name of the current user

### Examples

```
$ zet journal

$ zet journal -j my-journal-directory

$ zet journal yesterday

$ zet journal last Friday

$ zet journal --editor='my-editor {}'
```

