# zet notebooks

List the notebooks, optionally formatted. Format string passed to `-f` switch
accepts the following tokens:

- `{name}` - configured notebook's name
- `{path}` - notebook's path, relative to $ZET_ROOT
- `{abspath}` - notebook's absolute path

