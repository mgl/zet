# zet open

Open notes and files from a notebook. This command accepts a
[selector](selectors.md) with a twist: it might expand to a partial file
path, which is examined against all the files. If there is a match, it is
opened with the _opener_ program (e.g. xdg-open). If there are many matches,
`zet open` interactively queries which one should it open.

Only the _file part_ of selector is matched against the files.

Default opener can be changed with `-p` switch. It accepts `{}` placeholder
which will be replaced with actual file path to open.

### Examples

```
$ zet open 0:ot
More than one file matches. Which one to open?
[1] 0:other.md
[2] 0:robots-take-over-the-world.md
```
