# Synchronization

If you keep your notebooks in Git repositories, you may use
[git-sync](https://github.com/simonthum/git-sync/) to synchronize them. `zet
sync` is a command which you should use each time you want to synchronize.

> The prerequisite for all below methods is that git-sync script is
> downloaded and installed in your `$PATH`.

## Existing Remote Repositories

If you have a remote Git repository which you'd like to automatically
download when you invoke `zet sync`, you can configure it in
_.zet/zet.toml_. 

1.  Add configuration for the new notebook to _.zet/zet.toml_. Point `remote`
    field to Git address you would like to fetch.

    ```toml
    [[notebooks]]
    name = "My Synchronized Notebook"
    path = "my-notebook"
    remote = "ssh://git@example.com/my-notebook.git"
    ```

2.  Run `zet sync`

## Existing Notebook

If you already have a local notebook, you can configure its syncrhonization
with git with `zet sync --configure`. If you add `remote` field to notebook's
section in _zet.toml_, zet sync will automatically configure it as well.

```
$ zet sync --configure notebook
$ zet sync
```

## Local Only Notebooks

This is a variation of *Existing Notebook* method which stores notebook in
local Git repository. There isn't any remote configured, so all notes stay
locally on your computer. Notes are automatically committed during `zet
sync`.
