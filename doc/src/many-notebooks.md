# Working with Many Notebooks

Many notebooks can be collected under a single `$ZET_ROOT`. by adding a
separate `[[notebooks]]` section for each of them inside zet.toml:

```toml
[[notebooks]]
name = "0"
path = "notebooks/first one"

[[notebooks]]
name = "1"
path = "notebooks/second one"
```

## Default Notebook

You can query and set a default notebook with `zet default-notebook`. If
default notebook was never set, the first notebook in zet.toml will be used
as a default.

Default notebooks are important due to their role in filling the notebook gap
in [selectors](selectors.md). Whenever you don't specify a notebook in
selector, the default one will be used.
