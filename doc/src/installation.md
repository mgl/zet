# Installation

1.  Download zet from <https://git.goral.net.pl/zet.git/>:

    ```shell
    $ git clone https://git.goral.net.pl/zet.git
    ```

2.  Compile and install

    ```shell
    $ cd zet
    $ make
    $ make install
    ```

    > `make install` respects `PREFIX` and `DESTDIR` environent variables.
    > By default zet is installed to `~/.local`. If you wish to install Zet
    > system-wide, you should change make invocation to something like
    > `PREFIX=/usr make install`

3.  Initialize the first notebook

    ```shell
    $ cd ~/notebooks
    $ zet init
    ```

    > If you already have a directory with your notes, you can point zet
    > to it: `zet init path/to/notes "My Notebook Name"`

4.  Set `$ZET_ROOT` to use zet from anywhere

    ```shell
    $ echo "export ZET_ROOT=\"${HOME}/notebooks\"" >> ~/.bashrc
    ```

    > Zet can also work without setting `$ZET_ROOT`. In that case you'll have
    > to enter your notebook's directory (or any child directory) and run Zet
    > commands from there. This mode is useful if you have many notebooks
    > scattered over the file system.
