# zet note

Write new notes in a format preferred by the
[`default_filetype`](config.md#default_filetype) option. Normally `zet note`
opens editor, but it's also possible to add new notes by piping them into
`zet note --stdin`. Notes can be put into templates, which is especially
useful for notes piped into `zet note`.

By default `zet note` will create a new file. `zet note` tries to detect note
title and name new file accordingly. This happens whenever passed
[selector](selectors.md) (including the default one) points to a directory.
Alternatively you can provide a selector with note's destination. If it
exists, `zet note` will append to that file.

You can change the default editor by passing it to `--editor` switch. It
accepts a format string with `{}` placeholder, which will be replaced with a
file path to the temporary note file.

Normally `zet note` is interactive command which relies on editor being
displayed to the user. It means that it is hard to redirect standard output
to capture created note's path. That's why `zet note` provides `--and-then`
switch which executes a command after successful note creation. This command
is executed via a shell (`sh -c`) and it accepts a single placeholder `{}`,
which will be substituted with note's path.

### Templates

Templates are ordinary files and can be stored inside a notebook. They're
chosen with `-t` switch which accepts a selector. Template files may contain
the following placeholders, which will be automatically substituted when
creating the note:

- `{dt}` - current date/time
- `{host}` - hostame of the current system, as reported my `hostname` program
- `{user}` - name of the current user
- `{input}` - note piped into `zet note --stdin`

### Examples

```
$ zet note

$ zet note 0:quicknotes.md

$ echo "Hello" | zet note --stdin -t 0:note-template.md

$ zet note --editor='my-editor {}'

$ zet note --and-then 'rsync -av "{}" ssh://example.com/'
```

