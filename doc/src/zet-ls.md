# zet ls

List files inside notebooks. By default all files from all notebooks are
listed. This selection can be narrowed down by passing one or more
[selectors](selectors.md). If any selector expands to a directory, all files
in that directory are listed, recursively.

Along file paths, `zet ls` can also print note titles of known formats by
inspecting file contents.

### Examples

```
$ zet ls 0:
0:note.md
0:other-note.md


$ zet ls 0: --title --field-separator='|'
Note Title|0:note.md
|0:other-note.md
```

