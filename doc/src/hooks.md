# Hooks

Hooks are programs which you can place in a hooks directory to trigger
actions at certain points in zet's execution.

Hooks directory is \$ZET\_DIR/hooks (*.zet/hooks*). Zet searches it for
specific executables which are invoked at certain points of zet's lifetime.
Hooks without executable bits set are ignored.

Zet passes informations to the hook about the triggering event, such as
triggering command. They are passed via command line arguments. Hooks also
receive the same environment as ordinary zet commands. Notably, \$ZET\_ROOT
and notebook variables parsed from *zet.toml* are set.

Zet doesn't change a current working directory before invoking hooks, but it
respects `--directory` switch.

Hooks can be disabled by using `--no-hooks` switch or by setting
`ZET_NO_HOOKS=1` environment variable. Setting the environment variable
inside the hook has effect of disabling nesting of hooks for all consecutive
Zet commands.

## Hook Types

### pre-command

This hook is invoked before any non-core command, such as *zet mv*, *zet
note* or *zet sync*. Core commands, such as *zet init*, *zet env* or *zet
api* do not trigger this hook.

If pre-command fails by returning a non-zero exit code, it will prevent
execution of Zet command itself.

Zet passes the following arguments to it:

- command which triggered the hook, without the *zet-* prefix, for
  example *mv*, *note* or *sync*
- all arguments which are passed to the command

Arguments are passed exactly as they were passed to Zet (so they might be
expanded by the underlying shell, but selectors will not be expanded by Zet).

### post-command

This hook is invoked after Zet non-core command finishes. It receives a flag,
which takes a value of 0 or 1 and denotes whether Zet command was successful
or not. 0 means success. Other parameters are the same as in *pre-command*
hook.

Zet won't trigger post-command hook when it was unable to run a command (for
example when user invoked a non-core command which doesn't exist).
