# Selectors

A lot of commands which work on files use _selectors syntax_ in place where
you'd normally put a path to the file. Aim of selectors is to simplify the
way these paths are provided.

Zet expands selectors to ordinary paths. From the left, before the colon,
selectors have notebook's _name_ (not path), separated by the colon and then
relative path of a note inside that notebook. Each of these parts is
optional.

Selectors look like this:

```
selector := [notebook:][directory/][note.md]
```

For example `notebook:note.md`, `note.md`, `subdir/` and
`notebook:subdir/other-note.md` are all selectors.

Each part of selector is optional. Selectors can expand both to files and to
directories. Paths to which selectors expand don't have to exist at all. In
some situations it is then important to be aware of the rules which apply to
selector expansion.

## Selector Expansion Rules

- If selector is an absolute path, it is left untouched;
- if _notebook_ part is missing, selector expands with a [default
  notebook](many-notebooks.md) in place of it;
- if _path_ part (i.e. _directory/note.md_) is missing, selector expands to
  the path to notebook's directory;
- if selector expands to the path which doesn't exist on a file system:
    - if selector ends with a slash character (`/` or `\\`), then it is
      treated as a directory;
    - if selector doesn't end with a slash character, then it is treated as a
      file.

> You can test selectors with 2 [API](api.md) commands: `zet api paths <selector>`
> and `zet api is-file <selector>`

## Examples

Let's suppose that `ZET_ROOT=$HOME/notebooks` and that we have following file
structure:

```
~
└── notebooks/
    ├── .zet/
    │   └── zet.toml
    ├── notebook1/
    │   ├── subdir/
    │   │   └── note.md
    │   └── note.md
    ├── notebook2/
    │   └── note.md
```

We name our notebooks: "1" for _notebook1_ and "2" for _notebook2_. Default
notebook is notebook1. In that case, we'll have the following selectors:

| Selector              | Expanded Path                         | File or Directory |
|-----------------------|---------------------------------------|-------------------|
| `1:`                  | ~/notebooks/notebook1                 | Directory         |
| `1:note.md`           | ~/notebooks/notebook1/note.md         | File              |
| `1:subdir/note.md`    | ~/notebooks/notebook1/subdir/node.md  | File              |
| `2:`                  | ~/notebooks/notebook2                 | Directory         |
| `2:note.md`           | ~/notebooks/notebook2/note.md         | File              |
| `note.md`             | ~/notebooks/notebook1/note.md         | File              |
| `subdir/note.md`      | ~/notebooks/notebook1/subdir/note.md  | File              |
| `1:note.md/`          | ~/notebooks/notebook1/note.md         | File              |
| `subdir`              | ~/notebooks/notebook1/subdir          | Directory         |
| `subdir/`             | ~/notebooks/notebook1/subdir          | Directory         |
| `missing-note.md`     | ~/notebooks/notebook1/missing-note.md | File              |
| `missing-dir.md/`     | ~/notebooks/notebook1/missing-dir.md  | Directory         |

## Examples in Commands

- `zet ls 1:` will list all files in a notebook "1"
- `zet ls 1` lacks a colon after "1", so it will list a single file "1" in a
  notebook "1" (equivalent of `zet ls 1:1`)
- `zet mv 1 2/` will move a file named "1" from a notebook 1 to a directory
  named "2" in a notebook 1
- `zet note foo/` will create a new note in a directory "foo"
- `zet note foo` will create a note "foo" in a default notebook. If it
  already exists, `zet note` will append to it
