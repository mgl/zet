# zet gen

Generate a file name from a format string. The following format tokens are
available:

- `%u`  - new UUID[^uuid] (requires uuidgen program installed)
- `%su` - short UUID (8 first characters of UUID; requires uuidgen program
  installed)
- `%d`  - current date and time, with any non-numeric characters being stripped
- `%r`  - random number


[^uuid]: UUIDs are globally unique indentifiers composed of dash-separated
         groups of 8-4-4-4-12 hexadecimal characters (i.e. characters from
         the set of 0-9 and a-f)


### Examples

```
$ zet gen file-%su.txt
file-f391b9a0.txt
```
