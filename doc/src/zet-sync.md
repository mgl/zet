# zet sync

Automatically synchronize notes in notebooks which are git repositories. It
requires [git-sync](https://github.com/simonthum/git-sync) to work and works
faster for many notebooks if [GNU Parallel](https://www.gnu.org/software/parallel)
is present on the system.

Please refer to [Synchronization](sync.md) chapter for details.
