# zet tasks

Show tasks in notebooks. Tasks are lines with checkboxes in form of `- [ ]`
for open and `- [X]` for closed tasks.

### Examples

```
$ zet tasks
0:todo.md
---------
- [ ] task 1
    - [ ] subtask

$ zet tasks -s closed
0:todo.md
---------
- [X] finished task
```
