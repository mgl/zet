# Zet

**Zet** is command line (CLI) notes manager. Initially it was built to
address some pain points with CLI notes management, especially for
Zettelkasten-like notes and knowledge bases. It is generic and isn't
inherently bound to any particular methodology or notes format.

To quickly get started with Zet, see [Quick Start](quickstart.md).
