include config.mk

all: build

build:
	cargo build $(CARGOFLAGS)

install: build install-modules install-man
	cargo install $(CARGOFLAGS_INSTALL) --root $(DESTDIR)$(PREFIX)

install-modules:
	mkdir -p $(DESTDIR)$(PREFIX)/share/zet
	cp -rf modules $(DESTDIR)$(PREFIX)/share/zet/
	chmod 755 $(DESTDIR)$(PREFIX)/share/zet/modules/zet-*
	cp -rf lib $(DESTDIR)$(PREFIX)/share/zet/

install-man:
	$(INSTALL) -d "$(DESTDIR)$(MANPREFIX)/man1" "$(DESTDIR)$(MANPREFIX)/man5"
	$(SCDOC) < doc/man/zet.1.scd > "$(DESTDIR)$(MANPREFIX)/man1/zet.1"
	$(SCDOC) < doc/man/zet.toml.5.scd > "$(DESTDIR)$(MANPREFIX)/man5/zet.toml.5"

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/zet
	rm -rf $(DESTDIR)$(PREFIX)/share/zet
	rm -rf $(DESTDIR)$(MANPREFIX)/man1/zet.1
	rm -rf $(DESTDIR)$(MANPREFIX)/man5/zet.toml.1

lint:
	cargo clippy
	rustfmt --check src/*

check: build
	cargo test $(CARGOFLAGS)
	./check -n auto
	cargo clippy
	rustfmt --check src/*

clean:
	rm -rf target

.testenv: test-requirements.txt
	rm -rf "$@"
	python3 -m venv $@
	$@/bin/pip3 install -r $<

doc:
	cd doc && mdbook build

.PHONY: all build install uninstall clean test doc
