deploy:
    #!/usr/bin/bash
    set -o errexit
    set -o nounset
    set -o pipefail

    cd doc && mdbook build
    install-artifacts build "$(basename -- "${GIT_REF}")"
