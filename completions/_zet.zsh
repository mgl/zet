#compdef zet

_zet_get_notebooks_esc() {
    zet api notebooks | sed 's/$/\\:/'
}

_zet_get_notebooks() {
    zet api notebooks --selector
}

_zet-default-notebook() {
    local -a notebooks

    notebooks=( $(_zet_get_notebooks_esc) )
    _arguments ": :{_describe 'command' notebooks }" \
}

_zet-import() {
    local -a notebooks flags

    notebooks=( $(_zet_get_notebooks) )
    flags=(
        {-d,--destination=}'[destination selector]: :($notebooks)' \
    )

    _arguments \
        ": :{_files}" \
        ${flags[@]}
}

_zet-journal() {
    local -a notebooks flags

    notebooks=( $(_zet_get_notebooks) )
    flags=(
        '(-j --journal-directory)'{-j,--journal-directory=}'[root directory for journal]: :($notebooks)'
        {-t,--template=}'[template string ro a new journal entry]:template'
        '--editor=[editor command]:editor'
    )

    _arguments ${flags[@]}
}

_zet-ls() {
    local -a notebooks flags

    flags=(
        '--absolute[print absolute paths]'
        '(-t --title)'{-t,--title}'[print a title as well]'
        '(-F --field-separator)'{-F,--field-separator=}'[change the separator between fields]:separator'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-menu() {
    local -a notebooks flags

    flags=(
        '(-p --preview)'{-p,--preview}'[preview position]: :(top bottom left right)'
        '--previewer=[program used to preview files]:previewer'
        '--fresort[sort file list by their usage frecency]'
        '--no-header[disable header]'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-mv() {
    local -a notebooks flags

    flags=(
        '--to-title[rename note to canonical path obtained from the title]'
        '--interactive[confirm move before doing it]'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-note() {
    local -a notebooks flags

    flags=(
        '--stdin[read text from standard input]'
        '--editor=[editor command]:editor'
        {-t,--template=}'[template file]:_files'
        '--title=[value for {title} placeholder]:val'
        '--dt=[value for {dt} placeholder]:val'
        '--and-then=[command to execute after a successful note creation]:cmd'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-notebooks() {
    local -a notebooks flags

    flags=(
        {-f,--fmt=}'[format string]:_files'
    )

    _arguments ${flags[@]}
}

_zet-open() {
    local -a notebooks flags

    flags=(
        {-p,--program=}'[file opener]:prog'
        {-i,--ignore-case}'[matching is case-insensitive]'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-replace() {
    local -a notebooks flags empty

    flags=(
        {-i,--interactive}'[confirm each change]'
        {-I,--ignore-case}'[replacement is case-insensitive]'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    empty=()

    _arguments \
        "1: :{_describe 'from string' empty}" \
        "2: :{_describe 'to string' empty}" \
        "*: :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-run() {
    local -a notebooks flags

    flags=(
        {-n,--notebook=}'[choose notebook]:nb'
        {-a,--all}'[run command in all notebooks]'
    )

    notebooks=( $(_zet_get_notebooks_esc) )

    _arguments \
        ": :{_describe 'command' notebooks }" \
        ${flags[@]}
}

_zet-search() {
    local -a notebooks flags empty

    notebooks=( $(_zet_get_notebooks) )

    flags=(
        {-s,--selectors=}'[narrow down search to selectors]: :($notebooks)'
        '--or[return notes with at least one of queries found]'
        {-i,--ignore-case}'[case-insensitive search]'
        {-l,--lines-with-matches}'[only print path of files with matches]'
        '--absolute[print absolute paths]'
        {-a,--show-all}'[do not truncate results]'
        '--grep-program=[force particular grep flavor]: :(rg grep)'
    )

    empty=()

    _arguments \
        "*: :{_describe query empty}" \
        ${flags[@]}
}

_zet-show() {
    local -a notebooks flags

    notebooks=( $(_zet_get_notebooks_esc) )

    flags=(
        {-p,--program=}'[override PAGER selection]:pager'
    )

    empty=()

    _arguments \
        ": :{_describe query notebooks}" \
        ${flags[@]}
}

_zet-sync() {
    local -a notebooks

    notebooks=( $(zet api notebooks) )

    _arguments \
        "*: :{_describe query notebooks}"
}

_zet-tasks() {
    local -a notebooks flags

    notebooks=( $(_zet_get_notebooks_esc) )

    flags=(
        {-s,--status=}'[task status]: :(all open closed)'
    )

    _arguments \
        "*: :{_describe query notebooks}" \
        ${flags[@]}
}

_zet_commands() {
    local -a subcommands

    export IFS=$'\n'
    subcommands=(
        "init:initialize a new notebooks collection"
        "commands:print all installed zet subcommands"
        "env:print environment passed to subcommands"
        "api:plumbing, easy to parse API commands used by modules"
        "help:print help for zet and its subcommands"
    )
    subcommands+=($(zet commands --list))
    unset IFS

    _describe 'command' subcommands
}

_zet() {
    integer ret=0

    _arguments -C \
        '(- :)--version[display version information]' \
        '(- :)--help[display help message]' \
        {-C,--directory}'[change to directory before reading a configuration]: :_directories' \
        {-c,--config}'[path to zet.toml configuration file]: :_files' \
        '--no-hooks[disable hooks]' \
        '--no-defaults[disable automatic default arguments for subcommands]' \
        '(-): :->command' \
        '(-)*:: :->option-or-argument' && return

    case $state in
        (command)
            _zet_commands && ret=0 ;;
        (option-or-argument)
            _call_function ret _zet-${words[1]} ;;
    esac

    return ret
}

_zet
