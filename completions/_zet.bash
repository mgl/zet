_contains() {
    local match="$1"; shift
    local e
    for e in "$@"; do
        [[ "$e" == "$match" ]] && return 0;
    done
    return 1
}

_startswith() {
    local s=$1
    local prefix=$2

    [[ "${s}" != "${s#${prefix}}" ]]
}

# FIXME: this is vary simple parser, which doesn't include --long=val case,
# which, I think, Bash splits at '=' sign
_count_positional() {
    local no=-1
    for elem in "${COMP_WORDS[@]}"; do
        elem=$(echo "${elem}" | xargs)  # trim whitespace
        if [[ -n "${elem}" ]] && ! _startswith "${elem}" "-"; then
            no=$((no + 1))
        fi
    done

    echo "${no}"
}

_complete_selectors() {
    local nbs="$(zet api notebooks --selector | sed 's/ /\\ /g')"
    COMPREPLY+=($(compgen -W "${nbs}" -- "${CUR}"))
}

_compw() {
    COMPREPLY+=($(compgen -W "$*" -- "${CUR}"))
}

_zet-default-notebook() {
    [[ $COMP_CWORD -lt 3 ]] || return
    if [[ $(_count_positional) -le 3 ]]; then
        _compw "$(zet api notebooks)"
    fi
}

_zet-import() {
    if _startswith "${CUR}" "-"; then
        _compw "--destination"
        return
    fi

    case "${PREV}" in
        -d|--destination)  _complete_selectors ;;
        *)                 COMPREPLY+=($(compgen -f -- "${CUR}")) ;;
    esac
}

_zet-journal() {
    case "${PREV}" in
        -j|--journal-directory)  _complete_selectors ;;
        -t|--template)          return ;;
        -e|--editor)            return ;;
    esac


    if _startswith "${CUR}" "-"; then
        _compw --journal-directory --template --editor
    fi
}

_zet-ls() {
    case "${PREV}" in
        -F|--field-separator)  return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --absolute --title --field-separator
        return
    fi

    _complete_selectors
}

_zet-menu() {
    case "${PREV}" in
        -p|--preview) _compw top bottom left right; return ;;
        --previewer) return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --preview --previewer --fresort --no-header
        return
    fi

    _complete_selectors
}

_zet-mv() {
    if _startswith "${CUR}" "-"; then
        _compw --to-title --interactive
        return
    fi

    if [[ $(_count_positional) -le 4 ]]; then
        _complete_selectors
    fi
}

_zet-note() {
    case "${PREV}" in
        --editor)       return ;;
        -t|--template)  COMPREPLY+=($(compgen -f -- "${CUR}")); return ;;
        --title)        return ;;
        --dt)           return ;;
        --and-then)     return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --stdin --editor --template --title --dt --and-then
        return
    fi

    if [[ $(_count_positional) -le 3 ]]; then
        _complete_selectors
    fi
}

_zet-notebooks() {
    case "${PREV}" in
        -f|--fmt)       return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --fmt
        return
    fi
}

_zet-open() {
    case "${PREV}" in
        -p|--program)   return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --program --ignore-case
        return
    fi

    if [[ $(_count_positional) -le 3 ]]; then
        _complete_selectors
    fi
}

_zet-replace() {
    if _startswith "${CUR}" "-"; then
        _compw --interactive --ignore-case
        return
    fi

    if [[ $(_count_positional) -gt 3 ]]; then
        _complete_selectors
    fi
}

_zet-run() {
    case "${PREV}" in
        -n|--notebook)   _compw "$(zet api notebooks)"; return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --notebook --all
        return
    fi
}

_zet-search() {
    case "${PREV}" in
        -s|--selectors)     _complete_selectors; return ;;
        --grep-program)     _compw rg grep ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --selectors --or --ignore-case --lines-with-matches --absolute --show-all --grep-program
        return
    fi
}

_zet-show() {
    case "${PREV}" in
        -p|--program)   return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --program
        return
    fi

    if [[ $(_count_positional) -eq 1 ]]; then
        _complete_selectors
    fi
}

_zet-sync() {
    _compw "$(zet api notebooks)"
}

_zet-tasks() {
    case "${PREV}" in
        -s|--status)     _compw all open closed; return ;;
    esac

    if _startswith "${CUR}" "-"; then
        _compw --status
        return
    fi

    _complete_selectors
}

_zet() {
	COMPREPLY=()
	CUR="${COMP_WORDS[COMP_CWORD]}"
    PREV="${COMP_WORDS[COMP_CWORD-1]}"

    local commands=($(zet commands --list | sed 's/:.*//'))
    local commands+=(init commands env api help)

    # check if there's already a subcommand; if so - delegate completion
    for word in "${COMP_WORDS[@]}"; do
        if _contains "${PREV}" -C --directory -c --config; then
            continue;
        fi

        if _contains "${word}" "${commands[@]}"; then
            local compl_subc="_zet-${word}"
            if type "${compl_subc}" >/dev/null 2>&1; then
                "${compl_subc}"
            fi
            return
        fi
    done

    # no subcommand, check we're waiting for option's argument
    case "${PREV}" in
        -C|--directory)     COMPREPLY+=($(compgen -d -- "${CUR}")); return ;;
        -c|--config)        COMPREPLY+=($(compgen -f -- "${CUR}")); return ;;
    esac

    # no subcommand, check if we should complete global option names
    if _startswith "${CUR}" "-"; then
        _compw --directory --config --no-hooks --no-defaults --version --help
        return
    fi

    # complete subcommand names
    COMPREPLY+=($(compgen -W "${commands[*]}" -- "${CUR}"))
}

complete -F _zet zet
